package com.airbus.ims.workflow;

import com.airbus.ims.business.WorkflowOperations;
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Level;

public class PWO2Workflow extends AbstractWorkflow {
    PWO2Workflow(Issue i) {
        super(i);
    }

    PWO2Workflow(Issue i, String loggerName, Level level) {
        super(i, loggerName, level);
    }

    public void onCreateCustomScriptPostFunction() {
        WorkflowOperations.createOrEditPWO(mutableIssue,user, log, WorkflowOperations.PWO2);
    }

    public boolean onStart1CustomScriptedCondition() {
        return WorkflowOperations.isLinkedToIssueType(mutableIssue,user, log, WorkflowOperations.ID_IT_PWO1);
    }

    public void onStart1CustomScriptPostFunction() {
        WorkflowOperations.updateFieldLinkToData(mutableIssue,user, log);
    }
}
