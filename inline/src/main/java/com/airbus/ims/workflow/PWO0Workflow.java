package com.airbus.ims.workflow;

import com.airbus.ims.business.WorkflowOperations;
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Level;

public class PWO0Workflow extends AbstractWorkflow {
    PWO0Workflow(Issue i) {
        super(i);
    }

    PWO0Workflow(Issue i, String loggerName, Level level){
        super(i, loggerName, level);
    }

    public void onCreateCustomScriptPostFunction() {
        WorkflowOperations.createOrEditPWO(mutableIssue, user, log, WorkflowOperations.PWO0);
    }

    public boolean onCancelSimpleScriptedCondition(){
        return WorkflowOperations.checkIfStatusIsNotCompletedOrCancelled(issue);
    }

    public void onCancelCustomScriptPostFunction() {
        WorkflowOperations.cancelPWO0(mutableIssue, user, log);
    }

    public void onAdminReOpenCustomScriptPostFunction(){
        WorkflowOperations.reOpenPWO0AsAdministrator(mutableIssue, user, log);
    }

    public boolean onStart1CustomScriptedCondition() {
        return WorkflowOperations.isLinkedToIssueType(mutableIssue, user, log, WorkflowOperations.ID_IT_CLUSTER);
    }

    public void onStart1CustomScriptPostFunction() {
        WorkflowOperations.updateFieldLinkToData(mutableIssue, user, log);
    }

    public void onStart2CustomScriptPostFunction(){
        WorkflowOperations.updatePWO0Dependencies(mutableIssue, user, log);
    }

    public void onStart3CustomScriptPostFunction() {
        WorkflowOperations.processPWO0PreDates(mutableIssue, user, log);
    }

    public void onDeliverCustomScriptPostFunction() {
        WorkflowOperations.processPWO0FinalDates(mutableIssue, user, log);
    }
}