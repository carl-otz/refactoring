package com.airbus.ims.workflow;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

abstract class AbstractWorkflow {

    public Issue issue;
    public MutableIssue mutableIssue;
    public Logger log;
    public ApplicationUser user;

    public static final String DEFAULT_LOGGER_NAME = "com.valiantys.script.listener";

    AbstractWorkflow(Issue i) {
        initWithDefaultParameters(i);
    }

    AbstractWorkflow(Issue i, String loggerName, Level level){
        initWithDefaultParameters(i);
        log = Logger.getLogger(loggerName);
        log.setLevel(level);
    }

    /**
     * Initialize the workflow with default parameters
     * @param i
     */
    private void initWithDefaultParameters(Issue i){
        issue = i;
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        mutableIssue = issueManager.getIssueObject(issue.getId());
        log = Logger.getLogger(DEFAULT_LOGGER_NAME);
        log.setLevel(Level.DEBUG);
        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        user = authenticationContext.getLoggedInUser();
    }
}
