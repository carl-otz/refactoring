package com.airbus.ims.workflow;

import com.airbus.ims.business.WorkflowOperations;
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Level;

public class PWO1Workflow extends AbstractWorkflow {
    PWO1Workflow(Issue i) {
        super(i);
    }

    PWO1Workflow(Issue i, String loggerName, Level level){
        super(i, loggerName, level);
    }

    public void onCreateCustomScriptPostFunction() {
        WorkflowOperations.createOrEditPWO(mutableIssue, user, log, WorkflowOperations.PWO1);
    }

    public boolean onStart1CustomScriptedCondition() {
        return WorkflowOperations.isLinkedToIssueType(mutableIssue, user, log, WorkflowOperations.ID_IT_PWO0);
    }

    public boolean onStart2CustomScriptedCondition() {
        return WorkflowOperations.isPWO1StartConditionSatisfied(mutableIssue, user, log);
    }

    public void onStart1CustomScriptPostFunction() {
        WorkflowOperations.updateFieldLinkToData(mutableIssue, user, log);
    }

    public void onStart2CustomScriptPostFunction() {
        WorkflowOperations.processPWO1Dates(mutableIssue, user, log);
    }

    public void onDeliver1CustomScriptPostFunction(){
        WorkflowOperations.processPWO1IAValidatedField(mutableIssue, user, log);
    }

    public void onDeliver2CustomScriptPostFunction() {
        WorkflowOperations.processPWO1NbReworkField(mutableIssue, user, log);
    }
}