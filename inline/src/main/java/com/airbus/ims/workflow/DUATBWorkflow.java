package com.airbus.ims.workflow;

import com.airbus.ims.business.WorkflowOperations;
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Level;

public class DUATBWorkflow extends AbstractWorkflow {
    DUATBWorkflow(Issue i) {
        super(i);
    }

    DUATBWorkflow(Issue i, String loggerName, Level level) {
        super(i, loggerName, level);
    }

    public boolean onCopyDUCustomScriptedCondition() {
        return WorkflowOperations.isCopyDUConditionSatisfied(mutableIssue, user, log);
    }
}
