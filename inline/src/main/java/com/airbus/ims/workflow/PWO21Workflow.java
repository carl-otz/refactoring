package com.airbus.ims.workflow;

import com.airbus.ims.business.WorkflowOperations;
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Level;

public class PWO21Workflow extends AbstractWorkflow {
    PWO21Workflow(Issue i) {
        super(i);
    }

    PWO21Workflow(Issue i, String loggerName, Level level){
        super(i, loggerName, level);
    }

    public void onCreate1CustomScriptPostFunction() {
        WorkflowOperations.setSecurityLevelForSubPWO(mutableIssue, log);
    }

    public void onCreate2CustomScriptPostFunction() {
        WorkflowOperations.createOrEditPWO(mutableIssue, user, log, WorkflowOperations.PWO21);
    }

    public boolean onStart1CustomScriptedCondition() {
        return WorkflowOperations.isPWO21Ready(issue, user, log);
    }

    public void onStart1CustomScriptPostFunction() {
        WorkflowOperations.processPWO21Dates(mutableIssue, user, log);
    }

    public void onDeliver1CustomScriptPostFunction() {
        WorkflowOperations.processPWO21AuthoringRequest(mutableIssue, user, log);
    }

    public void onDeliver2CustomScriptPostFunction() {
        WorkflowOperations.processPWO21ReworkField(mutableIssue, user, log);
    }

    public void onReject1CustomScriptPostFunction() {
        WorkflowOperations.processPWO21AuthoringRework(mutableIssue, user, log);
    }

    public void onReject2CustomScriptPostFunction() {
        WorkflowOperations.processPWO21AuthoringValidation(mutableIssue, user, log);
    }
}