package com.airbus.ims.workflow;

import com.airbus.ims.business.WorkflowOperations;
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Level;

public class TriggerWorkflow extends AbstractWorkflow {
    TriggerWorkflow(Issue i) {
        super(i);
    }

    TriggerWorkflow(Issue i, String loggerName, Level level){
        super(i, loggerName, level);
    }

    public boolean onCancelSimpleScriptedCondition(){
        return WorkflowOperations.checkIfStatusIsNotCompletedOrCancelled(issue);
    }

    public boolean onCreateSimpleScriptedValidator() {
        return WorkflowOperations.validateTrigger(mutableIssue, user, log);
    }

    public boolean onInitializeCustomScriptedCondition() {
        return WorkflowOperations.initializeTrigger(issue);
    }
}
