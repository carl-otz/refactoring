package com.airbus.ims.workflow;

import com.airbus.ims.business.WorkflowOperations;
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Level;

public class PWO23Workflow extends AbstractWorkflow {
    PWO23Workflow(Issue i) {
        super(i);
    }

    PWO23Workflow(Issue i, String loggerName, Level level){
        super(i, loggerName, level);
    }

    public void onCreate1CustomScriptPostFunction() {
        WorkflowOperations.setSecurityLevelForSubPWO(mutableIssue, log);
    }

    public void onCreate2CustomScriptPostFunction() {
        WorkflowOperations.createOrEditPWO(mutableIssue, user, log, WorkflowOperations.PWO23);
    }

    public boolean onStart1CustomScriptedCondition() {
        return WorkflowOperations.isPWO23Ready(mutableIssue, user, log);
    }

    public void onStartCustomScriptPostFunction() {
        WorkflowOperations.processPWO23IntValidation(mutableIssue, user, log);
    }

    public void onDeliverCustomScriptPostFunction() {
        WorkflowOperations.processPWO23TechnicalClosure(mutableIssue, user, log);
    }
}
