package com.airbus.ims.workflow;

import com.airbus.ims.business.WorkflowOperations;
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Level;

public class PWO22Workflow extends AbstractWorkflow {
    PWO22Workflow(Issue i) {
        super(i);
    }

    PWO22Workflow(Issue i, String loggerName, Level level){
        super(i, loggerName, level);
    }

    public void onCreate1CustomScriptPostFunction() {
        WorkflowOperations.setSecurityLevelForSubPWO(mutableIssue, log);
    }

    public void onCreate2CustomScriptPostFunction() {
        WorkflowOperations.createOrEditPWO(mutableIssue, user, log, WorkflowOperations.PWO22);
    }

    public boolean onStart1CustomScriptedCondition() {
        return WorkflowOperations.isPWO22Ready(issue, user, log);
    }

    public void onStart1CustomScriptPostFunction() {
        WorkflowOperations.processPWO22VerificationDates(mutableIssue, user, log);
    }

    public void onDeliver1CustomScriptPostFunction() {
        WorkflowOperations.processPWO22FormDeliveryDates(mutableIssue, user, log);
    }

    public void onDeliver2CustomScriptPostFunction() {
        WorkflowOperations.processPWO22NbReworkField(mutableIssue, user, log);
    }

    public void onReject1CustomScriptPostFunction() {
        WorkflowOperations.processPWO22FormReworkDate(mutableIssue, user, log);
    }

    public void onReject2CustomScriptPostFunction() {
        WorkflowOperations.processPWO22CommittedDates(mutableIssue, user, log);
    }
}
