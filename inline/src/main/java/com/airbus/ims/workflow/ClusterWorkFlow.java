package com.airbus.ims.workflow;

import com.airbus.ims.business.WorkflowOperations;
import com.atlassian.jira.issue.Issue;

class ClusterWorkFlow extends AbstractWorkflow {

    ClusterWorkFlow(Issue i) {
        super(i);
    }

    public boolean onCancelSimpleScriptedCondition(){
        return WorkflowOperations.checkIfStatusIsNotCompletedOrCancelled(issue);
    }

    public boolean onAutoCancelCustomScriptedCondition() {
        return WorkflowOperations.autoCancelCluster(mutableIssue, user, log);
    }
}