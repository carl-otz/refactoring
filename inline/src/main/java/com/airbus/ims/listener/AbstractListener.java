package com.airbus.ims.listener;

import com.airbus.ims.business.*;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.index.IndexException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

abstract class AbstractListener {

    public IssueEvent event;
    public Logger log;

    public static final String DEFAULT_LOGGER_NAME = "com.valiantys.script.listener";

    AbstractListener(IssueEvent e) {
        initWithDefaultParameters(e);
    }

    AbstractListener(IssueEvent e, String loggerName, Level level){
        initWithDefaultParameters(e);
        log = Logger.getLogger(loggerName);
        log.setLevel(level);
    }

    /**
     * Initialize the workflow with default parameters
     * @param e
     */
    private void initWithDefaultParameters(IssueEvent e){
        event = e;
        log = Logger.getLogger(DEFAULT_LOGGER_NAME);
        log.setLevel(Level.DEBUG);
    }

    public static void creationEditOfPwo2Pwo21(IssueEvent event, Logger log) throws IndexException {
        MutableIssue issue = (MutableIssue) event.getIssue();
        //Business
        if (issue.getIssueTypeId() == ConstantManager.ID_IT_PWO2) {
            PWO2BusinessEvent.resolveCreationOrEditionOnPWO2(event, log);
        } else if (issue.getIssueTypeId() == ConstantManager.ID_IT_PWO21) {
            PWO21BusinessEvent.resolveCreationOrEditionOnPWO21(event, log);
        }
    }

    public static void creationEditOfPwo22Pwo23(IssueEvent event, Logger log) throws IndexException {
        MutableIssue issue = (MutableIssue) event.getIssue();
        //Business
        if (issue.getIssueTypeId() == ConstantManager.ID_IT_PWO22) {
            PWO22BusinessEvent.resolveCreationOrEditionOnPWO22(event, log);
        } else if (issue.getIssueTypeId() == ConstantManager.ID_IT_PWO23) {
            PWO23BusinessEvent.resolveCreationOrEditionOnPWO23(event, log);
        }
    }

    public static void creationEditOfPwo1(IssueEvent event, Logger log) throws IndexException {
        MutableIssue issue = (MutableIssue) event.getIssue();
        //Business
        if (issue.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
            PWO1BusinessEvent.resolveCreationOrEditionOnPWO1(event, log);
        }
    }

    public static void creationEditOfPwo0(IssueEvent event, Logger log) throws IndexException {
        MutableIssue issue = (MutableIssue) event.getIssue();
        //Business
        if (issue.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
            PWO0BusinessEvent.resolveCreationOrEditionOnPWO0(event, log);
        }
    }
}
