package com.airbus.ims.listener;

import groovy.lang.Binding;
import groovy.lang.Script;

public class Example {
    public Object myfunction(Object param) {
        Object var = param.invokeMethod("trunc", new Object[0]);
        return var;
    }

}

public class Example extends Script {
    public static void main(String[] args) {
        new Example(new Binding(args)).run();
    }

    public Object run() {


        return invokeMethod("println", new Object[]{invokeMethod("myfunction", new Object[]{42.576135})});
    }

    public Example(Binding binding) {
        super(binding);
    }

    public Example() {
        super();
    }
}
