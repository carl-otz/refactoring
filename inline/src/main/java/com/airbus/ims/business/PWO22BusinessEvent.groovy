package com.airbus.ims.business;

import com.airbus.ims.util.Toolbox;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.changehistory.ChangeHistoryItem;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Projects : PWO
 * Description : Creation/Edit of PWO-2.2 / PWO-2.3
 * Events : Issue Updated, Admin re-open, PWO-2.2 updated, PWO-2.3 updated
 * isDisabled : null
 */
public class PWO22BusinessEvent {

    public static void createPWO22(MutableIssue issue , ApplicationUser user, Logger log) throws IndexException {
        //Manager;
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        GroupManager groupManager = ComponentAccessor.getGroupManager();

        //CustomField;
        CustomField iaCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_COMPANY);
        CustomField companyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_COMPANYALLOWED);
        CustomField authCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_COMPANY);
        CustomField authVerifCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFCOMPANY);
        CustomField formCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_COMPANY);
        CustomField formVerifCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFCOMPANY);
        CustomField intCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_COMPANY);
        CustomField authVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_COMPANYAUTHVERIF);
        CustomField formVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_COMPANYFORMVERIF);
        CustomField intCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INTCOMPANY);

        CustomField requestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_REQUESTEDDATE);

        CustomField form_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_REQUESTEDDATE);
        CustomField form_Verification_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFICATION_REQUESTEDDATE);
        CustomField form_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VALIDATION_REQUESTEDDATE);
        CustomField form_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_1STCOMMITTEDDATE);
        CustomField form_Verification_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFICATION_1STCOMMITTEDDATE);
        CustomField form_Validation_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VALIDATION_1STCOMMITTEDDATE);
        CustomField form_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_LASTCOMMITTEDDATE);
        CustomField form_Verification_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFICATION_LASTCOMMITTEDDATE);
        CustomField form_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VALIDATION_LASTCOMMITTEDDATE);

        //Variables;
        Date requestedDateTrigger = null;
        Date form_RequestedDate;
        Date form_Verification_RequestedDate;
        Date form_Validation_RequestedDate;
        Date form_LastCommittedDate;
        Date form_Verification_LastCommittedDate;
        Date form_Validation_LastCommittedDate;

        List<Group> companyAllowedList = new ArrayList<Group>();
        List<Group> authVerifCompanyAllowedList = new ArrayList<Group>();
        List<Group> formVerifCompanyAllowedList = new ArrayList<Group>();
        List<Group> intCompanyAllowedList = new ArrayList<Group>();


        //Get RequestedDate from Trigger;
        Issue issueParent = issue.getParentObject();
        Collection<Issue> linkedIssues = issueLinkManager.getLinkCollection(issueParent, user).getAllIssues();
        for (Issue linkedIssue : linkedIssues) {
            //If linked issue is Cluster;
            if (linkedIssue.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                Collection<Issue> linkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues();
                for (Issue linkedIssueOfPWO1 : linkedIssuesOfPWO1) {
                    if (linkedIssueOfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                        Collection<Issue> linkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfPWO1, user).getAllIssues();
                        for (Issue linkedIssueOfPW0 : linkedIssuesOfPWO0) {
                            if (linkedIssueOfPW0.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                Collection<Issue> linkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfPW0, user).getAllIssues();
                                for (Issue linkedIssueOfCluster : linkedIssuesOfCluster) {
                                    if (linkedIssueOfCluster.getIssueTypeId() == ConstantManager.ID_IT_TRIGGER) {
                                        if(requestedDateTrigger == null || requestedDateTrigger.after((Date)linkedIssueOfCluster.getCustomFieldValue(requestedDateField))){
                                            requestedDateTrigger = (Date) linkedIssueOfCluster.getCustomFieldValue(requestedDateField);
                                            log.debug("Requested date trigger : " + requestedDateTrigger);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (requestedDateTrigger != null) {
            //Calculate date for PWO-2.1;
            form_RequestedDate = Toolbox.incrementTimestamp(requestedDateTrigger, - 49);
            form_Verification_RequestedDate = Toolbox.incrementTimestamp(requestedDateTrigger, - 42);
            form_Validation_RequestedDate = Toolbox.incrementTimestamp(requestedDateTrigger, - 35);

            Date form_FirstCommittedDate = form_RequestedDate;;
            Date form_Verification_FirstCommittedDate = form_Verification_RequestedDate;;
            Date form_Validation_FirstCommittedDate = form_Validation_RequestedDate;;

            form_LastCommittedDate = form_FirstCommittedDate;
            form_Verification_LastCommittedDate = form_Verification_FirstCommittedDate;
            form_Validation_LastCommittedDate = form_Validation_FirstCommittedDate;

            issue.setCustomFieldValue(form_RequestedDateField, form_RequestedDate);
            issue.setCustomFieldValue(form_Verification_RequestedDateField, form_Verification_RequestedDate);
            issue.setCustomFieldValue(form_Validation_RequestedDateField, form_Validation_RequestedDate);

            issue.setCustomFieldValue(form_1stCommittedDateField, form_FirstCommittedDate);
            issue.setCustomFieldValue(form_Verification_1stCommittedDateField, form_Verification_FirstCommittedDate);
            issue.setCustomFieldValue(form_Validation_1stCommittedDateField, form_Validation_FirstCommittedDate);

            issue.setCustomFieldValue(form_LastCommittedDateField, form_LastCommittedDate);
            issue.setCustomFieldValue(form_Verification_LastCommittedDateField, form_Verification_LastCommittedDate);
            issue.setCustomFieldValue(form_Validation_LastCommittedDateField, form_Validation_LastCommittedDate);

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }

        Collection<Issue> linkedIssuesOfPWO22 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfPWO22 : linkedIssuesOfPWO22) {
            if (linkedIssueOfPWO22.getProjectId() == ConstantManager.ID_PR_AWO) {
                Collection<Issue> linkedIssuesOfAWOForCompany = issueLinkManager.getLinkCollection(linkedIssueOfPWO22, user).getAllIssues();
                for (Issue linkedIssueOfAWOForCompany : linkedIssuesOfAWOForCompany) {
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(iaCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(iaCompanyField) != "AIRBUS") {
                            String ia_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(iaCompanyField). toString();
                            String ia_Company_Formatted = "Partner_" + ia_Company_Value;

                            Group ia_Company_Management_Formatted_Group = groupManager.getGroup(ia_Company_Formatted);

                            if (!companyAllowedList.contains(ia_Company_Management_Formatted_Group)) {
                                companyAllowedList.add(ia_Company_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO21) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(authCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(authCompanyField) != "AIRBUS") {
                            String auth_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(authCompanyField).toString();
                            String auth_Company_Formatted = "Partner_" + auth_Company_Value;

                            Group auth_Company_Management_Formatted_Group = groupManager.getGroup(auth_Company_Formatted);

                            if (!companyAllowedList.contains(auth_Company_Management_Formatted_Group)) {
                                companyAllowedList.add(auth_Company_Management_Formatted_Group);
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(authVerifCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(authVerifCompanyField) != "AIRBUS") {
                            String auth_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(authVerifCompanyField).toString();
                            String auth_VerifCompany_Formatted = "Partner_" + auth_VerifCompany_Value;

                            Group auth_VerifCompany_Management_Formatted_Group = groupManager.getGroup(auth_VerifCompany_Formatted);

                            if (!companyAllowedList.contains(auth_VerifCompany_Management_Formatted_Group)) {
                                companyAllowedList.add(auth_VerifCompany_Management_Formatted_Group);
                            }
                            if (!authVerifCompanyAllowedList.contains(auth_VerifCompany_Management_Formatted_Group)) {
                                authVerifCompanyAllowedList.add(auth_VerifCompany_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO22) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(formCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(formCompanyField) != "AIRBUS") {
                            String form_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(formCompanyField).toString();
                            String form_Company_Formatted = "Partner_" + form_Company_Value;

                            Group form_Company_Management_Formatted_Group = groupManager.getGroup(form_Company_Formatted);

                            if (!companyAllowedList.contains(form_Company_Management_Formatted_Group)) {
                                companyAllowedList.add(form_Company_Management_Formatted_Group);
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(formVerifCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(formVerifCompanyField) != "AIRBUS") {
                            String form_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(formVerifCompanyField).toString();
                            String form_VerifCompany_Formatted = "Partner_" + form_VerifCompany_Value;

                            Group form_VerifCompany_Management_Formatted_Group = groupManager.getGroup(form_VerifCompany_Formatted);

                            if (!companyAllowedList.contains(form_VerifCompany_Management_Formatted_Group)) {
                                companyAllowedList.add(form_VerifCompany_Management_Formatted_Group);
                            }
                            if (!formVerifCompanyAllowedList.contains(form_VerifCompany_Management_Formatted_Group)) {
                                formVerifCompanyAllowedList.add(form_VerifCompany_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO23) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(intCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(intCompanyField) != "AIRBUS") {
                            String int_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(intCompanyField).toString();
                            String int_Company_Formatted = "Partner_" + int_Company_Value;

                            Group int_Company_Management_Formatted_Group = groupManager.getGroup(int_Company_Formatted);

                            if (!companyAllowedList.contains(int_Company_Management_Formatted_Group)) {
                                companyAllowedList.add(int_Company_Management_Formatted_Group);
                                intCompanyAllowedList.add(int_Company_Management_Formatted_Group);
                            }
                        }
                    }
                }
                if (companyAllowedList != null) {
                    MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfPWO22.getKey());
                    mutableIssue.setCustomFieldValue(companyAllowedField, companyAllowedList);
                    if (authVerifCompanyAllowedList != null) {
                        mutableIssue.setCustomFieldValue(authVerifCompanyAllowedField, authVerifCompanyAllowedList);
                    }
                    if (formVerifCompanyAllowedList != null) {
                        mutableIssue.setCustomFieldValue(formVerifCompanyAllowedField, formVerifCompanyAllowedList);
                    }
                    if (intCompanyAllowedList != null) {
                        mutableIssue.setCustomFieldValue(intCompanyAllowedField, intCompanyAllowedList);
                    }
                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                    issueIndexingService.reIndex(mutableIssue); //JIRA7
                    companyAllowedList.clear();
                    authVerifCompanyAllowedList.clear();
                    formVerifCompanyAllowedList.clear();
                    intCompanyAllowedList.clear();
                }
            }
        }
    }


    public static void editPWO22(IssueEvent event, Logger log) throws IndexException {
        //Manager
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        org.ofbiz.core.entity.GenericValue changeLog = event.getChangeLog();

        MutableIssue issue = (MutableIssue) event.getIssue();
        ApplicationUser user = event.getUser();

        //CustomField;
        CustomField formCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_COMPANY);
        CustomField formVerifCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFCOMPANY);

        CustomField form_VerifiedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFIED);
        CustomField form_ValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VALIDATED);

        //Variables;
        List<String> itemsChangedPWO22Company = new ArrayList<String>();
        itemsChangedPWO22Company.add(formCompanyField.getFieldName());
        itemsChangedPWO22Company.add(formVerifCompanyField.getFieldName());

        boolean isPWO22CompanyChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedPWO22Company);
        log.debug("PWO22 Company is changed : " + isPWO22CompanyChanged);
        if (isPWO22CompanyChanged) {
            firePWO22CompanyChanged(issue, user, log);
        }


        List<String> itemsChangedFormVerified = new ArrayList<String>();
        itemsChangedFormVerified.add(form_VerifiedField.getFieldName());

        boolean isFormVerifiedChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedFormVerified);
        if (isFormVerifiedChanged) {
            fireFormVerifiedChanged(issue, user, log);
        }


        List<String> itemsChangedFormValidated = new ArrayList<String>();
        itemsChangedFormValidated.add(form_ValidatedField.getFieldName());

        boolean isFormValidatedChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedFormValidated);
        if (isFormValidatedChanged) {
            fireFormValidatedChanged(issue, user, log);
        }

        //Set costs data on Trigger;
        setCostDataOnTrigger(issue, user, log);
    }

    /**
     * Set costs data on Trigger
     * @param issue
     * @param user
     * @param log
     */
    private static void setCostDataOnTrigger(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);


        //CustomField
        CustomField partsData_TimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_TIMESPENT);
        CustomField maintPlanning_TimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_TIMESPENT);
        CustomField maint_TimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_TIMESPENT);
        CustomField iaTimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_TIMESPENT);
        CustomField domainPWO1Field = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_DOMAINPWO1);
        CustomField authTimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_TIMESPENT);
        CustomField formTimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_TIMESPENT);
        CustomField intTimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_TIMESPENT);


        //Variables
        Long partsDataTimeSpentCurrent;
        Long maintPlanningTimeSpentCurrent;
        Long maintTimeSpentCurrent;
        Long iaTimeSpentCurrent;
        Long authTimeSpentCurrent;
        Long formTimeSpentCurrent;
        Long intTimeSpentCurrent;
        Long partsDataTimeSpent = Long.valueOf(0);
        Long maintPlanningTimeSpent = Long.valueOf(0);
        Long maintTimeSpent = Long.valueOf(0);
        String partsDataTimeSpentString;
        String maintPlanningTimeSpentString;

        Issue issueParent = issue.getParentObject();
        Collection<Issue> linkedIssues = issueLinkManager.getLinkCollection(issueParent, user).getAllIssues();
        for (Issue linkedIssue : linkedIssues) {
            if (linkedIssue.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                Collection<Issue> linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues();
                for (Issue linkedIssueOCurrentfPWO1 : linkedIssuesOfCurrentPWO1) {
                    if (linkedIssueOCurrentfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                        Collection<Issue> linkedIssuesOfCurrentPWO0 = issueLinkManager.getLinkCollection(linkedIssueOCurrentfPWO1, user).getAllIssues();
                        for (Issue linkedIssueOfCurrentPW0 : linkedIssuesOfCurrentPWO0) {
                            if (linkedIssueOfCurrentPW0.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                Collection<Issue> linkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPW0, user).getAllIssues();
                                for (Issue linkedIssueOfCluster : linkedIssuesOfCluster) {
                                    if (linkedIssueOfCluster.getIssueTypeId() == ConstantManager.ID_IT_TRIGGER) {
                                        //Reset variable;
                                        partsDataTimeSpent= Long.valueOf(0);
                                        maintPlanningTimeSpent= Long.valueOf(0);
                                        maintTimeSpent= Long.valueOf(0);
                                        int partsDataEstimatedCosts = 0;
                                        int maintPlanningEstimatedCosts = 0;
                                        int maintEstimatedCosts = 0;
                                        //Trigger;
                                        Collection<Issue> LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                                        for (Issue linkedIssueOfTrigger : LinkedIssuesOfTrigger) {
                                            if (linkedIssueOfTrigger.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                                Collection<Issue> LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                                for (Issue linkedIssueOfClusterTrigger : LinkedIssuesOfClusterTrigger) {
                                                    if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(partsData_TimeSpentField) != null) {
                                                            partsDataTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(partsData_TimeSpentField).toString());
                                                            partsDataTimeSpent = partsDataTimeSpent + partsDataTimeSpentCurrent;
                                                        }
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(maintPlanning_TimeSpentField) != null) {
                                                            maintPlanningTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(maintPlanning_TimeSpentField).toString());
                                                            maintPlanningTimeSpent = maintPlanningTimeSpent + maintPlanningTimeSpentCurrent;
                                                        }
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(maint_TimeSpentField) != null) {
                                                            maintTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(maint_TimeSpentField).toString());
                                                            maintTimeSpent = maintTimeSpent + maintTimeSpentCurrent;
                                                        }
                                                        Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                                        for (Issue linkedIssueOfPWO0 : LinkedIssuesOfPWO0) {
                                                            if (linkedIssueOfPWO0.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                                                                if (linkedIssueOfPWO0.getCustomFieldValue(iaTimeSpentField) != null) {
                                                                    iaTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfPWO0.getCustomFieldValue(iaTimeSpentField).toString());
                                                                } else {
                                                                    iaTimeSpentCurrent = null;
                                                                }

                                                                if (linkedIssueOfPWO0.getCustomFieldValue(domainPWO1Field).toString() == "Parts Data") {
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(iaTimeSpentField) != null) {
                                                                        partsDataTimeSpent = partsDataTimeSpent + iaTimeSpentCurrent;
                                                                    }
                                                                } else if (linkedIssueOfPWO0.getCustomFieldValue(domainPWO1Field).toString() == "Maintenance Planning") {
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(iaTimeSpentField) != null) {
                                                                        maintPlanningTimeSpent = maintPlanningTimeSpent + iaTimeSpentCurrent;
                                                                    }
                                                                } else if (linkedIssueOfPWO0.getCustomFieldValue(domainPWO1Field).toString() == "Maintenance") {
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(iaTimeSpentField) != null) {
                                                                        maintTimeSpent = maintTimeSpent + iaTimeSpentCurrent;
                                                                    }
                                                                }
                                                                Collection<Issue> linkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                                for (Issue linkedIssueOfPWO1 : linkedIssuesOfPWO1) {
                                                                    if (linkedIssueOfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO2) {
                                                                        String domainOfPWO2 = linkedIssueOfPWO1.getCustomFieldValue(domainPWO1Field).toString();
                                                                        //Get PWO-2's subtask;
                                                                        Collection<Issue> linkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects();
                                                                        for (Issue linkedIssueOfPWO2 : linkedIssuesOfPWO2) {
                                                                            if (linkedIssueOfPWO2.getIssueTypeId() == ConstantManager.ID_IT_PWO21) {
                                                                                if (linkedIssueOfPWO2.getCustomFieldValue(authTimeSpentField) != null) {
                                                                                    authTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(authTimeSpentField).toString());
                                                                                    //AuthTimeSpent=(double)linkedIssueOfPWO2.getCustomFieldValue(authTimeSpentField);
                                                                                } else {
                                                                                    authTimeSpentCurrent = null;
                                                                                }
                                                                                if (domainOfPWO2 == "Parts Data" && authTimeSpentCurrent != null) {
                                                                                    partsDataTimeSpent = partsDataTimeSpent + authTimeSpentCurrent;
                                                                                } else if (domainOfPWO2 == "Maintenance Planning" && authTimeSpentCurrent != null) {
                                                                                    maintPlanningTimeSpent = maintPlanningTimeSpent + authTimeSpentCurrent;
                                                                                } else if (domainOfPWO2 == "Maintenance" && authTimeSpentCurrent != null) {
                                                                                    maintTimeSpent = maintTimeSpent + authTimeSpentCurrent;
                                                                                }
                                                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ConstantManager.ID_IT_PWO22) {
                                                                                if (linkedIssueOfPWO2.getCustomFieldValue(formTimeSpentField) != null) {
                                                                                    formTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(formTimeSpentField).toString());
                                                                                    //FormTimeSpent=(double)linkedIssueOfPWO2.getCustomFieldValue(formTimeSpentField);
                                                                                } else {
                                                                                    formTimeSpentCurrent = null;
                                                                                }
                                                                                if (domainOfPWO2 == "Parts Data" && formTimeSpentCurrent != null) {
                                                                                    partsDataTimeSpent = partsDataTimeSpent + formTimeSpentCurrent;
                                                                                } else if (domainOfPWO2 == "Maintenance Planning" && formTimeSpentCurrent != null) {
                                                                                    maintPlanningTimeSpent = maintPlanningTimeSpent + formTimeSpentCurrent;
                                                                                } else if (domainOfPWO2 == "Maintenance" && formTimeSpentCurrent != null) {
                                                                                    maintTimeSpent = maintTimeSpent + formTimeSpentCurrent;
                                                                                }
                                                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ConstantManager.ID_IT_PWO23) {
                                                                                if (linkedIssueOfPWO2.getCustomFieldValue(intTimeSpentField) != null) {
                                                                                    intTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(intTimeSpentField).toString());
                                                                                } else {
                                                                                    intTimeSpentCurrent = null;
                                                                                }
                                                                                if (domainOfPWO2 == "Parts Data" && intTimeSpentCurrent != null) {
                                                                                    partsDataTimeSpent = partsDataTimeSpent + intTimeSpentCurrent;
                                                                                } else if (domainOfPWO2 == "Maintenance Planning" && intTimeSpentCurrent != null) {
                                                                                    maintPlanningTimeSpent = maintPlanningTimeSpent + intTimeSpentCurrent;
                                                                                } else if (domainOfPWO2 == "Maintenance" && intTimeSpentCurrent != null) {
                                                                                    maintTimeSpent = maintTimeSpent + intTimeSpentCurrent;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //Set data on trigger;
                                        MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfCluster.getKey());
                                        partsDataTimeSpentString = Toolbox.formatSecondesValueInHoursMinutes(partsDataTimeSpent);
                                        mutableIssue.setCustomFieldValue(partsData_TimeSpentField, partsDataTimeSpentString);
                                        maintPlanningTimeSpentString = Toolbox.formatSecondesValueInHoursMinutes(maintPlanningTimeSpent);
                                        mutableIssue.setCustomFieldValue(maintPlanning_TimeSpentField, maintPlanningTimeSpentString);
                                        String MaintTimeSpenString = Toolbox.formatSecondesValueInHoursMinutes(maintTimeSpent);
                                        mutableIssue.setCustomFieldValue(maint_TimeSpentField, MaintTimeSpenString);
                                        issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                        issueIndexingService.reIndex(mutableIssue);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private static void fireFormValidatedChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);


        //CustomField
        CustomField TechnicallyClosedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_TECHNICALLYCLOSED);
        CustomField TechnicalClosureDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_TECHNICALCLOSUREDATE);

        CustomField form_ValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VALIDATED);
        CustomField form_Validation_1stDeliveryField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VALIDATION_1STDELIVERY);
        CustomField form_Validation_LastDeliveryField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VALIDATION_LASTDELIVERY);
        CustomField form_ReworkDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_REWORKDATE);
        CustomField form_Verification_ReworkDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFICATION_REWORKDATE);


        //Variables;
        Date TechnicalClosureDate;
        String TechnicallyClosed = "No";
        Date FormValidation1stDeliveryDate;
        Date FormValidationLastDeliveryDate;
        Date form_ReworkDate;
        Date form_Verification_ReworkDate;

        Date TodayDate = new LocalDate(new Date()).toDate();
        Timestamp TodaysDate = new Timestamp(TodayDate.getTime());

        if (issue.getCustomFieldValue(form_ValidatedField).toString() == "Rejected") {
            if (issue.getCustomFieldValue(form_Validation_1stDeliveryField) == null) {
                FormValidation1stDeliveryDate = TodaysDate;
                issue.setCustomFieldValue(form_Validation_1stDeliveryField, FormValidation1stDeliveryDate);
            }
            FormValidationLastDeliveryDate = TodaysDate;
            issue.setCustomFieldValue(form_Validation_LastDeliveryField, FormValidationLastDeliveryDate);

            //form_LastCommittedDate=TodaysDate+7;
            //issue.setCustomFieldValue(form_LastCommittedDateField,form_LastCommittedDate);

            //form_Verification_LastCommittedDate=TodaysDate+14;
            //issue.setCustomFieldValue(form_Verification_LastCommittedDateField,form_Verification_LastCommittedDate);

            //form_Validation_LastCommittedDate=TodaysDate+21;
            //issue.setCustomFieldValue(form_Validation_LastCommittedDateField,form_Validation_LastCommittedDate);

            if (issue.getCustomFieldValue(form_ReworkDateField) != Toolbox.incrementTimestamp(TodaysDate, 7)) {
                form_ReworkDate = Toolbox.incrementTimestamp(TodaysDate, 7);
                issue.setCustomFieldValue(form_ReworkDateField, form_ReworkDate);
            }

            form_Verification_ReworkDate = Toolbox.incrementTimestamp(TodaysDate, 14);
            issue.setCustomFieldValue(form_Verification_ReworkDateField, form_Verification_ReworkDate);

            TechnicallyClosed = "No";
            TechnicalClosureDate = null;
        } else if (issue.getCustomFieldValue(form_ValidatedField).toString() == "Validated") {
            TechnicallyClosed = "Yes";
            TechnicalClosureDate = TodaysDate;

            if (issue.getCustomFieldValue(form_Validation_1stDeliveryField) == null) {
                FormValidation1stDeliveryDate = TodaysDate;
                issue.setCustomFieldValue(form_Validation_1stDeliveryField, FormValidation1stDeliveryDate);
            }
            FormValidationLastDeliveryDate = TodaysDate;
            issue.setCustomFieldValue(form_Validation_LastDeliveryField, FormValidationLastDeliveryDate);
        }

        FieldConfig fieldConfigTechnicallyClosed = TechnicallyClosedField.getRelevantConfig(issue);
        Option valueTechnicallyClosed = ComponentAccessor.getOptionsManager().getOptions(fieldConfigTechnicallyClosed)?.find{
            it.toString() == TechnicallyClosed;
        }
        issue.setCustomFieldValue(TechnicallyClosedField, valueTechnicallyClosed);

        issue.setCustomFieldValue(TechnicalClosureDateField, TechnicalClosureDate);

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
        issueIndexingService.reIndex(issue);
    }

    private static void fireFormVerifiedChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        List<ChangeHistoryItem> changeItems = ComponentAccessor.getChangeHistoryManager().getAllChangeItems(issue);

        //CustomField
        CustomField form_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VALIDATION_REQUESTEDDATE);
        CustomField form_Validation_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VALIDATION_1STCOMMITTEDDATE);
        CustomField form_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VALIDATION_LASTCOMMITTEDDATE);
        CustomField form_VerifiedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFIED);
        CustomField form_ValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VALIDATED);
        CustomField form_Verified_1stDeliveryField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFIED_1STDELIVERY);
        CustomField form_Verified_LastDeliveryField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFIED_LASTDELIVERY);


        //Variables
        Date form_Validation_RequestedDate;
        Date form_Validation_1stCommittedDate;
        Date form_Validation_LastCommittedDate;
        Date form_Verification1stDeliveryDate;
        Date form_VerificationLastDeliveryDate;

        Date TodayDate = new LocalDate(new Date()).toDate();
        Timestamp TodaysDate = new Timestamp(TodayDate.getTime());
        Double Compteur = Double.valueOf(0);


        if (issue.getCustomFieldValue(form_VerifiedField).toString() == "Rejected") {
            if (issue.getCustomFieldValue(form_Verified_1stDeliveryField) == null) {
                form_Verification1stDeliveryDate = TodaysDate;
                issue.setCustomFieldValue(form_Verified_1stDeliveryField, form_Verification1stDeliveryDate);
            }
            form_VerificationLastDeliveryDate = TodaysDate;
            issue.setCustomFieldValue(form_Verified_LastDeliveryField, form_VerificationLastDeliveryDate);

            FieldConfig fieldConfigFormValidated = form_ValidatedField.getRelevantConfig(issue);
            Option valueFormValidated = ComponentAccessor.getOptionsManager().getOptions(fieldConfigFormValidated)?.find {
                it.toString() == "Validation pending";
            }
            issue.setCustomFieldValue(form_ValidatedField, valueFormValidated);
        }
        if (issue.getCustomFieldValue(form_VerifiedField).toString() == "Verified") {
            //if (!issue.getCustomFieldValue(form_Verified_1stDeliveryField)) {
            for (ChangeHistoryItem item : changeItems) {
                if (item.getField() == "form_Verified" && item.getTos().values().contains("Verified")) {
                    Compteur = Compteur + 1;
                }
            }
            if(Compteur<2){
                form_Validation_RequestedDate = Toolbox.incrementTimestamp(TodaysDate, 7);
                form_Validation_1stCommittedDate = form_Validation_RequestedDate;
                form_Validation_LastCommittedDate = Toolbox.incrementTimestamp(TodaysDate, 7);


                issue.setCustomFieldValue(form_Validation_RequestedDateField, form_Validation_RequestedDate);
                issue.setCustomFieldValue(form_Validation_1stCommittedDateField, form_Validation_1stCommittedDate);
                issue.setCustomFieldValue(form_Validation_LastCommittedDateField, form_Validation_LastCommittedDate);

            }
            if (issue.getCustomFieldValue(form_Verified_1stDeliveryField) == null) {
                form_Verification1stDeliveryDate = TodaysDate;
                issue.setCustomFieldValue(form_Verified_1stDeliveryField, form_Verification1stDeliveryDate);
            }

            form_VerificationLastDeliveryDate = TodaysDate;
            issue.setCustomFieldValue(form_Verified_LastDeliveryField, form_VerificationLastDeliveryDate);


            FieldConfig fieldConfigFormValidated = form_ValidatedField.getRelevantConfig(issue);
            Option valueFormValidated = ComponentAccessor.getOptionsManager().getOptions(fieldConfigFormValidated)?.find{
                it.toString() == "Validation pending";
            }
            issue.setCustomFieldValue(form_ValidatedField, valueFormValidated);
        }
        if (issue.getCustomFieldValue(form_ValidatedField).toString() != "Verification pending") {
            form_VerificationLastDeliveryDate = TodaysDate;
            issue.setCustomFieldValue(form_Verified_LastDeliveryField, form_VerificationLastDeliveryDate);
        }
        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
        issueIndexingService.reIndex(issue);
    }

    private static void firePWO22CompanyChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {

        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        GroupManager groupManager = ComponentAccessor.getGroupManager();

        //CustomField
        CustomField iaCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_COMPANY);
        CustomField companyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_COMPANYALLOWED);
        CustomField authCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_COMPANY);
        CustomField authVerifCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFCOMPANY);
        CustomField formCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_COMPANY);
        CustomField formVerifCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFCOMPANY);
        CustomField intCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_COMPANY);
        CustomField authVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_COMPANYAUTHVERIF);
        CustomField formVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_COMPANYFORMVERIF);
        CustomField intCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INTCOMPANY);

        //Variables
        List<Group> companyAllowedList = new ArrayList<Group>();
        List<Group> authVerifCompanyAllowedList = new ArrayList<Group>();
        List<Group> formVerifCompanyAllowedList = new ArrayList<Group>();
        List<Group> intCompanyAllowedList = new ArrayList<Group>();

        Collection<Issue> linkedIssuesOfPWO22 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfPWO22 : linkedIssuesOfPWO22) {
            if (linkedIssueOfPWO22.getProjectId() == ConstantManager.ID_PR_AWO) {
                log.debug("AWO found : " + linkedIssueOfPWO22.getKey());
                    Collection<Issue> LinkedIssuesOfAWOForCompany = issueLinkManager.getLinkCollection(linkedIssueOfPWO22, user).getAllIssues();
                for (Issue linkedIssueOfAWOForCompany : LinkedIssuesOfAWOForCompany) {
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(iaCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(iaCompanyField) != "AIRBUS") {
                            String ia_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(iaCompanyField).toString();
                            String ia_Company_Formatted = "Partner_" + ia_Company_Value;

                            Group ia_Company_Management_Formatted_Group = groupManager.getGroup(ia_Company_Formatted);

                            if (!companyAllowedList.contains(ia_Company_Management_Formatted_Group)) {
                                companyAllowedList.add(ia_Company_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO21) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(authCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(authCompanyField) != "AIRBUS") {
                            String auth_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(authCompanyField).toString();
                            String auth_Company_Formatted = "Partner_" + auth_Company_Value;

                            Group auth_Company_Management_Formatted_Group = groupManager.getGroup(auth_Company_Formatted);

                            if (!companyAllowedList.contains(auth_Company_Management_Formatted_Group)) {
                                companyAllowedList.add(auth_Company_Management_Formatted_Group);
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(authVerifCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(authVerifCompanyField) != "AIRBUS") {
                            String auth_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(authVerifCompanyField).toString();
                            String auth_VerifCompany_Formatted = "Partner_" + auth_VerifCompany_Value;

                            Group auth_VerifCompany_Management_Formatted_Group = groupManager.getGroup(auth_VerifCompany_Formatted);

                            if (!companyAllowedList.contains(auth_VerifCompany_Management_Formatted_Group)) {
                                companyAllowedList.add(auth_VerifCompany_Management_Formatted_Group);
                            }
                            if (!authVerifCompanyAllowedList.contains(auth_VerifCompany_Management_Formatted_Group)) {
                                authVerifCompanyAllowedList.add(auth_VerifCompany_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO22) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(formCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(formCompanyField) != "AIRBUS") {
                            String form_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(formCompanyField).toString();
                            String form_Company_Formatted = "Partner_" + form_Company_Value;

                            Group form_Company_Management_Formatted_Group = groupManager.getGroup(form_Company_Formatted);

                            if (!companyAllowedList.contains(form_Company_Management_Formatted_Group)) {
                                companyAllowedList.add(form_Company_Management_Formatted_Group);
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(formVerifCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(formVerifCompanyField) != "AIRBUS") {
                            String form_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(formVerifCompanyField).toString();
                            String form_VerifCompany_Formatted = "Partner_" + form_VerifCompany_Value;

                            Group form_VerifCompany_Management_Formatted_Group = groupManager.getGroup(form_VerifCompany_Formatted);

                            if (!companyAllowedList.contains(form_VerifCompany_Management_Formatted_Group)) {
                                companyAllowedList.add(form_VerifCompany_Management_Formatted_Group);
                            }
                            if (!formVerifCompanyAllowedList.contains(form_VerifCompany_Management_Formatted_Group)) {
                                formVerifCompanyAllowedList.add(form_VerifCompany_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO23) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(intCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(intCompanyField) != "AIRBUS") {
                            String int_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(intCompanyField).toString();
                            String int_Company_Formatted = "Partner_" + int_Company_Value;

                            Group int_Company_Management_Formatted_Group = groupManager.getGroup(int_Company_Formatted);

                            if (!companyAllowedList.contains(int_Company_Management_Formatted_Group)) {
                                companyAllowedList.add(int_Company_Management_Formatted_Group);
                                intCompanyAllowedList.add(int_Company_Management_Formatted_Group);
                            }
                        }
                    }

                }
                if (companyAllowedList != null) {
                    MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfPWO22.getKey());
                    mutableIssue.setCustomFieldValue(companyAllowedField, companyAllowedList);
                    if (authVerifCompanyAllowedList != null) {
                        mutableIssue.setCustomFieldValue(authVerifCompanyAllowedField, authVerifCompanyAllowedList);
                    }
                    if (formVerifCompanyAllowedList != null) {
                        mutableIssue.setCustomFieldValue(formVerifCompanyAllowedField, formVerifCompanyAllowedList);
                    }
                    if (intCompanyAllowedList != null) {
                        mutableIssue.setCustomFieldValue(intCompanyAllowedField, intCompanyAllowedList);
                    }
                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false); //JIRA7;
                    issueIndexingService.reIndex(mutableIssue); //JIRA7
                    companyAllowedList.clear();
                    authVerifCompanyAllowedList.clear();
                    formVerifCompanyAllowedList.clear();
                    intCompanyAllowedList.clear();
                }
            }
        }
    }



    public static void resolveCreationOrEditionOnPWO22(IssueEvent event, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        //CustomField
        CustomField firstSNAppliField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FIRSTSNAPPLI);

        List<String> itemsChangedAtCreation = new ArrayList<String>();
        itemsChangedAtCreation.add(firstSNAppliField.getFieldName());
        Issue issue = event.getIssue();
        MutableIssue mutableIssue = issueManager.getIssueObject(event.getIssue().getKey());
        log.debug("issue = " + issue.getKey());

        boolean isCreation = Toolbox.workloadIsUpdated(event.getChangeLog(), itemsChangedAtCreation);
        log.debug("is creation : " + isCreation);
        if (isCreation || event.getEventTypeId() == ConstantManager.ID_EVENT_ADMINREOPEN) {//CREATION
            createPWO22(mutableIssue, event.getUser(), log);
        } else {//EDIT
            editPWO22(event, log);
        }
    }
}