package com.airbus.ims.business;

import com.airbus.ims.util.Toolbox;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static com.airbus.ims.business.ConstantManager.ID_IT_PWO22;

/**
 * Projects : PWO
 * Description : Creation/Edit of PWO-2.2 / PWO-2.3
 * Events : Issue Updated, Admin re-open, PWO-2.2 updated, PWO-2.3 updated
 * isDisabled : null
 */
public class PWO23BusinessEvent {


    public static void resolveCreationOrEditionOnPWO23(IssueEvent event, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        //CustomField
        CustomField firstSNAppliField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FIRSTSNAPPLI);

        List<String> itemsChangedAtCreation = new ArrayList<String>();
        itemsChangedAtCreation.add(firstSNAppliField.getFieldName());
        Issue issue = event.getIssue();
        MutableIssue mutableIssue = issueManager.getIssueObject(event.getIssue().getKey());
        log.debug("issue = " + issue.getKey());

        boolean isCreation = Toolbox.workloadIsUpdated(event.getChangeLog(), itemsChangedAtCreation);
        log.debug("is creation : " + isCreation);
        if (isCreation || event.getEventTypeId() == ConstantManager.ID_EVENT_ADMINREOPEN) {//CREATION
            createPWO23(mutableIssue, event.getUser(), log);
        } else {//EDIT
            editPWO23(event, log);
        }
    }


    private static void createPWO23(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager;
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        GroupManager groupManager = ComponentAccessor.getGroupManager();

        //CustomField
        CustomField IACompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_COMPANY);
        CustomField CompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_COMPANYALLOWED);
        CustomField AuthCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_COMPANY);
        CustomField AuthVerifCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFCOMPANY);
        CustomField FormCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_COMPANY);
        CustomField FormVerifCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFCOMPANY);
        CustomField IntCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_COMPANY);
        CustomField AuthVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_COMPANYAUTHVERIF);
        CustomField FormVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_COMPANYFORMVERIF);
        CustomField IntCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INTCOMPANY);
        CustomField RequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_REQUESTEDDATE);

        CustomField Int_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_REQUESTEDDATE);
        CustomField Int_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_1STCOMMITTEDDATE);
        CustomField Int_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_LASTCOMMITTEDDATE);


        //Variables
        Date RequestedDateTrigger = null;
        Date Int_RequestedDate;
        Date Int_FirstCommittedDate;
        Date Int_LastCommittedDate;

        List<Group> CompanyAllowedList = new ArrayList<Group>();
        List<Group> AuthVerifCompanyAllowedList = new ArrayList<Group>();
        List<Group> FormVerifCompanyAllowedList = new ArrayList<Group>();
        List<Group> IntCompanyAllowedList = new ArrayList<Group>();


        //Get RequestedDate from Trigger;
        Issue issueParent = issue.getParentObject();
        Collection<Issue> LinkedIssues = issueLinkManager.getLinkCollection(issueParent, user).getAllIssues();
        for (Issue linkedIssue : LinkedIssues) {
            //If linked issue is Cluster;
            if (linkedIssue.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues();
                for (Issue LinkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
                    if (LinkedIssueOfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                        Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(LinkedIssueOfPWO1, user).getAllIssues();
                        for (Issue LinkedIssueOfPW0 : LinkedIssuesOfPWO0) {
                            if (LinkedIssueOfPW0.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(LinkedIssueOfPW0, user).getAllIssues();
                                for (Issue LinkedIssueOfCluster : LinkedIssuesOfCluster) {
                                    if (LinkedIssueOfCluster.getIssueTypeId() == ConstantManager.ID_IT_TRIGGER) {
                                        if(RequestedDateTrigger == null || RequestedDateTrigger.after((Date)LinkedIssueOfCluster.getCustomFieldValue(RequestedDateField))){
                                            RequestedDateTrigger = (Date) LinkedIssueOfCluster.getCustomFieldValue(RequestedDateField);
                                            log.debug("Requested date trigger : " + RequestedDateTrigger);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (RequestedDateTrigger != null) {
            //Calculate date for PWO-2.1;
            Int_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 28);
            Int_FirstCommittedDate = Int_RequestedDate;
            Int_LastCommittedDate = Int_FirstCommittedDate;

            issue.setCustomFieldValue(Int_RequestedDateField, Int_RequestedDate);
            issue.setCustomFieldValue(Int_1stCommittedDateField, Int_FirstCommittedDate);
            issue.setCustomFieldValue(Int_LastCommittedDateField, Int_LastCommittedDate);

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }

        Collection<Issue> LinkedIssuesOfPWO23 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfPWO23 : LinkedIssuesOfPWO23) {
            if (linkedIssueOfPWO23.getProjectId() == ConstantManager.ID_PR_AWO) {
                Collection<Issue> LinkedIssuesOfAWOForCompany = issueLinkManager.getLinkCollection(linkedIssueOfPWO23, user).getAllIssues();
                for (Issue linkedIssueOfAWOForCompany : LinkedIssuesOfAWOForCompany) {
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField) != "AIRBUS") {
                            String IA_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField).toString();
                            String IA_Company_Formatted = "Partner_" + IA_Company_Value;

                            Group IA_Company_Management_Formatted_Group = groupManager.getGroup(IA_Company_Formatted);

                            if (!CompanyAllowedList.contains(IA_Company_Formatted)) {
                                CompanyAllowedList.add(IA_Company_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO21) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField) != "AIRBUS") {
                            String Auth_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField).toString();
                            String Auth_Company_Formatted = "Partner_" + Auth_Company_Value;

                            Group Auth_Company_Management_Formatted_Group = groupManager.getGroup(Auth_Company_Formatted);

                            if (!CompanyAllowedList.contains(Auth_Company_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Auth_Company_Management_Formatted_Group);
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField) != "AIRBUS") {
                            String Auth_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField).toString();
                            String Auth_VerifCompany_Formatted = "Partner_" + Auth_VerifCompany_Value;

                            Group Auth_VerifCompany_Management_Formatted_Group = groupManager.getGroup(Auth_VerifCompany_Formatted);

                            if (!CompanyAllowedList.contains(Auth_VerifCompany_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Auth_VerifCompany_Management_Formatted_Group);
                            }
                            if (!AuthVerifCompanyAllowedList.contains(Auth_VerifCompany_Management_Formatted_Group)) {
                                AuthVerifCompanyAllowedList.add(Auth_VerifCompany_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO22) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField) != "AIRBUS") {
                            String Form_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField).toString();
                            String Form_Company_Formatted = "Partner_" + Form_Company_Value;

                            Group Form_Company_Management_Formatted_Group = groupManager.getGroup(Form_Company_Formatted);

                            if (!CompanyAllowedList.contains(Form_Company_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Form_Company_Management_Formatted_Group);
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField) != "AIRBUS") {
                            String Form_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField).toString();
                            String Form_VerifCompany_Formatted = "Partner_" + Form_VerifCompany_Value;

                            Group Form_VerifCompany_Management_Formatted_Group = groupManager.getGroup(Form_VerifCompany_Formatted);

                            if (!CompanyAllowedList.contains(Form_VerifCompany_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Form_VerifCompany_Management_Formatted_Group);
                            }
                            if (!FormVerifCompanyAllowedList.contains(Form_VerifCompany_Management_Formatted_Group)) {
                                FormVerifCompanyAllowedList.add(Form_VerifCompany_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO23) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField) != "AIRBUS") {
                            String Int_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField).toString();
                            String Int_Company_Formatted = "Partner_" + Int_Company_Value;

                            Group Int_Company_Management_Formatted_Group = groupManager.getGroup(Int_Company_Formatted);

                            if (!CompanyAllowedList.contains(Int_Company_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Int_Company_Management_Formatted_Group);
                                IntCompanyAllowedList.add(Int_Company_Management_Formatted_Group);
                            }
                        }
                    }
                }
                if (CompanyAllowedList != null) {
                    MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfPWO23.getKey());
                    mutableIssue.setCustomFieldValue(CompanyAllowedField, CompanyAllowedList);
                    if (AuthVerifCompanyAllowedList != null) {
                        mutableIssue.setCustomFieldValue(AuthVerifCompanyAllowedField, AuthVerifCompanyAllowedList);
                    }
                    if (FormVerifCompanyAllowedList != null) {
                        mutableIssue.setCustomFieldValue(FormVerifCompanyAllowedField, FormVerifCompanyAllowedList);
                    }
                    if (IntCompanyAllowedList != null) {
                        mutableIssue.setCustomFieldValue(IntCompanyAllowedField, IntCompanyAllowedList);
                    }
                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
//JIRA7;
                    issueIndexingService.reIndex(mutableIssue); //JIRA7;
                    CompanyAllowedList.clear();
                    AuthVerifCompanyAllowedList.clear();
                    FormVerifCompanyAllowedList.clear();
                    IntCompanyAllowedList.clear();
                }
            }
        }
    }

    private static void editPWO23(IssueEvent event, Logger log) throws IndexException {
        //Manager
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        org.ofbiz.core.entity.GenericValue changeLog = event.getChangeLog();

        MutableIssue issue = (MutableIssue) event.getIssue();
        ApplicationUser user = event.getUser();

        //CustomField;
        CustomField IntCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_COMPANY);


        List<String> itemsChangedPWO23Company = new ArrayList<String>();
        itemsChangedPWO23Company.add(IntCompanyField.getFieldName());
        log.debug("int company field value : " + issue.getCustomFieldValue(IntCompanyField));

        boolean isPWO23CompanyChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedPWO23Company);
        log.debug("PWO23 Company is changed : " + isPWO23CompanyChanged);
        if (isPWO23CompanyChanged) {
            fireCompanyChanged(issue, user, log);
        }

        //Set costs data on Trigger;
        setCostsDataOnTrigger(issue, user, log);
    }

    private static void setCostsDataOnTrigger(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField;
        CustomField PartsData_TimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_TIMESPENT);
        CustomField MaintPlanning_TimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_TIMESPENT);
        CustomField Maint_TimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_TIMESPENT);
        CustomField IATimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_TIMESPENT);
        CustomField DomainPWO1Field = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_DOMAINPWO1);
        CustomField AuthTimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_TIMESPENT);
        CustomField FormTimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_TIMESPENT);
        CustomField IntTimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_TIMESPENT);

        //Variables
        Long PartsDataTimeSpentCurrent;
        Long MaintPlanningTimeSpentCurrent;
        Long MaintTimeSpentCurrent;
        Long IATimeSpentCurrent;
        Long AuthTimeSpentCurrent;
        Long FormTimeSpentCurrent;
        Long IntTimeSpentCurrent;
        Long PartsDataTimeSpent = Long.valueOf(0);
        Long MaintPlanningTimeSpent = Long.valueOf(0);
        Long MaintTimeSpent = Long.valueOf(0);
        String PartsDataTimeSpentString;
        String MaintPlanningTimeSpentString;

        Issue issueParent = issue.getParentObject();
        Collection<Issue> LinkedIssues = issueLinkManager.getLinkCollection(issueParent, user).getAllIssues();
        for (Issue linkedIssue : LinkedIssues) {
            if (linkedIssue.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                Collection<Issue> LinkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues();
                for (Issue LinkedIssueOCurrentfPWO1 : LinkedIssuesOfCurrentPWO1) {
                    if (LinkedIssueOCurrentfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                        Collection<Issue> LinkedIssuesOfCurrentPWO0 = issueLinkManager.getLinkCollection(LinkedIssueOCurrentfPWO1, user).getAllIssues();
                        for (Issue LinkedIssueOfCurrentPW0 : LinkedIssuesOfCurrentPWO0) {
                            if (LinkedIssueOfCurrentPW0.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(LinkedIssueOfCurrentPW0, user).getAllIssues();
                                for (Issue LinkedIssueOfCluster : LinkedIssuesOfCluster) {
                                    if (LinkedIssueOfCluster.getIssueTypeId() == ConstantManager.ID_IT_TRIGGER) {
                                        //Reset variable;
                                        PartsDataTimeSpent= Long.valueOf(0);
                                        MaintPlanningTimeSpent= Long.valueOf(0);
                                        MaintTimeSpent= Long.valueOf(0);
                                        int PartsDataEstimatedCosts = 0;
                                        int MaintPlanningEstimatedCosts = 0;
                                        int MaintEstimatedCosts = 0;
                                        //Trigger;
                                        log.debug("Trigger is : " + LinkedIssueOfCluster.getKey());
                                        Collection<Issue> LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues();
                                        for (Issue linkedIssueOfTrigger : LinkedIssuesOfTrigger) {
                                            if (linkedIssueOfTrigger.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                                Collection<Issue> LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                                for (Issue linkedIssueOfClusterTrigger : LinkedIssuesOfClusterTrigger) {
                                                    if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_TimeSpentField) != null) {
                                                            PartsDataTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_TimeSpentField).toString());
                                                            PartsDataTimeSpent = PartsDataTimeSpent + PartsDataTimeSpentCurrent;
                                                        }
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_TimeSpentField) != null) {
                                                            MaintPlanningTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_TimeSpentField).toString());
                                                            MaintPlanningTimeSpent = MaintPlanningTimeSpent + MaintPlanningTimeSpentCurrent;
                                                        }
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_TimeSpentField) != null) {
                                                            MaintTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_TimeSpentField).toString());
                                                            MaintTimeSpent = MaintTimeSpent + MaintTimeSpentCurrent;
                                                        }
                                                        Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                                        for (Issue linkedIssueOfPWO0 : LinkedIssuesOfPWO0) {
                                                            if (linkedIssueOfPWO0.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                                                                if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField) != null) {
                                                                    IATimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField).toString());
                                                                } else {
                                                                    IATimeSpentCurrent = null;
                                                                }

                                                                if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Parts Data") {
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField) != null) {
                                                                        PartsDataTimeSpent = PartsDataTimeSpent + IATimeSpentCurrent;
                                                                    }
                                                                } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance Planning") {
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField) != null) {
                                                                        MaintPlanningTimeSpent = MaintPlanningTimeSpent + IATimeSpentCurrent;
                                                                    }
                                                                } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance") {
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField) != null) {
                                                                        MaintTimeSpent = MaintTimeSpent + IATimeSpentCurrent;
                                                                    }
                                                                }
                                                                Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                                for (Issue linkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
                                                                    if (linkedIssueOfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO2) {
                                                                        String DomainOfPWO2 = linkedIssueOfPWO1.getCustomFieldValue(DomainPWO1Field).toString();
                                                                        //Get PWO-2's subtask;
                                                                        Collection<Issue> LinkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects();
                                                                        for (Issue linkedIssueOfPWO2 : LinkedIssuesOfPWO2) {
                                                                            if (linkedIssueOfPWO2.getIssueTypeId() == ConstantManager.ID_IT_PWO21) {
                                                                                if (linkedIssueOfPWO2.getCustomFieldValue(AuthTimeSpentField) != null) {
                                                                                    AuthTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(AuthTimeSpentField).toString());
                                                                                    //AuthTimeSpent=(double)linkedIssueOfPWO2.getCustomFieldValue(AuthTimeSpentField);
                                                                                } else {
                                                                                    AuthTimeSpentCurrent = null;
                                                                                }
                                                                                if (DomainOfPWO2 == "Parts Data" && AuthTimeSpentCurrent != null) {
                                                                                    PartsDataTimeSpent = PartsDataTimeSpent + AuthTimeSpentCurrent;
                                                                                } else if (DomainOfPWO2 == "Maintenance Planning" && AuthTimeSpentCurrent != null) {
                                                                                    MaintPlanningTimeSpent = MaintPlanningTimeSpent + AuthTimeSpentCurrent;
                                                                                } else if (DomainOfPWO2 == "Maintenance" && AuthTimeSpentCurrent != null) {
                                                                                    MaintTimeSpent = MaintTimeSpent + AuthTimeSpentCurrent;
                                                                                }
                                                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO22) {
                                                                                if (linkedIssueOfPWO2.getCustomFieldValue(FormTimeSpentField) != null) {
                                                                                    FormTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(FormTimeSpentField).toString());
                                                                                    //FormTimeSpent=(double)linkedIssueOfPWO2.getCustomFieldValue(FormTimeSpentField);
                                                                                } else {
                                                                                    FormTimeSpentCurrent = null;
                                                                                }
                                                                                if (DomainOfPWO2 == "Parts Data" && FormTimeSpentCurrent  != null) {
                                                                                    PartsDataTimeSpent = PartsDataTimeSpent + FormTimeSpentCurrent;
                                                                                } else if (DomainOfPWO2 == "Maintenance Planning" && FormTimeSpentCurrent  != null) {
                                                                                    MaintPlanningTimeSpent = MaintPlanningTimeSpent + FormTimeSpentCurrent;
                                                                                } else if (DomainOfPWO2 == "Maintenance" && FormTimeSpentCurrent  != null) {
                                                                                    MaintTimeSpent = MaintTimeSpent + FormTimeSpentCurrent;
                                                                                }
                                                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ConstantManager.ID_IT_PWO23) {
                                                                                if (linkedIssueOfPWO2.getCustomFieldValue(IntTimeSpentField) != null) {
                                                                                    IntTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(IntTimeSpentField).toString());
                                                                                } else {
                                                                                    IntTimeSpentCurrent = null;
                                                                                }
                                                                                if (DomainOfPWO2 == "Parts Data" && IntTimeSpentCurrent != null) {
                                                                                    PartsDataTimeSpent = PartsDataTimeSpent + IntTimeSpentCurrent;
                                                                                } else if (DomainOfPWO2 == "Maintenance Planning" && IntTimeSpentCurrent != null) {
                                                                                    MaintPlanningTimeSpent = MaintPlanningTimeSpent + IntTimeSpentCurrent;
                                                                                } else if (DomainOfPWO2 == "Maintenance" && IntTimeSpentCurrent != null) {
                                                                                    MaintTimeSpent = MaintTimeSpent + IntTimeSpentCurrent;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //Set data on trigger;
                                        MutableIssue mutableIssue = issueManager.getIssueObject(LinkedIssueOfCluster.getKey());
                                        PartsDataTimeSpentString = Toolbox.formatSecondesValueInHoursMinutes(PartsDataTimeSpent);
                                        mutableIssue.setCustomFieldValue(PartsData_TimeSpentField, PartsDataTimeSpentString);
                                        MaintPlanningTimeSpentString = Toolbox.formatSecondesValueInHoursMinutes(MaintPlanningTimeSpent);
                                        mutableIssue.setCustomFieldValue(MaintPlanning_TimeSpentField, MaintPlanningTimeSpentString);
                                        String MaintTimeSpenString = Toolbox.formatSecondesValueInHoursMinutes(MaintTimeSpent);
                                        mutableIssue.setCustomFieldValue(Maint_TimeSpentField, MaintTimeSpenString);
                                        issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                        issueIndexingService.reIndex(mutableIssue);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private static void fireCompanyChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        GroupManager groupManager = ComponentAccessor.getGroupManager();

        //CustomField;
        CustomField IACompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_COMPANY);
        CustomField CompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_COMPANYALLOWED);
        CustomField AuthCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_COMPANY);
        CustomField AuthVerifCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFCOMPANY);
        CustomField FormCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_COMPANY);
        CustomField FormVerifCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFCOMPANY);
        CustomField IntCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_COMPANY);
        CustomField AuthVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_COMPANYAUTHVERIF);
        CustomField FormVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_COMPANYFORMVERIF);
        CustomField IntCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INTCOMPANY);

        //Variables
        List<Group> CompanyAllowedList = new ArrayList<Group>();
        List<Group> AuthVerifCompanyAllowedList = new ArrayList<Group>();
        List<Group> FormVerifCompanyAllowedList = new ArrayList<Group>();
        List<Group> IntCompanyAllowedList = new ArrayList<Group>();

        Collection<Issue> LinkedIssuesOfPWO23 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfPWO23 : LinkedIssuesOfPWO23) {
            if (linkedIssueOfPWO23.getProjectId() == ConstantManager.ID_PR_AWO) {
                log.debug("AWO found : " + linkedIssueOfPWO23.getKey());
                Collection<Issue> LinkedIssuesOfAWOForCompany = issueLinkManager.getLinkCollection(linkedIssueOfPWO23, user).getAllIssues();
                for (Issue linkedIssueOfAWOForCompany : LinkedIssuesOfAWOForCompany) {
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField) != "AIRBUS") {
                            String IA_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField).toString();
                            String IA_Company_Formatted = "Partner_" + IA_Company_Value;

                            Group IA_Company_Management_Formatted_Group = groupManager.getGroup(IA_Company_Formatted);

                            if (!CompanyAllowedList.contains(IA_Company_Management_Formatted_Group)) {
                                CompanyAllowedList.add(IA_Company_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO21) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField) != "AIRBUS") {
                            String Auth_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField).toString();
                            String Auth_Company_Formatted = "Partner_" + Auth_Company_Value;

                            Group Auth_Company_Management_Formatted_Group = groupManager.getGroup(Auth_Company_Formatted);

                            if (!CompanyAllowedList.contains(Auth_Company_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Auth_Company_Management_Formatted_Group);
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField) != "AIRBUS") {
                            String Auth_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField).toString();
                            String Auth_VerifCompany_Formatted = "Partner_" + Auth_VerifCompany_Value;

                            Group Auth_VerifCompany_Management_Formatted_Group = groupManager.getGroup(Auth_VerifCompany_Formatted);

                            if (!CompanyAllowedList.contains(Auth_VerifCompany_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Auth_VerifCompany_Management_Formatted_Group);
                            }
                            if (!AuthVerifCompanyAllowedList.contains(Auth_VerifCompany_Management_Formatted_Group)) {
                                AuthVerifCompanyAllowedList.add(Auth_VerifCompany_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO22) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField) != "AIRBUS") {
                            String Form_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField).toString();
                            String Form_Company_Formatted = "Partner_" + Form_Company_Value;

                            Group Form_Company_Management_Formatted_Group = groupManager.getGroup(Form_Company_Formatted);

                            if (!CompanyAllowedList.contains(Form_Company_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Form_Company_Management_Formatted_Group);
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField) != "AIRBUS") {
                            String Form_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField).toString();
                            String Form_VerifCompany_Formatted = "Partner_" + Form_VerifCompany_Value;

                            Group Form_VerifCompany_Management_Formatted_Group = groupManager.getGroup(Form_VerifCompany_Formatted);

                            if (!CompanyAllowedList.contains(Form_VerifCompany_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Form_VerifCompany_Management_Formatted_Group);
                            }
                            if (!FormVerifCompanyAllowedList.contains(Form_VerifCompany_Management_Formatted_Group)) {
                                FormVerifCompanyAllowedList.add(Form_VerifCompany_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO23) {
                        log.debug("PWO2.3 : " + linkedIssueOfAWOForCompany.getKey());
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField) != "AIRBUS") {
                            String Int_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField).toString();
                            String Int_Company_Formatted = "Partner_" + Int_Company_Value;
                            log.debug("Int_Company : " + Int_Company_Formatted);

                            Group Int_Company_Management_Formatted_Group = groupManager.getGroup(Int_Company_Formatted);

                            if (!CompanyAllowedList.contains(Int_Company_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Int_Company_Management_Formatted_Group);
                                IntCompanyAllowedList.add(Int_Company_Management_Formatted_Group);
                            }
                        }
                    }

                }
                if (CompanyAllowedList != null) {
                    MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfPWO23.getKey());
                    mutableIssue.setCustomFieldValue(CompanyAllowedField, CompanyAllowedList);
                    if (AuthVerifCompanyAllowedList != null) {
                        mutableIssue.setCustomFieldValue(AuthVerifCompanyAllowedField, AuthVerifCompanyAllowedList);
                    }
                    if (FormVerifCompanyAllowedList != null) {
                        mutableIssue.setCustomFieldValue(FormVerifCompanyAllowedField, FormVerifCompanyAllowedList);
                    }
                    log.debug("Int company allowed list : " + IntCompanyAllowedList);
                    if (IntCompanyAllowedList != null) {
                        mutableIssue.setCustomFieldValue(IntCompanyAllowedField, IntCompanyAllowedList);
                    }
                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
//JIRA7;
                    issueIndexingService.reIndex(mutableIssue); //JIRA7;
                    CompanyAllowedList.clear();
                    AuthVerifCompanyAllowedList.clear();
                    FormVerifCompanyAllowedList.clear();
                    IntCompanyAllowedList.clear();
                }
            }
        }
    }
}
