package com.airbus.ims.business;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.changehistory.ChangeHistoryItem
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.security.groups.GroupManager;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import com.airbus.ims.util.Toolbox;
import com.atlassian.jira.user.ApplicationUser;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Projects : PWO
 * Description : Creation/Edit of PWO-2 / PWO-2.1
 * Events : Admin re-open, PWO-2 updated, PWO-2.1 updated, Issue Created
 * isDisabled : false
 */
public class PWO21BusinessEvent{

    /**
     * Get requestedDate from Trigger
     * @param issue
     * @param user
     */
    public static Date getRequestedDateFromTrigger(Issue issue, ApplicationUser user, Logger log){
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        //CustomField
        CustomField requestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_REQUESTEDDATE);

        //Variable
        Date requestedDateTrigger = null;

        Issue issueParent = issue.getParentObject();
        Collection<Issue> linkedIssues = issueLinkManager.getLinkCollection(issueParent, user).getAllIssues();
        for (Issue linkedIssue : linkedIssues) {
            //If linked issue is Cluster
            if (linkedIssue.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                Collection<Issue> linkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues();
                for (Issue linkedIssueOfPWO1 : linkedIssuesOfPWO1) {
                    if (linkedIssueOfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                        Collection<Issue> linkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfPWO1, user).getAllIssues();
                        for (Issue linkedIssueOfPW0 : linkedIssuesOfPWO0) {
                            if (linkedIssueOfPW0.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                Collection<Issue> linkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfPW0, user).getAllIssues();
                                for (Issue linkedIssueOfCluster : linkedIssuesOfCluster) {
                                    if (linkedIssueOfCluster.getIssueTypeId() == ConstantManager.ID_IT_TRIGGER) {
                                        if (requestedDateTrigger == null || requestedDateTrigger.after((Date) linkedIssueOfCluster.getCustomFieldValue(requestedDateField))) {
                                            requestedDateTrigger = (Date) linkedIssueOfCluster.getCustomFieldValue(requestedDateField);
                                            log.debug("Requested date trigger : " + requestedDateTrigger);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return requestedDateTrigger;
    }

    /**
     * Calculate date for PWO-2.1
     * @param issue
     * @param user
     * @param requestedDateTrigger
     */
    private static void calculateDateForPWO21(MutableIssue issue, ApplicationUser user, Date requestedDateTrigger) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField auth_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_REQUESTEDDATE);
        CustomField auth_Verification_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFICATION_REQUESTEDDATE);
        CustomField auth_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VALIDATION_REQUESTEDDATE);
        CustomField auth_FirstCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_FIRSTCOMMITTEDDATE);
        CustomField auth_Verification_FirstCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFICATION_FIRSTCOMMITTEDDATE);
        CustomField auth_Validation_FirstCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VALIDATION_FIRSTCOMMITTEDDATE);
        CustomField auth_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_LASTCOMMITTEDDATE);
        CustomField auth_Verification_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFICATION_LASTCOMMITTEDDATE);
        CustomField auth_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VALIDATION_LASTCOMMITTEDDATE);

        //Variables
        Timestamp auth_RequestedDate = Toolbox.incrementTimestamp(requestedDateTrigger, - 84);
        Timestamp auth_Verification_RequestedDate = Toolbox.incrementTimestamp(requestedDateTrigger, - 70);
        Timestamp auth_Validation_RequestedDate = Toolbox.incrementTimestamp(requestedDateTrigger, - 56);

        Date auth_FirstCommittedDate = auth_RequestedDate;
        Date auth_Verification_FirstCommittedDate = auth_Verification_RequestedDate;
        Date auth_Validation_FirstCommittedDate = auth_Validation_RequestedDate;

        Date auth_LastCommittedDate = auth_FirstCommittedDate;
        Date auth_Verification_LastCommittedDate = auth_Verification_FirstCommittedDate;
        Date auth_Validation_LastCommittedDate = auth_Validation_FirstCommittedDate;

        issue.setCustomFieldValue(auth_RequestedDateField, auth_RequestedDate);
        issue.setCustomFieldValue(auth_Verification_RequestedDateField, auth_Verification_RequestedDate);
        issue.setCustomFieldValue(auth_Validation_RequestedDateField, auth_Validation_RequestedDate);

        issue.setCustomFieldValue(auth_FirstCommittedDateField, auth_FirstCommittedDate);
        issue.setCustomFieldValue(auth_Verification_FirstCommittedDateField, auth_Verification_FirstCommittedDate);
        issue.setCustomFieldValue(auth_Validation_FirstCommittedDateField, auth_Validation_FirstCommittedDate);

        issue.setCustomFieldValue(auth_LastCommittedDateField, auth_LastCommittedDate);
        issue.setCustomFieldValue(auth_Verification_LastCommittedDateField, auth_Verification_LastCommittedDate);
        issue.setCustomFieldValue(auth_Validation_LastCommittedDateField, auth_Validation_LastCommittedDate);

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
        issueIndexingService.reIndex(issue);
    }

    public static void createPWO21(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        GroupManager groupManager = ComponentAccessor.getGroupManager();

        //CustomField
        CustomField companyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_COMPANYALLOWED);
        CustomField intCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_COMPANY);
        CustomField authVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_COMPANYAUTHVERIF);
        CustomField formVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_COMPANYFORMVERIF);
        CustomField intCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INTCOMPANY);
        CustomField formVerifCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFCOMPANY);
        CustomField formCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_COMPANY);
        CustomField authVerifCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFCOMPANY);
        CustomField authCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_COMPANY);
        CustomField iaCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_COMPANY);

        //Variables
        Date requestedDateTrigger = getRequestedDateFromTrigger(issue, user, log);

        List<Group> companyAllowedList = new ArrayList<Group>();
        List<Group> authVerifCompanyAllowedList = new ArrayList<Group>();
        List<Group> formVerifCompanyAllowedList = new ArrayList<Group>();
        List<Group> intCompanyAllowedList = new ArrayList<Group>();


        if(requestedDateTrigger != null) {
            //Calculate date for PWO-2.1
            PWO21BusinessEvent.calculateDateForPWO21(issue, user, requestedDateTrigger);
        }

        Collection<Issue> linkedIssuesOfPWO21 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfPWO21 : linkedIssuesOfPWO21) {
            if (linkedIssueOfPWO21.getProjectId() == ConstantManager.ID_PR_AWO) {
                Collection<Issue> linkedIssuesOfAWOForCompany = issueLinkManager.getLinkCollection(linkedIssueOfPWO21, user).getAllIssues();
                for (Issue linkedIssueOfAWOForCompany : linkedIssuesOfAWOForCompany) {
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(iaCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(iaCompanyField) != "AIRBUS") {
                            String IA_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(iaCompanyField).toString();
                            String IA_Company_Formatted = "Partner_" + IA_Company_Value;

                            Group IA_Company_Management_Formatted_Group = groupManager.getGroup(IA_Company_Formatted);

                            if (!companyAllowedList.contains(IA_Company_Management_Formatted_Group)) {
                                companyAllowedList.add(IA_Company_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO21) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(authCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(authCompanyField) != "AIRBUS") {
                            String auth_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(authCompanyField).toString();
                            String auth_Company_Formatted = "Partner_" + auth_Company_Value;

                            Group auth_Company_Management_Formatted_Group = groupManager.getGroup(auth_Company_Formatted);

                            if (!companyAllowedList.contains(auth_Company_Management_Formatted_Group)) {
                                companyAllowedList.add(auth_Company_Management_Formatted_Group);
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(authVerifCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(authVerifCompanyField) != "AIRBUS") {
                            String auth_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(authVerifCompanyField).toString();
                            String auth_VerifCompany_Formatted = "Partner_" + auth_VerifCompany_Value;

                            Group auth_VerifCompany_Management_Formatted_Group = groupManager.getGroup(auth_VerifCompany_Formatted);

                            if (!companyAllowedList.contains(auth_VerifCompany_Management_Formatted_Group)) {
                                companyAllowedList.add(auth_VerifCompany_Management_Formatted_Group);
                            }
                            if (!authVerifCompanyAllowedList.contains(auth_VerifCompany_Management_Formatted_Group)) {
                                authVerifCompanyAllowedList.add(auth_VerifCompany_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO22) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(formCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(formCompanyField) != "AIRBUS") {
                            String form_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(formCompanyField).toString();
                            String form_Company_Formatted = "Partner_" + form_Company_Value;

                            Group form_Company_Management_Formatted_Group = groupManager.getGroup(form_Company_Formatted);

                            if (!companyAllowedList.contains(form_Company_Management_Formatted_Group)) {
                                companyAllowedList.add(form_Company_Management_Formatted_Group);
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(formVerifCompanyField) !=null && linkedIssueOfAWOForCompany.getCustomFieldValue(formVerifCompanyField) != "AIRBUS") {
                            String form_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(formVerifCompanyField).toString();
                            String form_VerifCompany_Formatted = "Partner_" + form_VerifCompany_Value;

                            Group form_VerifCompany_Management_Formatted_Group = groupManager.getGroup(form_VerifCompany_Formatted);

                            if (!companyAllowedList.contains(form_VerifCompany_Management_Formatted_Group)) {
                                companyAllowedList.add(form_VerifCompany_Management_Formatted_Group);
                            }
                            if (!formVerifCompanyAllowedList.contains(form_VerifCompany_Management_Formatted_Group)) {
                                formVerifCompanyAllowedList.add(form_VerifCompany_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO23) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(intCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(intCompanyField) != "AIRBUS") {
                            String Int_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(intCompanyField).toString();
                            String Int_Company_Formatted = "Partner_" + Int_Company_Value;

                            Group Int_Company_Management_Formatted_Group = groupManager.getGroup(Int_Company_Formatted);

                            if (!companyAllowedList.contains(Int_Company_Management_Formatted_Group)) {
                                companyAllowedList.add(Int_Company_Management_Formatted_Group);
                                intCompanyAllowedList.add(Int_Company_Management_Formatted_Group);
                            }
                        }
                    }
                }
                if (companyAllowedList != null) {
                    MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfPWO21.getKey());
                    mutableIssue.setCustomFieldValue(companyAllowedField, companyAllowedList);
                    if (authVerifCompanyAllowedList != null) {
                        mutableIssue.setCustomFieldValue(authVerifCompanyAllowedField, authVerifCompanyAllowedList);
                    }
                    if (formVerifCompanyAllowedList != null) {
                        mutableIssue.setCustomFieldValue(formVerifCompanyAllowedField, formVerifCompanyAllowedList);
                    }
                    if (intCompanyAllowedList != null) {
                        mutableIssue.setCustomFieldValue(intCompanyAllowedField, intCompanyAllowedList);
                    }
                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                    //JIRA7
                    issueIndexingService.reIndex(mutableIssue);//JIRA7
                    companyAllowedList.clear();
                    authVerifCompanyAllowedList.clear();
                    formVerifCompanyAllowedList.clear();
                    intCompanyAllowedList.clear();
                }
            }
        }
    }

    public static void fireCompanyChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        GroupManager groupManager = ComponentAccessor.getGroupManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField iaCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_COMPANY);
        CustomField companyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_COMPANYALLOWED);
        CustomField authCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_COMPANY);
        CustomField authVerifCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFCOMPANY);
        CustomField formCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_COMPANY);
        CustomField formVerifCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFCOMPANY);
        CustomField intCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_COMPANY);
        CustomField authVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_COMPANYAUTHVERIF);
        CustomField formVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_COMPANYFORMVERIF);
        CustomField intCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INTCOMPANY);

        //Variable
        List<Group> companyAllowedList = new ArrayList<Group>();
        List<Group> authVerifCompanyAllowedList = new ArrayList<Group>();
        List<Group> formVerifCompanyAllowedList = new ArrayList<Group>();
        List<Group> intCompanyAllowedList = new ArrayList<Group>();

        Collection<Issue> linkedIssuesOfPWO21 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfPWO21 : linkedIssuesOfPWO21) {
            if (linkedIssueOfPWO21.getProjectId() == ConstantManager.ID_PR_AWO) {
                Collection<Issue> linkedIssuesOfAWOForCompany = issueLinkManager.getLinkCollection(linkedIssueOfPWO21, user).getAllIssues();
                for (Issue linkedIssueOfAWOForCompany : linkedIssuesOfAWOForCompany) {
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(iaCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(iaCompanyField) != "AIRBUS") {
                            String IA_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(iaCompanyField).toString();
                            String IA_Company_Formatted = "Partner_" + IA_Company_Value;

                            Group IA_Company_Management_Formatted_Group = groupManager.getGroup(IA_Company_Formatted);

                            if (!companyAllowedList.contains(IA_Company_Management_Formatted_Group)) {
                                companyAllowedList.add(IA_Company_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO21) {
                        log.debug("PWO-2.1 : " + linkedIssueOfAWOForCompany.getKey());
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(authCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(authCompanyField) != "AIRBUS") {
                            String auth_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(authCompanyField).toString();
                            String auth_Company_Formatted = "Partner_" + auth_Company_Value;

                            Group auth_Company_Management_Formatted_Group = groupManager.getGroup(auth_Company_Formatted);

                            if (!companyAllowedList.contains(auth_Company_Management_Formatted_Group)) {
                                companyAllowedList.add(auth_Company_Management_Formatted_Group);
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(authVerifCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(authVerifCompanyField) != "AIRBUS") {
                            String auth_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(authVerifCompanyField).toString();
                            String auth_VerifCompany_Formatted = "Partner_" + auth_VerifCompany_Value;

                            Group auth_VerifCompany_Management_Formatted_Group = groupManager.getGroup(auth_VerifCompany_Formatted);

                            if (!companyAllowedList.contains(auth_VerifCompany_Management_Formatted_Group)) {
                                companyAllowedList.add(auth_VerifCompany_Management_Formatted_Group);
                            }
                            if (!authVerifCompanyAllowedList.contains(auth_VerifCompany_Management_Formatted_Group)) {
                                authVerifCompanyAllowedList.add(auth_VerifCompany_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO22) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(formCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(formCompanyField) != "AIRBUS") {
                            String form_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(formCompanyField).toString();
                            String form_Company_Formatted = "Partner_" + form_Company_Value;

                            Group form_Company_Management_Formatted_Group = groupManager.getGroup(form_Company_Formatted);

                            if (!companyAllowedList.contains(form_Company_Management_Formatted_Group)) {
                                companyAllowedList.add(form_Company_Management_Formatted_Group);
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(formVerifCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(formVerifCompanyField) != "AIRBUS") {
                            String form_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(formVerifCompanyField).toString();
                            String form_VerifCompany_Formatted = "Partner_" + form_VerifCompany_Value;

                            Group form_VerifCompany_Management_Formatted_Group = groupManager.getGroup(form_VerifCompany_Formatted);

                            if (!companyAllowedList.contains(form_VerifCompany_Management_Formatted_Group)) {
                                companyAllowedList.add(form_VerifCompany_Management_Formatted_Group);
                            }
                            if (!formVerifCompanyAllowedList.contains(form_VerifCompany_Management_Formatted_Group)) {
                                formVerifCompanyAllowedList.add(form_VerifCompany_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantManager.ID_IT_PWO23) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(intCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(intCompanyField) != "AIRBUS") {
                            String Int_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(intCompanyField).toString();
                            String Int_Company_Formatted = "Partner_" + Int_Company_Value;

                            Group Int_Company_Management_Formatted_Group = groupManager.getGroup(Int_Company_Formatted);

                            if (!companyAllowedList.contains(Int_Company_Management_Formatted_Group)) {
                                companyAllowedList.add(Int_Company_Management_Formatted_Group);
                                intCompanyAllowedList.add(Int_Company_Management_Formatted_Group);
                            }
                        }
                    }
                }

                if (companyAllowedList != null) {
                    MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfPWO21.getKey());
                    mutableIssue.setCustomFieldValue(companyAllowedField, companyAllowedList);
                    if (authVerifCompanyAllowedList != null) {
                        mutableIssue.setCustomFieldValue(authVerifCompanyAllowedField, authVerifCompanyAllowedList);
                    }
                    if (formVerifCompanyAllowedList != null) {
                        mutableIssue.setCustomFieldValue(formVerifCompanyAllowedField, formVerifCompanyAllowedList);
                    }
                    if (intCompanyAllowedList != null) {
                        mutableIssue.setCustomFieldValue(intCompanyAllowedField, intCompanyAllowedList);
                    }
                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                    //JIRA7
                    issueIndexingService.reIndex(mutableIssue);//JIRA7
                    //CompanyAllowedList.removeAll
                    companyAllowedList.clear();
                    authVerifCompanyAllowedList.clear();
                    formVerifCompanyAllowedList.clear();
                    intCompanyAllowedList.clear();
                }
            }
        }
    }

    public static void fireAuthVerifiedChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);


        //CustomField
        CustomField auth_VerifiedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFIED);
        CustomField auth_Verified_1stDeliveryField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFICATION_1STDELIVERYDATE);
        CustomField auth_Verified_LastDeliveryField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFICATION_LASTDELIVERYDATE);
        CustomField auth_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VALIDATION_REQUESTEDDATE);
        CustomField auth_Validation_FirstCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VALIDATION_FIRSTCOMMITTEDDATE);
        CustomField auth_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VALIDATION_LASTCOMMITTEDDATE);
        CustomField auth_ValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VALIDATED);

        //Variables
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp todaysDate = new Timestamp(todayDate.getTime());
        Date authVerification1stDeliveryDate;
        Date authVerificationLastDeliveryDate;
        Date auth_Validation_RequestedDate;
        Date auth_Validation_FirstCommittedDate;
        Date auth_Validation_LastCommittedDate;
        Integer compteur = 0;

        if (issue.getCustomFieldValue(auth_VerifiedField).toString() == "Rejected") {
            if (issue.getCustomFieldValue(auth_Verified_1stDeliveryField) == null) {
                authVerification1stDeliveryDate = todaysDate;
                issue.setCustomFieldValue(auth_Verified_1stDeliveryField, authVerification1stDeliveryDate);
            }
            authVerificationLastDeliveryDate = todaysDate;
            issue.setCustomFieldValue(auth_Verified_LastDeliveryField, authVerificationLastDeliveryDate);

            //auth_ReworkDate=todaysDate+7
            //issue.setCustomFieldValue(auth_ReworkDateField,auth_ReworkDate)
        } else if (issue.getCustomFieldValue(auth_VerifiedField).toString() == "Verified") {
            //if (!issue.getCustomFieldValue(auth_Verified_1stDeliveryField)) {
            List<ChangeHistoryItem> changeItems = ComponentAccessor.getChangeHistoryManager().getAllChangeItems(issue);
            for(ChangeHistoryItem item : changeItems) {
                if (item.getField() == "auth_Verified" && item.getTos().values().contains("Verified")) {
                    compteur = compteur + 1;
                }
            }
            if (compteur < 2) {
                auth_Validation_RequestedDate = Toolbox.incrementTimestamp(todaysDate, 14);
                auth_Validation_FirstCommittedDate = auth_Validation_RequestedDate;
                auth_Validation_LastCommittedDate = auth_Validation_FirstCommittedDate;

                issue.setCustomFieldValue(auth_Validation_RequestedDateField, auth_Validation_RequestedDate);
                issue.setCustomFieldValue(auth_Validation_FirstCommittedDateField, auth_Validation_FirstCommittedDate);
                issue.setCustomFieldValue(auth_Validation_LastCommittedDateField, auth_Validation_LastCommittedDate);
            }

            if (issue.getCustomFieldValue(auth_Verified_1stDeliveryField) == null) {
                authVerification1stDeliveryDate = todaysDate;

                issue.setCustomFieldValue(auth_Verified_1stDeliveryField, authVerification1stDeliveryDate);


            }
            authVerificationLastDeliveryDate = todaysDate;
            issue.setCustomFieldValue(auth_Verified_LastDeliveryField, authVerificationLastDeliveryDate);

            FieldConfig fieldConfigAuthValidated = auth_ValidatedField.getRelevantConfig(issue);
            Option valueAuthValidated = ComponentAccessor.getOptionsManager().getOptions(fieldConfigAuthValidated)?.find{
                it.toString() == "Validation pending";
            }
            issue.setCustomFieldValue(auth_ValidatedField, valueAuthValidated);
        }
        if (issue.getCustomFieldValue(auth_VerifiedField).toString() != "Verification pending") {
            authVerificationLastDeliveryDate = todaysDate;
            issue.setCustomFieldValue(auth_Verified_LastDeliveryField, authVerificationLastDeliveryDate);
        }
        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
        issueIndexingService.reIndex(issue);
    }

    public static void fireAuthValidated(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField technicalClosureDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_TECHNICALCLOSUREDATE);
        CustomField auth_ValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VALIDATED);
        CustomField auth_Validation_1stDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VALIDATION_1STDELIVERYDATE);
        CustomField auth_ReworkDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_REWORKDATE);
        CustomField auth_Validation_LastDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VALIDATION_LASTDELIVERYDATE);
        CustomField auth_Verification_ReworkDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFICATION_REWORKDATE);
        CustomField technicallyClosedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_TECHNICALLYCLOSED);

        //Variable
        Date technicalClosureDate;
        Date authValidation1stDeliveryDate;
        Date authValidationLastDeliveryDate;
        Date auth_ReworkDate;
        Date auth_Verification_ReworkDate;
        String technicallyClosed = "No";

        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp todaysDate = new Timestamp(todayDate.getTime());

        if (issue.getCustomFieldValue(auth_ValidatedField).toString() == "Rejected") {
            if (issue.getCustomFieldValue(auth_Validation_1stDeliveryDateField) == null) {
                authValidation1stDeliveryDate = todaysDate;
                issue.setCustomFieldValue(auth_Validation_1stDeliveryDateField, authValidation1stDeliveryDate);
            }
            authValidationLastDeliveryDate = todaysDate;
            issue.setCustomFieldValue(auth_Validation_LastDeliveryDateField, authValidationLastDeliveryDate);


            if (issue.getCustomFieldValue(auth_ReworkDateField) != Toolbox.incrementTimestamp(todaysDate, 7)) {
                auth_ReworkDate = Toolbox.incrementTimestamp(todaysDate, 7);
                issue.setCustomFieldValue(auth_ReworkDateField, auth_ReworkDate);
            }

            auth_Verification_ReworkDate = Toolbox.incrementTimestamp(todaysDate, 14);
            issue.setCustomFieldValue(auth_Verification_ReworkDateField, auth_Verification_ReworkDate);

            technicallyClosed = "No";
            technicalClosureDate = null;

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        } else if (issue.getCustomFieldValue(auth_ValidatedField).toString() == "Validated") {
            technicallyClosed = "Yes";
            technicalClosureDate = todaysDate;

            if (issue.getCustomFieldValue(auth_Validation_1stDeliveryDateField) == null) {
                authValidation1stDeliveryDate = todaysDate;
                issue.setCustomFieldValue(auth_Validation_1stDeliveryDateField, authValidation1stDeliveryDate);
            }
            authValidationLastDeliveryDate = todaysDate;
            issue.setCustomFieldValue(auth_Validation_LastDeliveryDateField, authValidationLastDeliveryDate);
        }

        FieldConfig fieldConfigTechnicallyClosed = technicallyClosedField.getRelevantConfig(issue);
        Option valueTechnicallyClosed = ComponentAccessor.getOptionsManager().getOptions(fieldConfigTechnicallyClosed)?.find{
            it.toString() == technicallyClosed;
        }
        issue.setCustomFieldValue(technicallyClosedField, valueTechnicallyClosed);

        issue.setCustomFieldValue(technicalClosureDateField, technicalClosureDate);

        log.debug("indexing 2");
        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
        issueIndexingService.reIndex(issue);
    }

    public static void editPWO21(IssueEvent event, Logger log) throws IndexException {
        //Manager
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();

        //Field
        CustomField authCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_COMPANY);
        CustomField authVerifCompanyField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFCOMPANY);
        CustomField auth_VerifiedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFIED);
        CustomField auth_ValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VALIDATED);

        //Variable
        MutableIssue mutableIssue = issueManager.getIssueObject(event.getIssue().getKey());
        ApplicationUser user = event.getUser();

        log.debug("is edit");
        List<String> itemsChangedPWO21Company = new ArrayList<String>();
        itemsChangedPWO21Company.add(authCompanyField.getFieldName());
        itemsChangedPWO21Company.add(authVerifCompanyField.getFieldName());

        boolean isPWO21CompanyChanged = Toolbox.workloadIsUpdated(event.getChangeLog(), itemsChangedPWO21Company);
        if (isPWO21CompanyChanged) {
            fireCompanyChanged(mutableIssue, user, log);
        }


        List<String> itemsChangedAuthVerified = new ArrayList<String>();
        itemsChangedAuthVerified.add(auth_VerifiedField.getFieldName());

        boolean isAuthVerifiedChanged = Toolbox.workloadIsUpdated(event.getChangeLog(), itemsChangedAuthVerified);
        log.debug("is auth Verified changed : " + isAuthVerifiedChanged);
        if (isAuthVerifiedChanged) {
            fireAuthVerifiedChanged(mutableIssue, user, log);
        }

        List<String> itemsChangedAuthValidated = new ArrayList<String>();
        itemsChangedAuthValidated.add(auth_ValidatedField.getFieldName());

        boolean isAuthValidatedChanged = Toolbox.workloadIsUpdated(event.getChangeLog(), itemsChangedAuthValidated);
        if (isAuthValidatedChanged) {
            fireAuthValidated(mutableIssue, user, log);
        }


        //Set costs data on Trigger
        setCostsDataOnTrigger(mutableIssue, user, log);
    }

    /**
     * Set costs data on Trigger
     */
    public static void setCostsDataOnTrigger(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        //CustomField
        CustomField partsData_TimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_TIMESPENT);
        CustomField maintPlanning_TimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_TIMESPENT);
        CustomField maint_TimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_TIMESPENT);
        CustomField iaTimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_TIMESPENT);
        CustomField domainPWO1Field = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_DOMAINPWO1);
        CustomField authTimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_TIMESPENT);
        CustomField formTimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_TIMESPENT);
        CustomField intTimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_TIMESPENT);

        //Variable
        Long partsDataTimeSpent = Long.valueOf(0);
        Long maintPlanningTimeSpent = Long.valueOf(0);
        Long maintTimeSpent = Long.valueOf(0);
        Long partsDataEstimatedCosts;
        Long maintPlanningEstimatedCosts;
        Long maintEstimatedCosts;
        Long partsDataTimeSpentCurrent;
        Long maintPlanningTimeSpentCurrent;
        Long maintTimeSpentCurrent;
        Long iaTimeSpentCurrent;
        Long authTimeSpentCurrent;
        Long formTimeSpentCurrent;
        Long intTimeSpentCurrent;
        String partsDataTimeSpentString;
        String maintPlanningTimeSpentString;
        String maintTimeSpentString;

        Issue issueParent = issue.getParentObject();
        Collection<Issue> linkedIssues = issueLinkManager.getLinkCollection(issueParent, user).getAllIssues();
        for (Issue linkedIssue : linkedIssues) {
            if (linkedIssue.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                Collection<Issue> linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues();
                for (Issue linkedIssueOCurrentfPWO1 : linkedIssuesOfCurrentPWO1) {
                    if (linkedIssueOCurrentfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                        Collection<Issue> linkedIssuesOfCurrentPWO0 = issueLinkManager.getLinkCollection(linkedIssueOCurrentfPWO1, user).getAllIssues();
                        for (Issue linkedIssueOfCurrentPW0 : linkedIssuesOfCurrentPWO0) {
                            if (linkedIssueOfCurrentPW0.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                Collection<Issue> linkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPW0, user).getAllIssues();
                                for (Issue linkedIssueOfCluster : linkedIssuesOfCluster) {
                                    if (linkedIssueOfCluster.getIssueTypeId() == ConstantManager.ID_IT_TRIGGER) {
                                        //Reset variable
                                        partsDataTimeSpent = Long.valueOf(0);
                                        maintPlanningTimeSpent = Long.valueOf(0);
                                        maintTimeSpent = Long.valueOf(0);
                                        partsDataEstimatedCosts = Long.valueOf(0);
                                        maintPlanningEstimatedCosts = Long.valueOf(0);
                                        maintEstimatedCosts = Long.valueOf(0);
                                        log.debug("Trigger is : " + linkedIssueOfCluster.getKey());
                                        Collection<Issue> linkedIssuesOfTrigger = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                                        for (Issue linkedIssueOfTrigger : linkedIssuesOfTrigger) {
                                            if (linkedIssueOfTrigger.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                                Collection<Issue> linkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                                for (Issue linkedIssueOfClusterTrigger : linkedIssuesOfClusterTrigger) {
                                                    if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(partsData_TimeSpentField) != null) {
                                                            partsDataTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(partsData_TimeSpentField).toString());
                                                            partsDataTimeSpent = partsDataTimeSpent + partsDataTimeSpentCurrent;
                                                        }
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(maintPlanning_TimeSpentField)  != null) {
                                                            maintPlanningTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(maintPlanning_TimeSpentField).toString());
                                                            maintPlanningTimeSpent = maintPlanningTimeSpent + maintPlanningTimeSpentCurrent;
                                                        }
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(maint_TimeSpentField) != null) {
                                                            maintTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(maint_TimeSpentField).toString());
                                                            maintTimeSpent = maintTimeSpent + maintTimeSpentCurrent;
                                                        }
                                                        Collection<Issue> linkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                                        for (Issue linkedIssueOfPWO0 : linkedIssuesOfPWO0) {
                                                            if (linkedIssueOfPWO0.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                                                                if (linkedIssueOfPWO0.getCustomFieldValue(iaTimeSpentField) != null) {
                                                                    iaTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfPWO0.getCustomFieldValue(iaTimeSpentField).toString());
                                                                } else {
                                                                    iaTimeSpentCurrent = null;
                                                                }

                                                                if (linkedIssueOfPWO0.getCustomFieldValue(domainPWO1Field).toString() == "Parts Data") {
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(iaTimeSpentField) != null) {
                                                                        partsDataTimeSpent = partsDataTimeSpent + iaTimeSpentCurrent;
                                                                    }
                                                                } else if (linkedIssueOfPWO0.getCustomFieldValue(domainPWO1Field).toString() == "Maintenance Planning") {
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(iaTimeSpentField) != null) {
                                                                        maintPlanningTimeSpent = maintPlanningTimeSpent + iaTimeSpentCurrent;
                                                                    }
                                                                } else if (linkedIssueOfPWO0.getCustomFieldValue(domainPWO1Field).toString() == "Maintenance") {
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(iaTimeSpentField) != null) {
                                                                        maintTimeSpent = maintTimeSpent + iaTimeSpentCurrent;
                                                                    }
                                                                }
                                                                Collection<Issue> linkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                                for (Issue linkedIssueOfPWO1 : linkedIssuesOfPWO1) {
                                                                    if (linkedIssueOfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO2) {
                                                                        String domainOfPWO2 = linkedIssueOfPWO1.getCustomFieldValue(domainPWO1Field).toString();
                                                                        //Get PWO-2's subtask
                                                                        Collection<Issue> linkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects();
                                                                        for (Issue linkedIssueOfPWO2 : linkedIssuesOfPWO2) {
                                                                            if (linkedIssueOfPWO2.getIssueTypeId() == ConstantManager.ID_IT_PWO21) {
                                                                                if (linkedIssueOfPWO2.getCustomFieldValue(authTimeSpentField) != null) {
                                                                                    authTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(authTimeSpentField).toString());
                                                                                    //AuthTimeSpent=(double)linkedIssueOfPWO2.getCustomFieldValue(authTimeSpentField);
                                                                                } else {
                                                                                    authTimeSpentCurrent = null;
                                                                                }
                                                                                if (domainOfPWO2 == "Parts Data" && authTimeSpentCurrent != null) {
                                                                                    partsDataTimeSpent = partsDataTimeSpent + authTimeSpentCurrent;
                                                                                } else if (domainOfPWO2 == "Maintenance Planning" && authTimeSpentCurrent != null) {
                                                                                    maintPlanningTimeSpent = maintPlanningTimeSpent + authTimeSpentCurrent;
                                                                                } else if (domainOfPWO2 == "Maintenance" && authTimeSpentCurrent != null) {
                                                                                    maintTimeSpent = maintTimeSpent + authTimeSpentCurrent;
                                                                                }
                                                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ConstantManager.ID_IT_PWO22) {
                                                                                if (linkedIssueOfPWO2.getCustomFieldValue(formTimeSpentField) != null) {
                                                                                    formTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(formTimeSpentField).toString());
                                                                                    //FormTimeSpent=(double)linkedIssueOfPWO2.getCustomFieldValue(formTimeSpentField);
                                                                                } else {
                                                                                    formTimeSpentCurrent = null;
                                                                                }
                                                                                if (domainOfPWO2 == "Parts Data" && formTimeSpentCurrent != null) {
                                                                                    partsDataTimeSpent = partsDataTimeSpent + formTimeSpentCurrent;
                                                                                } else if (domainOfPWO2 == "Maintenance Planning" && formTimeSpentCurrent != null) {
                                                                                    maintPlanningTimeSpent = maintPlanningTimeSpent + formTimeSpentCurrent;
                                                                                } else if (domainOfPWO2 == "Maintenance" && formTimeSpentCurrent != null) {
                                                                                    maintTimeSpent = maintTimeSpent + formTimeSpentCurrent;
                                                                                }
                                                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ConstantManager.ID_IT_PWO23) {
                                                                                if (linkedIssueOfPWO2.getCustomFieldValue(intTimeSpentField) != null) {
                                                                                    intTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(intTimeSpentField).toString());
                                                                                } else {
                                                                                    intTimeSpentCurrent = null;
                                                                                }
                                                                                if (domainOfPWO2 == "Parts Data" && intTimeSpentCurrent != null) {
                                                                                    partsDataTimeSpent = partsDataTimeSpent + intTimeSpentCurrent;
                                                                                } else if (domainOfPWO2 == "Maintenance Planning" && intTimeSpentCurrent != null) {
                                                                                    maintPlanningTimeSpent = maintPlanningTimeSpent + intTimeSpentCurrent;
                                                                                } else if (domainOfPWO2 == "Maintenance" && intTimeSpentCurrent != null) {
                                                                                    maintTimeSpent = maintTimeSpent + intTimeSpentCurrent;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        //Set data on trigger
                                        MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfCluster.getKey());
                                        partsDataTimeSpentString = Toolbox.formatSecondesValueInHoursMinutes(partsDataTimeSpent);
                                        mutableIssue.setCustomFieldValue(partsData_TimeSpentField, partsDataTimeSpentString);
                                        maintPlanningTimeSpentString = Toolbox.formatSecondesValueInHoursMinutes(maintPlanningTimeSpent);
                                        mutableIssue.setCustomFieldValue(maintPlanning_TimeSpentField, maintPlanningTimeSpentString);
                                        maintTimeSpentString = Toolbox.formatSecondesValueInHoursMinutes(maintTimeSpent);
                                        mutableIssue.setCustomFieldValue(maint_TimeSpentField, maintTimeSpentString);
                                        issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                        issueIndexingService.reIndex(mutableIssue);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static void resolveCreationOrEditionOnPWO21(IssueEvent event, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        //CustomField
        CustomField firstSNAppliField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FIRSTSNAPPLI);

        List<String> itemsChangedAtCreation = new ArrayList<String>();
        itemsChangedAtCreation.add(firstSNAppliField.getFieldName());
        MutableIssue mutableIssue = issueManager.getIssueObject(event.getIssue().getKey());
        log.debug("issue = " + mutableIssue.getKey());
        boolean isCreation = Toolbox.workloadIsUpdated(event.getChangeLog(), itemsChangedAtCreation);
        log.debug("is creation : " + isCreation);
        if (isCreation || event.getEventTypeId() == ConstantManager.ID_EVENT_ADMINREOPEN) {//CREATION
            createPWO21(mutableIssue, event.getUser(), log);
        } else {//EDIT
            editPWO21(event, log);
        }
    }
}