package com.airbus.ims.business;

public class ConstantManager {
    //CONSTANTS
    // Please give the SERVER name where is the script to execute
    public static final String SERVER = "msahd01";
    // Please give the path where is the script to execute
    public static final String NETWORK = "gdat632";
    public static final String FOLDER = "IMS";
    public static final String SUBFOLDER = "IMS_Script";
    public static final String GROOVY_PATH_FUNCTION = "/atlassian/jira/data/scripts/util/Functions.groovy";
    //old long ID_CF_MOA = 10135
    public static final long ID_CF_PROGRAM = 10347;
    public static final long ID_CF_REFERENCE = 10001;
    public static final long ID_CF_INCREMENTALID = 10136;
    public static final long ID_PROJECT_DU = 10002;
    public static final long ID_CF_LINKTODATA = 10038;

    //PWO-0
    public static final long ID_CF_IA_VALIDATION_REQUESTEDDATE = 10154;
    public static final long ID_CF_IA_VALIDATION_1STCOMMITTEDDATE = 10155;
    public static final long ID_CF_IA_VALIDATION_LASTCOMMITTEDDATE = 10156;
    public static final long ID_CF_IA_1STDELIVERYDATE = 10147;
    public static final long ID_CF_IA_LASTDELIVERYDATE = 10148;
    public static final long ID_CF_IA_VALIDATED = 10151;
    public static final long ID_CF_PARTSDATA_STATUS = 10049;
    public static final long ID_CF_MAINTPLANNING_STATUS = 10071;
    public static final long ID_CF_MAINT_STATUS = 10091;
    public static final long ID_CF_STARTDATE = 10009;
    public static final long ID_CF_IA_REQUESTEDDATE = 10144;
    public static final long ID_CF_IA_1STCOMMITTEDDATE = 10145;
    public static final long ID_CF_IA_LASTCOMMITTEDDATE = 10146;
    public static final long ID_CF_REQUESTEDDATE = 10010;
    public static final long ID_CF_PREIA_PARTSDATA_REQUESTEDDATE = 10058;
    public static final long ID_CF_PREIA_PARTSDATA_VALIDATION_REQUESTEDDATE = 10066;
    public static final long ID_CF_PREIA_MAINTPLANNING_REQUESTEDDATE = 10077;
    public static final long ID_CF_PREIA_MAINTPLANNING_VALIDATION_REQUESTEDDATE = 10085;
    public static final long ID_CF_PREIA_MAINT_REQUESTEDDATE = 10097;
    public static final long ID_CF_PREIA_MAINT_VALIDATION_REQUESTEDDATE = 10105;
    public static final long ID_CF_PARTSDATA_1STCOMMITTEDDATE = 10060;
    public static final long ID_CF_MAINTPLANNING_1STCOMMITTEDDATE = 10079;
    public static final long IF_CF_MAINT_1STCOMMITTEDDATE = 10099;
    public static final long ID_CF_PARTSDATA_LASTCOMMITTEDDATE = 10062;
    public static final long ID_CF_MAINTPLANNING_LASTCOMMITTEDDATE = 10088;
    public static final long IF_CF_MAINT_LASTCOMMITTEDDATE = 10101;
    public static final long ID_CF_PARTSDATA_VALIDATION_1STCOMMITTEDDATE = 10067;
    public static final long ID_CF_MAINTPLANNINGVALIDATION_1STCOMMITTEDDATE = 10086;
    public static final long IF_CF_MAINTVALIDATION_1STCOMMITTEDDATE = 10106;
    public static final long ID_CF_PARTSDATAVALIDATION_LASTCOMMITTEDDATE = 10090;
    public static final long ID_CF_MAINTPLANNINGVALIDATION_LASTCOMMITTEDDATE = 10081;
    public static final long IF_CF_MAINTVALIDATION_LASTCOMMITTEDDATE = 10108;
    public static final long ID_CF_DOMAIN = 10034;

    //Trigger
    public static final long ID_CF_TYPEOFHC = 10701;
    public static final String REQUIRED_TRIGGER_STATUS = "10004";

    //PWO-1
    public static final long ID_PROJECT_AWO = 10003;
    public static final long ID_CF_IA_WU = 10163;
    public static final long ID_CF_IA_NBWU = 10164;

    //????????????????
    public static final long ID_CF_NBREWORK_PWO1 = 10149;
    public static final long ID_CF_MANUALLYEDITED_PWO1 = 10172;

    //PWO-21
    public static final long ID_CF_AUTH_REQUESTEDDATE = 10175;
    public static final long ID_CF_AUTH_FIRSTCOMMITTEDDATE = 10176;
    public static final long ID_CF_AUTH_LASTCOMMITTEDDATE = 10177;
    public static final long ID_CF_AUTH_VERIFICATION_LASTCOMMITTEDDATE = 10185;
    public static final long ID_CF_AUTH_VALIDATION_LASTCOMMITTEDDATE = 10193;
    public static final long ID_CF_AUTH_REWORKDATE = 11123;
    public static final long ID_CF_NBREWORK = 10180;
    public static final long ID_CF_MANUALLYEDITED = 10243;
    public static final long ID_CF_AUTH_VERIFIED = 10183;
    public static final long ID_CF_AUTH_VALIDATED = 10189;
    public static final long ID_CF_AUTH_1STDELIVERYDATE = 10178;
    public static final long ID_CF_AUTH_LASTDELIVERYDATE = 10179;
    public static final long ID_CF_AUTH_VERIFICATION_REQUESTEDDATE = 10192;
    public static final long ID_CF_AUTH_VERIFICATION_FIRSTCOMMITTEDDATE = 10184;
    public static final long ID_CF_AUTH_VALIDATION_REQUESTEDDATE = 10190;
    public static final long ID_CF_AUTH_VALIDATION_FIRSTCOMMITTEDDATE = 10191;
    public static final long ID_CF_AUTH_VERIFICATION_REWORKDATE = 11124;
    public static final long ID_CF_AUTH_WU = 10359;
    public static final long ID_CF_AUTH_NBWU = 10360;
    public static final long ID_CF_AUTH_VERIF_WU = 10362;
    public static final long ID_CF_AUTH_VERIF_NBWU = 10363;
    public static final String AUTH_VERIFIED = "Verification pending";
    public static final String AUTH_VALIDATED = "Validation pending";
    public static final String VALIDATION_PENDING = "Validation pending";
    public static final String NOT_STARTED = "Not started";

    //PWO-22
    public static final long ID_CF_FORM_REQUESTEDDATE = 10283;
    public static final long ID_CF_FORM_VERIFICATION_REQUESTEDDATE = 10292;
    public static final long ID_CF_FORM_VALIDATION_REQUESTEDDATE = 10299;
    public static final long ID_CF_FORM_1STCOMMITTEDDATE = 10284;
    public static final long ID_CF_FORM_VERIFICATION_1STCOMMITTEDDATE = 10293;
    public static final long ID_CF_FORM_VALIDATION_1STCOMMITTEDDATE = 10300;
    public static final long ID_CF_FORM_LASTCOMMITTEDDATE = 10285;
    public static final long ID_CF_FORM_VERIFICATION_LASTCOMMITTEDDATE = 10294;
    public static final long ID_CF_FORM_VALIDATION_LASTCOMMITTEDDATE = 10301;
    public static final long ID_CF_FORM_REWORKDATE = 11125;
    public static final long ID_CF_FORM_LAST_COMMITTED_DATE = 10285;
    public static final long ID_CF_FORM_VERIFICATION_LAST_COMMITTED_DATE = 10294;
    public static final long ID_CF_FORM_VALIDATION_LAST_COMMITTED_DATE = 10301;
    public static final long ID_CF_FORM_VERIFIED = 10291;
    public static final long ID_CF_FORM_VALIDATED = 10298;
    public static final long ID_CF_FORM_1STDELIVERYDATE = 10286;
    public static final long ID_CF_FORM_LASTDELIVERYDATE = 10287;
    public static final long ID_CF_FORM_VERIFICATION_REWORKDATE = 11126;
    public static final String FORM_VERIFIED = "Verification pending";
    public static final String FORM_VALIDATED = "Validation pending";

    //PWO-23
    public static final long ID_CF_INT_1STDELIVERYDATE = 10329;
    public static final long ID_CF_INT_LASTDELIVERYDATE = 10330;
    public static final long ID_CF_TECHNICALLYCLOSED = 10054;
    public static final long ID_CF_TECHNICALCLOSUREDATE = 10040;
    public static final long ID_CF_INT_REQUESTEDDATE = 10326;
    public static final long ID_CF_INT_1STCOMMITTEDDATE = 10327;
    public static final long ID_CF_INT_LASTCOMMITTEDDATE = 10328;
    public static final long ID_CF_INT_WU = 10374;
    public static final long ID_CF_INT_NBWU = 10375;
    public static final long ID_CF_AWO_TRANSWU = 10371;
    public static final long ID_CF_AWO_TRANSNBWU = 10372;
    public static final long ID_CF_AWO_TRANSCOMMENT = 10373;
    public static final long ID_CF_AWO_FORMWU = 10365;
    public static final long ID_CF_AWO_FORMNBWU = 10366;
    public static final long ID_CF_AWO_FORMCOMMENT = 10367;
    public static final long ID_CF_AWO_FORMVERIFWU = 10368;
    public static final long ID_CF_AWO_FORMVERIFNBWU = 10369;
    public static final long ID_CF_AWO_FORMVERIFCOMMENT = 10370;
    public static final long ID_CF_AWO_TRANSOBJLAN = 10351;

    //IssueTypes id
    public static final String ID_IT_CLUSTER = "10001";
    public static final String ID_IT_TRIGGER = "10000";
    public static final String ID_IT_PWO0 = "10004";
    public static final String ID_IT_PWO1 = "10005";
    public static final String ID_IT_PWO2 = "10006";
    public static final String ID_IT_PWO21 = "10007";
    public static final String ID_IT_PWO22 = "10008";
    public static final String ID_IT_PWO23 = "10009";

    //Types
    public static final String PWO0 = "PWO0";
    public static final String PWO1 = "PWO1";
    public static final String PWO2 = "PWO2";
    public static final String PWO21 = "PWO21";
    public static final String PWO22 = "PWO22";
    public static final String PWO23 = "PWO23";
    public static final String AWO = "AWO";

    public static final long ID_PR_AWO = 10003;
    public static final long ID_EVENT_ADMINREOPEN = 10400;

    //Fields;
    public static final long ID_CF_IA_COMPANY = 10142;
    public static final long ID_CF_COMPANYALLOWED = 10702;
    public static final long ID_CF_AUTH_COMPANY = 10173;
    public static final long ID_CF_AUTH_VERIFCOMPANY = 10181;
    public static final long ID_CF_FORM_COMPANY = 10281;
    public static final long ID_CF_FORM_VERIFCOMPANY = 10289;
    public static final long ID_CF_INT_COMPANY = 10324;
    public static final long ID_CF_COMPANYAUTHVERIF = 11130;
    public static final long ID_CF_COMPANYFORMVERIF = 10703;
    public static final long ID_CF_INTCOMPANY = 11200;

    public static final long ID_CF_FIRSTSNAPPLI = 10011;
    public static final long ID_CF_AUTH_VERIFICATION_1STDELIVERYDATE = 10186;
    public static final long ID_CF_AUTH_VERIFICATION_LASTDELIVERYDATE = 10187;
    public static final long ID_CF_AUTH_VALIDATION_1STDELIVERYDATE = 10194;
    public static final long ID_CF_AUTH_VALIDATION_LASTDELIVERYDATE = 10195;

    public static final long ID_CF_BLOCKED = 10138;
    public static final long ID_CF_BLOCKEDDOMAIN = 10041;
    public static final long ID_CF_BLOCKSTARTDATE = 10043;
    public static final long ID_CF_OLDBLOCKSTARTDATE = 11800;
    public static final long ID_CF_BLOCKENDDATE = 10044;
    public static final long ID_CF_OLDBLOCKENDDATE = 12000;
    public static final long ID_CF_ESCALATED = 10055;
    public static final long ID_CF_ESCALATESTARTDATE = 10047;
    public static final long ID_CF_OLDESCALATESTARTDATE = 12100;
    public static final long ID_CF_ESCALATEENDDATE = 10048;
    public static final long ID_CF_OLDESCALATEENDDATE = 12101;
    public static final long ID_CF_TOTALBLOCKINGDURATION = 11011;
    public static final long ID_CF_TOTALESCALATEDDURATION = 11019;

    public static final long ID_CF_FORM_VERIFIED_1STDELIVERY = 10295;
    public static final long ID_CF_FORM_VERIFIED_LASTDELIVERY = 10296;
    public static final long ID_CF_FORM_VALIDATION_1STDELIVERY = 10302;
    public static final long ID_CF_FORM_VALIDATION_LASTDELIVERY = 10303;

    public static final long ID_CF_PARTSDATA_TIMESPENT = 11012;
    public static final long ID_CF_MAINTPLANNING_TIMESPENT = 11013;
    public static final long ID_CF_MAINT_TIMESPENT = 11014;
    public static final long ID_CF_IA_TIMESPENT = 11017;
    public static final long ID_CF_DOMAINPWO1 = 10139;
    public static final long ID_CF_AUTH_TIMESPENT = 11015;
    public static final long ID_CF_FORM_TIMESPENT = 11016;
    public static final long ID_CF_INT_TIMESPENT = 11018;

    public static final long ID_CF_RESTRICTEDDATA = 10800;
    public static final long ID_CF_AUTHVERIFCOMPANY = 11130;
    public static final long ID_CF_FORMVERIFCOMPANY = 10703;
    public static final long ID_CF_PARTSDATA_EFFECTIVECOST = 10113;
    public static final long ID_CF_PARTSDATA_ESTIMATEDCOSTS = 10024;
    public static final long ID_CF_PARTSDATA_EFFECTIVECOSTS = 10113;
    public static final long ID_CF_MAINTPLANNING_EFFECTIVECOST = 10114;
    public static final long ID_CF_MAINTPLANNING_ESTIMATEDCOSTS = 10027;
    public static final long ID_CF_MAINTPLANNING_EFFECTIVECOSTS = 10114;
    public static final long ID_CF_MAINT_EFFECTIVECOST = 10115;
    public static final long ID_CF_MAINT_ESTIMATEDCOSTS = 10030;
    public static final long ID_CF_MAINT_EFFECTIVECOSTS = 10115;
    public static final long ID_CF_IA_EFFECTIVECOST = 10161;
    public static final long ID_CF_IA_VALIDATION_1STDELIVERYDATE = 10157;
    public static final long ID_CF_IA_VALIDATION_LASTDELIVERYDATE = 10158;
    public static final long ID_CF_IA_BLOCKED = 10138;
    public static final long ID_CF_IA_REWORKDATE = 11122;
    public static final long ID_CF_WUVALUE = 10625;
    public static final long ID_CF_TECHNICALCLOSURE = 10040;
    public static final long ID_CF_AUTH_EFFECTIVECOST = 10240;
    public static final long ID_CF_FORM_EFFECTIVECOST = 10305;
    public static final long ID_CF_INT_EFFECTIVECOST = 10332;
    public static final long ID_CF_TRIGGERTOTALEFFECTIVECOSTS = 10116;
    public static final long ID_CF_IMPACTCONFIRMED = 10141;
    public static final long ID_CF_IMPACTONTD = 10008;
    public static final long ID_CF_IA_ADDITIONALCOST = 11116;
    public static final long ID_CF_PREIA_PARTSDATA_ADDTIONALCOST = 11115;
    public static final long ID_CF_PREIA_MAINTPLANNING_ADDTIONALCOST = 11117;
    public static final long ID_CF_PREIA_MAINT_ADDTIONALCOST = 11118;
    public static final long ID_CF_PARTSDATA_PREIA_REQUESTEDDATE = 10058;
    public static final long ID_CF_PARTSDATA_PREIA_1STCOMMITEDDATE = 10060;
    public static final long ID_CF_PARTSDATA_PREIA_LASTCOMMITEDDATE = 10062;
    public static final long ID_CF_PARTSDATA_VALIDATION_PREIA_REQUESTEDDATE = 10066;
    public static final long ID_CF_PARTSDATA_PREIA_VALIDATION_1STCOMMITEDDATE = 10067;
    public static final long ID_CF_PARTSDATA_PREIA_VALIDATION_LASTCOMMITEDDATE = 10090;
    public static final long IF_CF_PARTSDATA_WU = 10123;
    public static final long IF_CF_PARTSDATA_NBWU = 10124;

    public static final long ID_CF_MAINTPLANNING_PREIA_REQUESTEDDATE = 10077;
    public static final long ID_CF_MAINTPLANNING_PREIA_1STCOMMITEDDATE = 10079;
    public static final long ID_CF_MAINTPLANNING_PREIA_LASTCOMMITEDDATE = 10088;
    public static final long ID_CF_MAINTPLANNING_VALIDATION_PREIA_REQUESTEDDATE = 10085;
    public static final long ID_CF_MAINTPLANNING_VALIDATION_PREIA_1STCOMMITEDDATE = 10086;
    public static final long ID_CF_MAINTPLANNING_VALIDATION_PREIA_LASTCOMMITEDDATE = 10081;
    public static final long IF_CF_MAINTPLANNING_WU = 10126;
    public static final long IF_CF_MAINTPLANNING_NBWU = 10127;
    public static final long ID_CF_MAINT_PREIA_REQUESTEDDATE = 10097;
    public static final long ID_CF_MAINT_PREIA_1STCOMMITEDDATE = 10099;
    public static final long ID_CF_MAINT_PREIA_LASTCOMMITEDDATE = 10101;
    public static final long ID_CF_MAINT_PREIA_VALIDATION_REQUESTEDDATE = 10105;
    public static final long ID_CF_MAINT_PREIA_VALIDATION_1STCOMMITEDDATE = 10106;
    public static final long ID_CF_MAINT_PREIA_VALIDATION_LASTCOMMITEDDATE = 10108;
    public static final long IF_CF_MAINT_WU = 10129;
    public static final long IF_CF_MAINT_NBWU = 10130;


    public static final long ID_CF_PARTSDATA_VALIDATED = 10065;
    public static final long ID_CF_PARTSDATA_DN = 10120;
    public static final long ID_CF_PARTSDATA_1STDELIVERY = 10061;
    public static final long ID_CF_PARTSDATA_LASTDELIVERY = 10063;
    public static final long ID_CF_PARTSDATA_VALIDATION_1STDELIVERY = 10068;
    public static final long ID_CF_PARTSDATA_VALIDATION_LASTDELIVERY = 10070;
    public static final long ID_CF_PARTSDATASTATUS = 10049;
    public static final long ID_CF_PARTSDATA_VALIDATION_1STDELIVERYDATE = 10068;
    public static final long ID_CF_PARTSDATA_VALIDATION_LASTDELIVERYDATE = 10070;
    public static final long ID_CF_PARTSDATA_REWORKDATE = 11119;
    public static final long ID_CF_MAINTPLANNING_VALIDATED = 10084;
    public static final long ID_CF_MAINTPLANNING_DN = 10121;
    public static final long ID_CF_MAINTPLANNING_1STDELIVERY = 10080;
    public static final long ID_CF_MAINTPLANNING_LASTDELIVERY = 10082;
    public static final long ID_CF_MAINTPLANNING_VALIDATION_1STDELIVERY = 10087;
    public static final long ID_CF_MAINTPLANNING_VALIDATION_LASTDELIVERY = 10089;
    public static final long ID_CF_MAINTPLANNINGSTATUS = 10071;
    public static final long ID_CF_MAINTPLANNING_VALDIDATION_1STDELIVERYDATE = 10087;
    public static final long ID_CF_MAINTPLANNING_VALDIDATION_LASTDELIVERYDATE = 10089;
    public static final long ID_CF_MAINTPLANNING_REWORKDATE = 11120;

    public static final long ID_CF_MAINT_VALIDATED = 10104;
    public static final long ID_CF_MAINT_DN = 10122;
    public static final long ID_CF_MAINT_1STDELIVERY = 10100;
    public static final long ID_CF_MAINT_LASTDELIVERY = 10102;
    public static final long ID_CF_MAINT_VALIDATION_1STDELIVERY = 10107;
    public static final long ID_CF_MAINT_VALIDATION_LASTDELIVERY = 10109;
    public static final long ID_CF_MAINTSTATUS = 10091;
    public static final long ID_CF_MAINT_VALDIDATION_1STDELIVERYDATE = 10107;
    public static final long ID_CF_MAINT_VALDIDATION_LASTDELIVERYDATE = 10109;
    public static final long ID_CF_MAINT_REWORKDATE = 11121;

}
