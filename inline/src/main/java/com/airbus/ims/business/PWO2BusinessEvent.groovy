package com.airbus.ims.business;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.user.ApplicationUser;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import com.airbus.ims.util.Toolbox;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Projects : PWO
 * Description : Creation/Edit of PWO-2 / PWO-2.1
 * Events : Admin re-open, PWO-2 updated, PWO-2.1 updated, Issue Created
 * isDisabled : false
 */
public class PWO2BusinessEvent{

    public static void createPWO2(MutableIssue issue , ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        //CustomField
        CustomField requestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_REQUESTEDDATE);


        //Variables
        Date requestedDateTrigger;
        Date requestedDate;

        Collection<Issue> linkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssue : linkedIssues) {
            if (linkedIssue.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                log.debug("PWO-1 : " + linkedIssue.getKey());
                Collection<Issue> linkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues();
                for (Issue linkedIssueOfPWO1 : linkedIssuesOfPWO1) {
                    if (linkedIssueOfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                        log.debug("PWO-0 : " + linkedIssueOfPWO1.getKey());
                        Collection<Issue> linkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfPWO1, user).getAllIssues();
                        for (Issue linkedIssueOfPW0 : linkedIssuesOfPWO0) {
                            if (linkedIssueOfPW0.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                log.debug("Cluster : " + linkedIssueOfPW0.getKey());
                                Collection<Issue> linkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfPW0, user).getAllIssues();
                                for (Issue linkedIssueOfCluster : linkedIssuesOfCluster) {
                                    if (linkedIssueOfCluster.getIssueTypeId() == ConstantManager.ID_IT_TRIGGER) {
                                        log.debug("Trigger : " + linkedIssueOfCluster.getKey());
                                        if (requestedDateTrigger == null || requestedDateTrigger > (Date) linkedIssueOfCluster.getCustomFieldValue(requestedDateField)) {
                                            requestedDateTrigger = (Date) linkedIssueOfCluster.getCustomFieldValue(requestedDateField)
                                            log.debug("Requested date trigger : " + requestedDateTrigger);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (requestedDateTrigger != null) {
            requestedDate = Toolbox.incrementTimestamp(requestedDateTrigger, - 28);

            issue.setCustomFieldValue(requestedDateField, requestedDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
    }

    public static void fireRestrictedDataChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField restrictedDataField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_RESTRICTEDDATA);

        //Variable
        String restrictedDataTrigger = "No";

        Collection<Issue>  linkedIssuesOfCurrentPWO2 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfCurrentPWO2 : linkedIssuesOfCurrentPWO2) {
            if (linkedIssueOfCurrentPWO2.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                Collection<Issue> linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO2, user).getAllIssues();
                for (Issue linkedIssueOfCurrentPWO1 : linkedIssuesOfCurrentPWO1) {
                    if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                        Collection<Issue> linkedIssuesOfCurrentPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues();
                        for (Issue linkedIssueOfCurrentPWO0 : linkedIssuesOfCurrentPWO0) {
                            if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                log.debug("Cluster is : " + linkedIssueOfCurrentPWO0.getKey());
                                Collection<Issue> linkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                                for (Issue linkedIssueOfCluster : linkedIssuesOfCluster) {
                                    if (linkedIssueOfCluster.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                                        if (linkedIssueOfCluster.getCustomFieldValue(restrictedDataField).toString() == "Yes") {
                                            restrictedDataTrigger = "Yes";
                                        } else if (restrictedDataTrigger != "Yes" && linkedIssueOfCluster.getCustomFieldValue(restrictedDataField).toString() == "Unknown") {
                                            restrictedDataTrigger = "Unknown";
                                        }
                                        Collection<Issue> linkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                                        for (Issue linkedIssueOfPWO : linkedIssuesOfPWO0) {
                                            if (linkedIssueOfPWO.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                                                if (linkedIssueOfPWO.getCustomFieldValue(restrictedDataField).toString() == "Yes") {
                                                    restrictedDataTrigger = "Yes";
                                                } else if (restrictedDataTrigger != "Yes" && linkedIssueOfPWO.getCustomFieldValue(restrictedDataField).toString() == "Unknown") {
                                                    restrictedDataTrigger = "Unknown";
                                                }
                                                Collection<Issue> linkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO, user).getAllIssues();
                                                for (Issue linkedIssueOfPWO1 : linkedIssuesOfPWO1) {
                                                    if (linkedIssueOfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO2) {
                                                        if (linkedIssueOfPWO1.getCustomFieldValue(restrictedDataField).toString() == "Yes") {
                                                            restrictedDataTrigger = "Yes";
                                                        } else if (restrictedDataTrigger != "Yes" && linkedIssueOfPWO1.getCustomFieldValue(restrictedDataField).toString() == "Unknown") {
                                                            restrictedDataTrigger = "Unknown";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            FieldConfig fieldConfigRestrictedData = restrictedDataField.getRelevantConfig(linkedIssueOfCurrentPWO0);
                            Option valueRestrictedData = ComponentAccessor.getOptionsManager().getOptions(fieldConfigRestrictedData)?.find {
                                it.toString() == restrictedDataTrigger;
                            }

                            //Set Restricted data on Cluster
                            MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfCurrentPWO0.getKey());
                            mutableIssue.setCustomFieldValue(restrictedDataField, valueRestrictedData);
                            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                            issueIndexingService.reIndex(mutableIssue);

                            //Set Restricted data on Triggers
                            Collection<Issue> linkedTriggersOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                            for (Issue linkedTriggerOfCluster : linkedTriggersOfCluster) {
                                if (linkedTriggerOfCluster.getIssueTypeId() == ConstantManager.ID_IT_TRIGGER) {
                                    FieldConfig fieldConfigRestrictedDataTrigger = restrictedDataField.getRelevantConfig(linkedTriggerOfCluster);
                                    Option valueRestrictedDataTrigger = ComponentAccessor.getOptionsManager().getOptions(fieldConfigRestrictedDataTrigger)?.find {
                                        it.toString() == restrictedDataTrigger;
                                    }
                                    mutableIssue = issueManager.getIssueObject(linkedTriggerOfCluster.getKey());
                                    mutableIssue.setCustomFieldValue(restrictedDataField, valueRestrictedDataTrigger);
                                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                    issueIndexingService.reIndex(mutableIssue);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static void fireDomainChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField domainField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_DOMAIN);
        
        String domain = issue.getCustomFieldValue(domainField).toString();
        Collection<Issue> subTasks = issue.getSubTaskObjects();
        MutableIssue mutableIssue;
        for (Issue subTask : subTasks) {
            FieldConfig fieldConfigDomain = domainField.getRelevantConfig(subTask);
            Option valueDomain = ComponentAccessor.getOptionsManager().getOptions(fieldConfigDomain)?.find{
                it.toString() == domain;
            }
            mutableIssue = issueManager.getIssueObject(subTask.getKey());
            mutableIssue.setCustomFieldValue(domainField, valueDomain);
            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        }
    }

    public static void fireBlockedChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField blockedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_BLOCKED);
        CustomField blockedDomainField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_BLOCKEDDOMAIN);
        CustomField blockEndDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_BLOCKENDDATE);
        CustomField oldBlockStartDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_OLDBLOCKSTARTDATE);
        CustomField oldBlockEndDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_OLDBLOCKENDDATE);
        CustomField blockStartDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_BLOCKSTARTDATE);
        CustomField totalBlockingDurationField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_TOTALBLOCKINGDURATION);

        //Variables
        String totalBlockingDurationString;
        Date blockStartDate;
        Date blockEndDate;

        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp todaysDate = new Timestamp(todayDate.getTime());

        double totalBlockingDuration;
        String totalBlockingDurationTriggerString;
        double totalBlockingDurationTrigger;
        double totalBlockingDurationOld;
        String blockedTrigger;

        if (issue.getCustomFieldValue(blockedField).toString() == "Yes") {
            blockEndDate = null;
            issue.setCustomFieldValue(blockEndDateField, blockEndDate);
            blockStartDate = todaysDate;
            issue.setCustomFieldValue(blockStartDateField, blockStartDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);

            if (issue.getCustomFieldValue(oldBlockStartDateField) == null || issue.getCustomFieldValue(oldBlockStartDateField).toString().substring(0, 10) != todaysDate.toString().substring(0, 10)) {
                log.debug("Old start date to update");
                issue.setCustomFieldValue(oldBlockStartDateField, blockStartDate);
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);
            }
            blockedTrigger = "No";
            Collection<Issue> linkedIssuesOfCurrentPWO2 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssueOfCurrentPWO2 : linkedIssuesOfCurrentPWO2) {
                if (linkedIssueOfCurrentPWO2.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                    Collection<Issue> linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO2, user).getAllIssues();
                    for (Issue linkedIssueOfCurrentPWO1 : linkedIssuesOfCurrentPWO1) {
                        if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                            Collection<Issue> linkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues();
                            for (Issue linkedIssueOfCurrentPWO0 : linkedIssuesOfCurrentPWO) {
                                if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                    Collection<Issue> linkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                                    for (Issue linkedIssueOfCluster : linkedIssuesOfCluster) {
                                        if (linkedIssueOfCluster.getIssueTypeId() == ConstantManager.ID_IT_TRIGGER) {
                                            //Trigger
                                            Collection<Issue> linkedIssuesOfTrigger = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                                            for (Issue linkedIssueOfTrigger : linkedIssuesOfTrigger) {
                                                if (linkedIssueOfTrigger.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                                    Collection<Issue> linkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                                    for (Issue linkedIssueOfClusterTrigger : linkedIssuesOfClusterTrigger) {
                                                        if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                                                            //Get total blocking duration of PWO0
                                                            if (linkedIssueOfClusterTrigger.getCustomFieldValue(blockedDomainField) != null) {
                                                                blockedTrigger = "Yes";
                                                            }
                                                            Collection<Issue> linkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                                            for (Issue linkedIssueOfPWO0 : linkedIssuesOfPWO0) {
                                                                if (linkedIssueOfPWO0.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                                                                    //Get total blocking duration of PWO1
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(blockedField).toString() == "Yes") {
                                                                        blockedTrigger = "Yes";
                                                                    }
                                                                    Collection<Issue> linkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                                    for (Issue linkedIssueOfPWO1 : linkedIssuesOfPWO1) {
                                                                        if (linkedIssueOfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO2) {
                                                                            if (linkedIssueOfPWO1.getCustomFieldValue(blockedField).toString() == "Yes") {
                                                                                blockedTrigger = "Yes";
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            //Set data on trigger
                                            MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfCluster.getKey());
                                            FieldConfig fieldConfigBlockedTrigger = blockedField.getRelevantConfig(linkedIssueOfCluster);
                                            Option valueBlockedTrigger = ComponentAccessor.getOptionsManager().getOptions(fieldConfigBlockedTrigger)?.find {
                                                it.toString() == blockedTrigger;
                                            }
                                            mutableIssue.setCustomFieldValue(blockedField, valueBlockedTrigger);
                                            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                            issueIndexingService.reIndex(mutableIssue);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            blockEndDate = todaysDate;
            issue.setCustomFieldValue(blockEndDateField, blockEndDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);

            Date oldBlockDate = (Date) issue.getCustomFieldValue(oldBlockEndDateField);

            if (issue.getCustomFieldValue(oldBlockEndDateField) == null || issue.getCustomFieldValue(oldBlockEndDateField) != todaysDate) {
                log.debug("Old end date to update");
                        issue.setCustomFieldValue(oldBlockEndDateField, blockEndDate);
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);

                blockStartDate = (Date) issue.getCustomFieldValue(blockStartDateField);
                if (issue.getCustomFieldValue(totalBlockingDurationField) != null) {
                    totalBlockingDurationOld = Integer.valueOf(issue.getCustomFieldValue(totalBlockingDurationField).toString().replace(" d", ""));
                    if (issue.getCustomFieldValue(oldBlockStartDateField) == todaysDate || ((Date)issue.getCustomFieldValue(oldBlockStartDateField)).after(oldBlockDate)) {
                        totalBlockingDuration = Toolbox.daysBetween(blockEndDate, blockStartDate) + 1;
                    } else {
                        totalBlockingDuration = Toolbox.daysBetween(blockEndDate, blockStartDate);
                    }
                    totalBlockingDuration = totalBlockingDurationOld + totalBlockingDuration;
                } else {
                    totalBlockingDuration = Toolbox.daysBetween(blockEndDate, blockStartDate) + 1;
                }
                totalBlockingDuration = totalBlockingDuration.trunc();
                totalBlockingDurationString = totalBlockingDuration.trunc() + " d";
                issue.setCustomFieldValue(totalBlockingDurationField, totalBlockingDurationString);

                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);
            }


            totalBlockingDurationTrigger = 0;
            blockedTrigger = "No";
            Collection<Issue> linkedIssuesOfCurrentPWO2 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssueOfCurrentPWO2 : linkedIssuesOfCurrentPWO2) {
                Collection<Issue> linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO2, user).getAllIssues();
                for (Issue linkedIssueOfCurrentPWO1 : linkedIssuesOfCurrentPWO1) {
                    if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                        Collection<Issue> linkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues();
                        for (Issue linkedIssueOfCurrentPWO0 : linkedIssuesOfCurrentPWO) {
                            if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                Collection<Issue> linkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                                for (Issue linkedIssueOfCluster : linkedIssuesOfCluster) {
                                    if (linkedIssueOfCluster.getIssueTypeId() == ConstantManager.ID_IT_TRIGGER) {
                                        Collection<Issue> linkedIssuesOfTrigger = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                                        for (Issue linkedIssueOfTrigger : linkedIssuesOfTrigger) {
                                            if (linkedIssueOfTrigger.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                                Collection<Issue> linkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                                for (Issue linkedIssueOfClusterTrigger : linkedIssuesOfClusterTrigger) {
                                                    if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(totalBlockingDurationField) != null) {
                                                            totalBlockingDurationTriggerString = linkedIssueOfClusterTrigger.getCustomFieldValue(totalBlockingDurationField).toString()
                                                            totalBlockingDurationTriggerString = totalBlockingDurationTriggerString.replaceAll(" d", "");
                                                            totalBlockingDurationTrigger = Double.parseDouble(totalBlockingDurationTrigger + totalBlockingDurationTriggerString);
                                                        }
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(blockedDomainField)  != null) {
                                                            blockedTrigger = "Yes";
                                                        }
                                                        Collection<Issue> linkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                                        for (Issue linkedIssueOfPWO0 : linkedIssuesOfPWO0) {
                                                            if (linkedIssueOfPWO0.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                                                                if (linkedIssueOfPWO0.getCustomFieldValue(totalBlockingDurationField)  != null) {
                                                                    totalBlockingDurationTriggerString = linkedIssueOfPWO0.getCustomFieldValue(totalBlockingDurationField).toString();
                                                                    totalBlockingDurationTriggerString = totalBlockingDurationTriggerString.replaceAll(" d", "");
                                                                    totalBlockingDurationTrigger = Double.parseDouble(totalBlockingDurationTrigger + totalBlockingDurationTriggerString);
                                                                }
                                                                if (linkedIssueOfPWO0.getCustomFieldValue(blockedField).toString() == "Yes") {
                                                                    blockedTrigger = "Yes";
                                                                }
                                                                Collection<Issue> linkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                                for (Issue linkedIssueOfPWO1 : linkedIssuesOfPWO1) {
                                                                    if (linkedIssueOfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO2) {
                                                                        if (linkedIssueOfPWO1.getCustomFieldValue(blockedField).toString() == "Yes") {
                                                                            blockedTrigger = "Yes";
                                                                        }
                                                                        if (linkedIssueOfPWO1.getCustomFieldValue(totalBlockingDurationField) != null) {
                                                                            totalBlockingDurationTriggerString = linkedIssueOfPWO1.getCustomFieldValue(totalBlockingDurationField).toString();
                                                                            totalBlockingDurationTriggerString = totalBlockingDurationTriggerString.replaceAll(" d", "");
                                                                            totalBlockingDurationTrigger = Double.parseDouble(totalBlockingDurationTrigger + totalBlockingDurationTriggerString);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfCluster.getKey());
                                        FieldConfig fieldConfigBlockedTrigger = blockedField.getRelevantConfig(linkedIssueOfCluster);
                                        Option valueBlockedTrigger = ComponentAccessor.getOptionsManager().getOptions(fieldConfigBlockedTrigger)?.find {
                                            it.toString() == blockedTrigger;
                                        }
                                        mutableIssue.setCustomFieldValue(blockedField, valueBlockedTrigger);
                                        totalBlockingDurationTrigger = totalBlockingDurationTrigger.trunc();
                                        totalBlockingDurationTriggerString = totalBlockingDurationTrigger.trunc() + " d";
                                        mutableIssue.setCustomFieldValue(totalBlockingDurationField, totalBlockingDurationTriggerString);
                                        issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                        issueIndexingService.reIndex(mutableIssue);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static void fireEscalatedChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField blockedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_BLOCKED);
        CustomField escalatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_ESCALATED);
        CustomField escalateStartDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_ESCALATESTARTDATE);
        CustomField oldEscalateStartDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_OLDESCALATESTARTDATE);
        CustomField escalateEndDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_ESCALATEENDDATE);
        CustomField oldEscalateEndDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_OLDESCALATEENDDATE);
        CustomField totalEscalatedDurationField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_TOTALESCALATEDDURATION);

        //Variables
        String totalEscalatedDurationString;
        Date escalateStartDate;
        Date escalateEndDate;

        Integer totalEscalatedDuration;
        String totalEscalatedDurationTriggerString;
        Integer totalEscalatedDurationTrigger;
        Integer totalEscalatedDurationOld;
        String escalatedTrigger;

        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp todaysDate = new Timestamp(todayDate.getTime());

        if (issue.getCustomFieldValue(escalatedField).toString() == "Yes") {
            escalateEndDate = null;
            issue.setCustomFieldValue(escalateEndDateField, escalateEndDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);

            if (issue.getCustomFieldValue(oldEscalateStartDateField) == null || issue.getCustomFieldValue(oldEscalateStartDateField) != todaysDate) {
                log.debug("Start date to update");
                        escalateStartDate = todaysDate;
                        issue.setCustomFieldValue(escalateStartDateField, escalateStartDate);
                        issue.setCustomFieldValue(oldEscalateStartDateField, escalateStartDate);

                        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                        issueIndexingService.reIndex(issue);
            }


            escalatedTrigger = "No";
            Collection<Issue> linkedIssuesOfCurrentPWO2 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssueOfCurrentPWO2 : linkedIssuesOfCurrentPWO2) {
                if (linkedIssueOfCurrentPWO2.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                    Collection<Issue> linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO2, user).getAllIssues()
                    for (Issue linkedIssueOfCurrentPWO1 : linkedIssuesOfCurrentPWO1) {
                        if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                            Collection<Issue> linkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues()
                            for (Issue linkedIssueOfCurrentPWO0 : linkedIssuesOfCurrentPWO) {
                                if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                    Collection<Issue> linkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                                    for (Issue linkedIssueOfCluster : linkedIssuesOfCluster) {
                                        if (linkedIssueOfCluster.getIssueTypeId() == ConstantManager.ID_IT_TRIGGER) {
                                            //Trigger
                                            Collection<Issue> linkedIssuesOfTrigger = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues()
                                            for (Issue linkedIssueOfTrigger : linkedIssuesOfTrigger) {
                                                if (linkedIssueOfTrigger.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                                    Collection<Issue> linkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                                                    for (Issue linkedIssueOfClusterTrigger : linkedIssuesOfClusterTrigger) {
                                                        if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                                                            //Get total blocking duration of PWO0
                                                            if (linkedIssueOfClusterTrigger.getCustomFieldValue(escalatedField) != null) {
                                                                escalatedTrigger = "Yes";
                                                            }
                                                            Collection<Issue> linkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                                            for (Issue linkedIssueOfPWO0 : linkedIssuesOfPWO0) {
                                                                if (linkedIssueOfPWO0.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                                                                    //Get total blocking duration of PWO1
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(escalatedField).toString() == "Yes") {
                                                                        escalatedTrigger = "Yes";
                                                                    }
                                                                    Collection<Issue> linkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                                                    for (Issue linkedIssueOfPWO1 : linkedIssuesOfPWO1) {
                                                                        if (linkedIssueOfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO2) {
                                                                            if (linkedIssueOfPWO1.getCustomFieldValue(escalatedField).toString() == "Yes") {
                                                                                escalatedTrigger = "Yes";
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfCluster.getKey());
                                            FieldConfig fieldConfigEscalatedTrigger = escalatedField.getRelevantConfig(linkedIssueOfCluster);
                                            Option valueEscalatedTrigger = ComponentAccessor.getOptionsManager().getOptions(fieldConfigEscalatedTrigger)?.find {
                                                it.toString() == escalatedTrigger;
                                            }
                                            mutableIssue.setCustomFieldValue(escalatedField, valueEscalatedTrigger);
                                            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                            issueIndexingService.reIndex(mutableIssue);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            escalateEndDate = todaysDate;
            issue.setCustomFieldValue(escalateEndDateField, escalateEndDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);

            escalateStartDate = (Date) issue.getCustomFieldValue(escalateStartDateField);
            Date oldEscalatedDate = (Date) issue.getCustomFieldValue(oldEscalateEndDateField);

            if (issue.getCustomFieldValue(oldEscalateEndDateField)  == null || issue.getCustomFieldValue(oldEscalateEndDateField) != todaysDate) {
                log.debug("set old date");
                        issue.setCustomFieldValue(oldEscalateEndDateField, escalateEndDate);
                        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                        issueIndexingService.reIndex(issue);

                if (issue.getCustomFieldValue(totalEscalatedDurationField) != null) {
                    totalEscalatedDurationOld = Integer.valueOf(issue.getCustomFieldValue(totalEscalatedDurationField).toString().replace(" d", ""));
                    if (issue.getCustomFieldValue(oldEscalateStartDateField) == todaysDate || ((Date)issue.getCustomFieldValue(oldEscalateStartDateField)).after(oldEscalatedDate)) {
                        totalEscalatedDuration = Toolbox.daysBetween(escalateEndDate, escalateStartDate) + 1;
                    } else {
                        totalEscalatedDuration = Toolbox.daysBetween(escalateEndDate,  escalateStartDate);
                    }
                    totalEscalatedDuration = totalEscalatedDurationOld + totalEscalatedDuration;
                } else {
                    totalEscalatedDuration = Toolbox.daysBetween(escalateEndDate, escalateStartDate)  + 1;
                }
                totalEscalatedDuration = totalEscalatedDuration.trunc();
                totalEscalatedDurationString = totalEscalatedDuration.trunc() + " d";

                issue.setCustomFieldValue(totalEscalatedDurationField, totalEscalatedDurationString);

                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);

            }

            totalEscalatedDurationTrigger = 0;
            escalatedTrigger = "No";
            Collection<Issue> linkedIssuesOfCurrentPWO2 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssueOfCurrentPWO2 : linkedIssuesOfCurrentPWO2) {
                if (linkedIssueOfCurrentPWO2.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                    Collection<Issue> linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO2, user).getAllIssues();
                    for (Issue linkedIssueOfCurrentPWO1 : linkedIssuesOfCurrentPWO1) {
                        if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                            Collection<Issue> linkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues();
                            for (Issue linkedIssueOfCurrentPWO0 : linkedIssuesOfCurrentPWO) {
                                if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                    Collection<Issue> linkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                                    for (Issue linkedIssueOfCluster : linkedIssuesOfCluster) {
                                        if (linkedIssueOfCluster.getIssueTypeId() == ConstantManager.ID_IT_TRIGGER) {
                                            //Trigger
                                            Collection<Issue> linkedIssuesOfTrigger = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                                            for (Issue linkedIssueOfTrigger : linkedIssuesOfTrigger) {
                                                if (linkedIssueOfTrigger.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                                    Collection<Issue> linkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                                    for (Issue linkedIssueOfClusterTrigger : linkedIssuesOfClusterTrigger) {
                                                        if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                                                            //Get total blocking duration of PWO0
                                                            if (linkedIssueOfClusterTrigger.getCustomFieldValue(totalEscalatedDurationField) != null) {
                                                                totalEscalatedDurationTriggerString = linkedIssueOfClusterTrigger.getCustomFieldValue(totalEscalatedDurationField).toString();
                                                                totalEscalatedDurationTriggerString = totalEscalatedDurationTriggerString.replaceAll(" d", "");
                                                                totalEscalatedDurationTrigger = Integer.valueOf(totalEscalatedDurationTrigger + totalEscalatedDurationTriggerString);
                                                            }
                                                            if (linkedIssueOfClusterTrigger.getCustomFieldValue(escalatedField).toString() == "Yes") {
                                                                escalatedTrigger = "Yes";
                                                            }
                                                            Collection<Issue> linkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                                            for (Issue linkedIssueOfPWO0 : linkedIssuesOfPWO0) {
                                                                if (linkedIssueOfPWO0.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                                                                    //Get total blocking duration of PWO1
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(totalEscalatedDurationField) != null) {
                                                                        totalEscalatedDurationTriggerString = linkedIssueOfPWO0.getCustomFieldValue(totalEscalatedDurationField).toString();
                                                                        totalEscalatedDurationTriggerString = totalEscalatedDurationTriggerString.replaceAll(" d", "");
                                                                        totalEscalatedDurationTrigger = Integer.valueOf(totalEscalatedDurationTrigger + totalEscalatedDurationTriggerString);
                                                                    }
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(escalatedField).toString() == "Yes") {
                                                                        escalatedTrigger = "Yes";
                                                                    }
                                                                    Collection<Issue> linkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                                    for (Issue linkedIssueOfPWO1 : linkedIssuesOfPWO1) {
                                                                        if (linkedIssueOfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO2) {
                                                                            if (linkedIssueOfPWO1.getCustomFieldValue(blockedField).toString() == "Yes") {
                                                                                escalatedTrigger = "Yes";
                                                                            }
                                                                            if (linkedIssueOfPWO1.getCustomFieldValue(totalEscalatedDurationField) != null) {
                                                                                totalEscalatedDurationTriggerString = linkedIssueOfPWO1.getCustomFieldValue(totalEscalatedDurationField).toString();
                                                                                totalEscalatedDurationTriggerString = totalEscalatedDurationTriggerString.replaceAll(" d", "");
                                                                                totalEscalatedDurationTrigger = Integer.valueOf(totalEscalatedDurationTrigger + totalEscalatedDurationTriggerString);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            //Set data on trigger
                                            FieldConfig fieldConfigEscalatedTrigger = escalatedField.getRelevantConfig(linkedIssueOfCluster);
                                            Option valueEscalatedTrigger = ComponentAccessor.getOptionsManager().getOptions(fieldConfigEscalatedTrigger)?.find{
                                                it.toString() == escalatedTrigger;
                                            }
                                            MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfCluster.getKey());
                                            mutableIssue.setCustomFieldValue(escalatedField, valueEscalatedTrigger);
                                            totalEscalatedDurationTrigger = totalEscalatedDurationTrigger.trunc();
                                            totalEscalatedDurationTriggerString = totalEscalatedDurationTrigger.trunc() + " d";
                                            mutableIssue.setCustomFieldValue(totalEscalatedDurationField, totalEscalatedDurationTriggerString);
                                            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                            issueIndexingService.reIndex(mutableIssue);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static void editPWO2(IssueEvent event, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        org.ofbiz.core.entity.GenericValue changeLog = event.getChangeLog();

        //CustomField
        CustomField restrictedDataField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_RESTRICTEDDATA);
        CustomField domainField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_DOMAIN);
        CustomField BlockedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_BLOCKEDDOMAIN);
        CustomField EscalatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_ESCALATED);
        
        //Variable
        MutableIssue mutableIssue = issueManager.getIssueObject(event.getIssue().getKey());
        ApplicationUser user = event.getUser();

        List<String> itemsChangedRestrictedData = new ArrayList<String>();
        itemsChangedRestrictedData.add(restrictedDataField.getFieldName());
        boolean isRestrictedDataChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedRestrictedData);
        if (isRestrictedDataChanged) {
            fireRestrictedDataChanged(mutableIssue, user, log);
        }

        List<String> itemsChangedDomain = new ArrayList<String>();
        itemsChangedDomain.add(domainField.getFieldName());
        boolean isDomainChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedDomain);
        if (isDomainChanged) {
            fireDomainChanged(mutableIssue, user, log);
        }

        List<String> itemsChangedBlocked = new ArrayList<String>();
        itemsChangedBlocked.add(BlockedField.getFieldName());
        boolean isBlockedChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedBlocked);
        if (isBlockedChanged) {
            fireBlockedChanged(mutableIssue, user, log);
        }


        List<String> itemsChangedEscalated = new ArrayList<String>();
        itemsChangedEscalated.add(EscalatedField.getFieldName());
        boolean isEscalatedChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedEscalated);
        if (isEscalatedChanged) {
            fireEscalatedChanged(mutableIssue, user, log);
        }
    }

    public static void resolveCreationOrEditionOnPWO2(IssueEvent event, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        //CustomField
        CustomField firstSNAppliField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FIRSTSNAPPLI);

        List<String> itemsChangedAtCreation = new ArrayList<String>();
        itemsChangedAtCreation.add(firstSNAppliField.getFieldName());
        Issue issue = event.getIssue();
        MutableIssue mutableIssue = issueManager.getIssueObject(event.getIssue().getKey());
        log.debug("issue = " + issue.getKey());

        boolean isCreation = Toolbox.workloadIsUpdated(event.getChangeLog(), itemsChangedAtCreation);
        log.debug("is creation : " + isCreation);
        if (isCreation || event.getEventTypeId() == ConstantManager.ID_EVENT_ADMINREOPEN) {//CREATION
            createPWO2(mutableIssue, event.getUser(), log);
        } else {//EDIT
            editPWO2(event, log);
        }
    }
}