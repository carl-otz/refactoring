package com.airbus.ims.business;

import com.airbus.ims.util.Toolbox;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.user.ApplicationUser;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.ofbiz.core.entity.GenericValue;

import java.sql.Timestamp

import static com.airbus.ims.util.Toolbox.formatHoursMinutesFormatInSecondes;

public class PWO0BusinessEvent {
    public static void resolveCreationOrEditionOnPWO0(IssueEvent event, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        //CustomField
        CustomField firstSNAppliField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FIRSTSNAPPLI);

        List<String> itemsChangedAtCreation = new ArrayList<String>();
        itemsChangedAtCreation.add(firstSNAppliField.getFieldName());
        Issue issue = event.getIssue();
        MutableIssue mutableIssue = issueManager.getIssueObject(event.getIssue().getKey());
        log.debug("issue = " + issue.getKey());

        boolean isCreation = Toolbox.workloadIsUpdated(event.getChangeLog(), itemsChangedAtCreation);
        log.debug("is creation : " + isCreation);
        if (isCreation || event.getEventTypeId() == ConstantManager.ID_EVENT_ADMINREOPEN) {//CREATION
            createPWO0(mutableIssue, event.getUser(), log);
        } else {//EDIT
            editPWO0(event, log);
        }
    }

    private static void createPWO0(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField;
        CustomField PartsData_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_EFFECTIVECOST);
        CustomField PartsData_PreIA_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_PREIA_REQUESTEDDATE);
        CustomField PartsData_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_PREIA_1STCOMMITEDDATE);
        CustomField PartsData_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_PREIA_LASTCOMMITEDDATE);
        CustomField PartsData_Validation_PreIA_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_VALIDATION_PREIA_REQUESTEDDATE);
        CustomField PartsData_PreIA_Validation_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_PREIA_VALIDATION_1STCOMMITEDDATE);
        CustomField PartsData_PreIA_Validation_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_PREIA_VALIDATION_LASTCOMMITEDDATE);
        CustomField PartsData_WUField = customFieldManager.getCustomFieldObject(ConstantManager.IF_CF_PARTSDATA_WU);
        CustomField PartsData_NbWUField = customFieldManager.getCustomFieldObject(ConstantManager.IF_CF_PARTSDATA_NBWU);

        CustomField MaintPlanning_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_EFFECTIVECOST);
        CustomField MaintPlanning_PreIA_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_PREIA_REQUESTEDDATE);
        CustomField MaintPlanning_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_PREIA_1STCOMMITEDDATE);
        CustomField MaintPlanning_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_PREIA_LASTCOMMITEDDATE);
        CustomField MaintPlanning_Validation_PreIA_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_VALIDATION_PREIA_REQUESTEDDATE);
        CustomField MaintPlanning_Validation_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_VALIDATION_PREIA_1STCOMMITEDDATE);
        CustomField MaintPlanning_Validation_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_VALIDATION_PREIA_LASTCOMMITEDDATE);
        CustomField MaintPlanning_WUField = customFieldManager.getCustomFieldObject(ConstantManager.IF_CF_MAINTPLANNING_WU);
        CustomField MaintPlanning_NbWUField = customFieldManager.getCustomFieldObject(ConstantManager.IF_CF_MAINTPLANNING_NBWU);
        CustomField Maint_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_EFFECTIVECOST);
        CustomField Maint_PreIA_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_PREIA_REQUESTEDDATE);
        CustomField Maint_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_PREIA_1STCOMMITEDDATE);
        CustomField Maint_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_PREIA_LASTCOMMITEDDATE);
        CustomField Maint_PreIA_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_PREIA_VALIDATION_REQUESTEDDATE);
        CustomField Maint_PreIA_Validation_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_PREIA_VALIDATION_1STCOMMITEDDATE);
        CustomField Maint_PreIA_Validation_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_PREIA_VALIDATION_LASTCOMMITEDDATE);
        CustomField Maint_WUField = customFieldManager.getCustomFieldObject(ConstantManager.IF_CF_MAINT_WU);
        CustomField Maint_NbWUField = customFieldManager.getCustomFieldObject(ConstantManager.IF_CF_MAINT_NBWU);
        CustomField RestrictedDataField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_RESTRICTEDDATA);
        CustomField RequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_REQUESTEDDATE);
        CustomField WUValueField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_WUVALUE);
        CustomField FirstSNAppliField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FIRSTSNAPPLI);
        CustomField PreIA_PartsData_AdditionalCostField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PREIA_PARTSDATA_ADDTIONALCOST);
        CustomField PreIA_MaintPlanning_AdditionalCostField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PREIA_MAINTPLANNING_ADDTIONALCOST);
        CustomField PreIA_Maint_AdditionalCostField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PREIA_MAINT_ADDTIONALCOST);

        //Variables;
        String RestrictedDataResult;
        Date RequestedDateTrigger;
        Date RequestedDate;
        Date PartsData_PreIA_RequestedDate;
        Date PartsData_Validation_PreIA_RequestedDate;
        Date PartsData_PreIA_1stCommitedDate;
        Date PartsData_PreIA_LastCommitedDate;
        Date PartsData_PreIA_Validation_1stCommitedDate;
        Date PartsData_PreIA_Validation_LastCommitedDate;
        Date MaintPlanning_PreIA_RequestedDate;
        Date MaintPlanning_Validation_PreIA_RequestedDate;
        Date MaintPlanning_PreIA_1stCommitedDate;
        Date MaintPlanning_PreIA_LastCommitedDate;
        Date MaintPlanning_PreIA_Validation_1stCommitedDate;
        Date MaintPlanning_PreIA_Validation_LastCommitedDate;
        Date Maint_PreIA_RequestedDate;
        Date Maint_PreIA_Validation_RequestedDate;
        Date Maint_PreIA_1stCommitedDate;
        Date Maint_PreIA_LastCommitedDate;
        Date Maint_PreIA_Validation_1stCommitedDate;
        Date Maint_PreIA_Validation_LastCommitedDate;

        Issue issuePartsDataWU = Toolbox.getIssueFromNFeedFieldNewFieldsSingle(PartsData_WUField, issue, log);
        Issue issueMaintPlanningWU = Toolbox.getIssueFromNFeedFieldNewFieldsSingle(MaintPlanning_WUField, issue, log);
        Issue issueMaintWU = Toolbox.getIssueFromNFeedFieldNewFieldsSingle(Maint_WUField, issue, log);
        //Issue issueIAWU = (Issue)getIssueFromNFeedFieldNewFieldsSingle(IA_WUField,issue,log);
        double PartsDataEffectiveCost;
        double MaintPlanningEffectiveCost;
        double MaintEffectiveCost;
        double PartsDataWUValue;
        double MaintPlanningWUValue;
        double MaintWUValue;
        double PartsDataNbWU;
        double MaintPlanningNbWU;
        double MaintNbWU;


        //Business;
        List<String> itemsChangedAtCreation = new ArrayList<String>();
        itemsChangedAtCreation.add(FirstSNAppliField.getFieldName());
        log.debug("issue = " + issue.getKey());

        if (issue.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
            log.debug("is PWO-0");
            //Get linked issues;
            Collection<Issue> LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssue : LinkedIssues) {
                //If linked issue is Cluster;
                if (linkedIssue.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                    log.debug("linked Cluster : " + linkedIssue.getKey());
                    RestrictedDataResult = linkedIssue.getCustomFieldValue(RestrictedDataField).toString();
                    Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues();
                    for (Issue LinkedIssueOfCluster : LinkedIssuesOfCluster) {
                        log.debug("linked issue of cluster : " + LinkedIssueOfCluster.getKey());
                        if (LinkedIssueOfCluster.getIssueTypeId() == ConstantManager.ID_IT_TRIGGER) {
                            log.debug("Linked Trigger : " + LinkedIssueOfCluster.getKey());
                            if (RequestedDateTrigger == null || RequestedDateTrigger.after((Date) LinkedIssueOfCluster.getCustomFieldValue(RequestedDateField))) {
                                RequestedDateTrigger = (Date) LinkedIssueOfCluster.getCustomFieldValue(RequestedDateField);
                                log.debug("Requested date trigger : " + RequestedDateTrigger);
                            }
                        }
                    }
                }
            }

            if (RequestedDateTrigger != null) {//Calculate date;
                log.debug("Calculate date");
                RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 119);

                PartsData_PreIA_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 126);
                PartsData_Validation_PreIA_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 119);
                PartsData_PreIA_1stCommitedDate = PartsData_PreIA_RequestedDate;
                PartsData_PreIA_LastCommitedDate = PartsData_PreIA_1stCommitedDate;
                PartsData_PreIA_Validation_1stCommitedDate = PartsData_Validation_PreIA_RequestedDate;
                PartsData_PreIA_Validation_LastCommitedDate = PartsData_PreIA_Validation_1stCommitedDate;

                MaintPlanning_PreIA_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 126);
                MaintPlanning_Validation_PreIA_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger ,- 119);
                MaintPlanning_PreIA_1stCommitedDate = MaintPlanning_PreIA_RequestedDate;
                MaintPlanning_PreIA_LastCommitedDate = MaintPlanning_PreIA_1stCommitedDate;
                MaintPlanning_PreIA_Validation_1stCommitedDate = MaintPlanning_Validation_PreIA_RequestedDate;
                MaintPlanning_PreIA_Validation_LastCommitedDate = MaintPlanning_PreIA_Validation_1stCommitedDate;

                Maint_PreIA_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 126);
                Maint_PreIA_Validation_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 119);
                Maint_PreIA_1stCommitedDate = Maint_PreIA_RequestedDate;
                Maint_PreIA_LastCommitedDate = Maint_PreIA_1stCommitedDate;
                Maint_PreIA_Validation_1stCommitedDate = Maint_PreIA_Validation_RequestedDate;
                Maint_PreIA_Validation_LastCommitedDate = Maint_PreIA_Validation_1stCommitedDate;
            }

            //Calculate effective costs;
            if (issuePartsDataWU != null && issue.getCustomFieldValue(PartsData_NbWUField) != null) {
                PartsDataWUValue = (Double) issuePartsDataWU.getCustomFieldValue(WUValueField);
                PartsDataNbWU = (Double) issue.getCustomFieldValue(PartsData_NbWUField);
                PartsDataEffectiveCost = PartsDataWUValue * PartsDataNbWU;
            }
            if (issue.getCustomFieldValue(PreIA_PartsData_AdditionalCostField) != null) {
                PartsDataEffectiveCost = PartsDataEffectiveCost + (Double) issue.getCustomFieldValue(PreIA_PartsData_AdditionalCostField);
            }
            if (issueMaintPlanningWU != null && issue.getCustomFieldValue(MaintPlanning_NbWUField) != null) {
                MaintPlanningWUValue = (Double) issueMaintPlanningWU.getCustomFieldValue(WUValueField);
                MaintPlanningNbWU = (Double) issue.getCustomFieldValue(MaintPlanning_NbWUField);
                MaintPlanningEffectiveCost = MaintPlanningWUValue * MaintPlanningNbWU;
            }
            if (issue.getCustomFieldValue(PreIA_MaintPlanning_AdditionalCostField) != null) {
                MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + (Double) issue.getCustomFieldValue(PreIA_MaintPlanning_AdditionalCostField);
            }
            if (issueMaintWU != null && issue.getCustomFieldValue(Maint_NbWUField) != null) {
                MaintWUValue = (Double) issueMaintWU.getCustomFieldValue(WUValueField);
                MaintNbWU = (Double) issue.getCustomFieldValue(Maint_NbWUField);
                MaintEffectiveCost = MaintWUValue * MaintNbWU;
            }
            if (issue.getCustomFieldValue(PreIA_Maint_AdditionalCostField) != null) {
                MaintEffectiveCost = MaintEffectiveCost + (Double) issue.getCustomFieldValue(PreIA_Maint_AdditionalCostField);
            }


            //Set Data on PWO-0;
            issue.setCustomFieldValue(RequestedDateField, RequestedDate);
            log.debug("setting date on PWO-0 at creation");

            issue.setCustomFieldValue(PartsData_PreIA_RequestedDateField, PartsData_PreIA_RequestedDate);
            issue.setCustomFieldValue(PartsData_Validation_PreIA_RequestedDateField, PartsData_Validation_PreIA_RequestedDate);
            issue.setCustomFieldValue(PartsData_PreIA_1stCommitedDateField, PartsData_PreIA_1stCommitedDate);
            issue.setCustomFieldValue(PartsData_PreIA_LastCommitedDateField, PartsData_PreIA_LastCommitedDate);
            issue.setCustomFieldValue(PartsData_PreIA_Validation_1stCommitedDateField, PartsData_PreIA_Validation_1stCommitedDate);
            issue.setCustomFieldValue(PartsData_PreIA_Validation_LastCommitedDateField, PartsData_PreIA_Validation_LastCommitedDate);

            issue.setCustomFieldValue(MaintPlanning_PreIA_RequestedDateField, MaintPlanning_PreIA_RequestedDate);
            issue.setCustomFieldValue(MaintPlanning_Validation_PreIA_RequestedDateField, MaintPlanning_Validation_PreIA_RequestedDate);
            issue.setCustomFieldValue(MaintPlanning_PreIA_1stCommitedDateField, MaintPlanning_PreIA_1stCommitedDate);
            issue.setCustomFieldValue(MaintPlanning_PreIA_LastCommitedDateField, MaintPlanning_PreIA_LastCommitedDate);
            issue.setCustomFieldValue(MaintPlanning_Validation_PreIA_1stCommitedDateField, MaintPlanning_PreIA_Validation_1stCommitedDate);
            issue.setCustomFieldValue(MaintPlanning_Validation_PreIA_LastCommitedDateField, MaintPlanning_PreIA_Validation_LastCommitedDate);

            issue.setCustomFieldValue(Maint_PreIA_RequestedDateField, Maint_PreIA_RequestedDate);
            issue.setCustomFieldValue(Maint_PreIA_Validation_RequestedDateField, Maint_PreIA_Validation_RequestedDate);
            issue.setCustomFieldValue(Maint_PreIA_1stCommitedDateField, Maint_PreIA_1stCommitedDate);
            issue.setCustomFieldValue(Maint_PreIA_LastCommitedDateField, Maint_PreIA_LastCommitedDate);
            issue.setCustomFieldValue(Maint_PreIA_Validation_1stCommitedDateField, Maint_PreIA_Validation_1stCommitedDate);
            issue.setCustomFieldValue(Maint_PreIA_Validation_LastCommitedDateField, Maint_PreIA_Validation_LastCommitedDate);


            issue.setCustomFieldValue(PartsData_EffectiveCostField, PartsDataEffectiveCost);
            issue.setCustomFieldValue(MaintPlanning_EffectiveCostField, MaintPlanningEffectiveCost);
            issue.setCustomFieldValue(Maint_EffectiveCostField, MaintEffectiveCost);

            FieldConfig fieldConfigRestrictedData = RestrictedDataField.getRelevantConfig(issue);
            Option valueRestrictedData = ComponentAccessor.getOptionsManager().getOptions(fieldConfigRestrictedData) ?.find{
                it.toString() == RestrictedDataResult;
            }
            issue.setCustomFieldValue(RestrictedDataField, valueRestrictedData);

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);

            //Set costs data on Trigger;
            setCostDataOnTriggerCreationMode(issue, user, log);

        }
    }

    private static void setCostDataOnTriggerCreationMode(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField PartsData_EstimatedCostsField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_ESTIMATEDCOSTS);
        CustomField PartsData_EffectiveCostsField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_EFFECTIVECOSTS);
        CustomField Maint_EffectiveCostsField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_EFFECTIVECOSTS);
        CustomField MaintPlanning_EstimatedCostsField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_ESTIMATEDCOSTS);
        CustomField MaintPlanning_EffectiveCostsField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_EFFECTIVECOSTS);
        CustomField Maint_EstimatedCostsField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_ESTIMATEDCOSTS);
        CustomField DomainPWO1Field = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_DOMAINPWO1);
        CustomField IAEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_EFFECTIVECOST);
        CustomField AuthEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_EFFECTIVECOST);
        CustomField FormEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_EFFECTIVECOST);
        CustomField IntEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_EFFECTIVECOST);
        CustomField TriggerTotalEffectiveCostsField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_TRIGGERTOTALEFFECTIVECOSTS);

        //Variables
        double IAEffectiveCost;
        double PartsDataEstimatedCosts = 0;
        double MaintPlanningEstimatedCosts = 0;
        double MaintEstimatedCosts = 0;
        double PartsDataEffectiveCostsOfTrigger = 0;
        double MaintPlanningEffectiveCostsOfTrigger = 0;
        double MaintEffectiveCostsOfTrigger = 0;
        double AuthEffectiveCost = 0;
        double FormEffectiveCost = 0;
        double IntEffectiveCost = 0;
        double TriggerEffectiveCost;


        Collection<Issue> LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfCurrentPWO0 : LinkedIssuesOfCurrentPWO) {
            if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                for (Issue LinkedIssueOfCluster : LinkedIssuesOfCluster) {
                    if (LinkedIssueOfCluster.getIssueTypeId() == ConstantManager.ID_IT_TRIGGER) {
                        //Trigger;
                        Collection<Issue> LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues();
                        for (Issue linkedIssueOfTrigger : LinkedIssuesOfTrigger) {
                            if (linkedIssueOfTrigger.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                Collection<Issue> LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                for (Issue linkedIssueOfClusterTrigger : LinkedIssuesOfClusterTrigger) {
                                    if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {

                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EstimatedCostsField) != null) {
                                            PartsDataEstimatedCosts = PartsDataEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EstimatedCostsField);
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EstimatedCostsField) != null) {
                                            MaintPlanningEstimatedCosts = MaintPlanningEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EstimatedCostsField);
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EstimatedCostsField) != null) {
                                            MaintEstimatedCosts = MaintEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EstimatedCostsField);
                                        }

                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EffectiveCostsField) != null) {
                                            PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EffectiveCostsField);
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EffectiveCostsField) != null) {
                                            MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EffectiveCostsField);
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EffectiveCostsField) != null) {
                                            MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EffectiveCostsField);
                                        }

                                        Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                        for (Issue linkedIssueOfPWO0 : LinkedIssuesOfPWO0) {
                                            if (linkedIssueOfPWO0.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                                                if (linkedIssueOfPWO0.getCustomFieldValue(IAEffectiveCostField) != null) {
                                                    IAEffectiveCost = (Double) linkedIssueOfPWO0.getCustomFieldValue(IAEffectiveCostField);
                                                } else {
                                                    IAEffectiveCost = 0;
                                                }

                                                if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Parts Data") {
                                                    PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + IAEffectiveCost;
                                                } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance Planning") {
                                                    MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + IAEffectiveCost;
                                                } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance") {
                                                    MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + IAEffectiveCost;
                                                }

                                                Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                for (Issue linkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
                                                    if (linkedIssueOfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO2) {
                                                        String DomainOfPWO2 = linkedIssueOfPWO1.getCustomFieldValue(DomainPWO1Field).toString(); //Fix

                                                        //Get PWO-2's subtask;
                                                        Collection<Issue> LinkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects();
                                                        for (Issue linkedIssueOfPWO2 : LinkedIssuesOfPWO2) {
                                                            if (linkedIssueOfPWO2.getIssueTypeId() == ConstantManager.ID_IT_PWO21) {
                                                                //Bug? Refactoring addition
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(AuthEffectiveCostField) != null) {
                                                                    AuthEffectiveCost = (Double) linkedIssueOfPWO2.getCustomFieldValue(AuthEffectiveCostField);
                                                                }
                                                                if (DomainOfPWO2 == "Parts Data") {
                                                                    PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + AuthEffectiveCost;
                                                                } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                                    MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + AuthEffectiveCost;
                                                                } else if (DomainOfPWO2 == "Maintenance") {
                                                                    MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + AuthEffectiveCost;
                                                                }
                                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ConstantManager.ID_IT_PWO22) {
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(FormEffectiveCostField) != null) {
                                                                    FormEffectiveCost = (Double) linkedIssueOfPWO2.getCustomFieldValue(FormEffectiveCostField);
                                                                }
                                                                if (DomainOfPWO2 == "Parts Data") {
                                                                    PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + FormEffectiveCost;
                                                                } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                                    MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + FormEffectiveCost;
                                                                } else if (DomainOfPWO2 == "Maintenance") {
                                                                    MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + FormEffectiveCost;
                                                                }
                                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ConstantManager.ID_IT_PWO23) {
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(IntEffectiveCostField) != null) {
                                                                    IntEffectiveCost = (Double) linkedIssueOfPWO2.getCustomFieldValue(IntEffectiveCostField);
                                                                }
                                                                if (DomainOfPWO2 == "Parts Data") {
                                                                    PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + IntEffectiveCost;
                                                                } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                                    MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + IntEffectiveCost;
                                                                } else if (DomainOfPWO2 == "Maintenance") {
                                                                    MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + IntEffectiveCost;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        TriggerEffectiveCost = PartsDataEffectiveCostsOfTrigger + MaintPlanningEffectiveCostsOfTrigger + MaintEffectiveCostsOfTrigger;
                        //Set data on trigger;
                        MutableIssue mutableIssue = issueManager.getIssueObject(LinkedIssueOfCluster.getKey());

                        mutableIssue.setCustomFieldValue(PartsData_EstimatedCostsField, PartsDataEstimatedCosts);
                        mutableIssue.setCustomFieldValue(MaintPlanning_EstimatedCostsField, MaintPlanningEstimatedCosts);
                        mutableIssue.setCustomFieldValue(Maint_EstimatedCostsField, MaintEstimatedCosts);
                        mutableIssue.setCustomFieldValue(PartsData_EffectiveCostsField, PartsDataEffectiveCostsOfTrigger);
                        mutableIssue.setCustomFieldValue(MaintPlanning_EffectiveCostsField, MaintPlanningEffectiveCostsOfTrigger);
                        mutableIssue.setCustomFieldValue(Maint_EffectiveCostsField, MaintEffectiveCostsOfTrigger);
                        mutableIssue.setCustomFieldValue(TriggerTotalEffectiveCostsField, TriggerEffectiveCost);
                        issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                        issueIndexingService.reIndex(mutableIssue);
                    }
                }
            }
        }
    }

    private static void editPWO0(IssueEvent event, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        MutableIssue issue = (MutableIssue) event.getIssue();
        ApplicationUser user = event.getUser();
        org.ofbiz.core.entity.GenericValue changeLog = event.getChangeLog();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField PartsData_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_EFFECTIVECOST);
        CustomField PartsData_NbWUField = customFieldManager.getCustomFieldObject(ConstantManager.IF_CF_PARTSDATA_NBWU);
        CustomField PartsDataValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_VALIDATED);
        CustomField PartsData_DN_Field = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_DN);
        CustomField PartsDataStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATASTATUS);

        CustomField MaintPlanning_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_EFFECTIVECOST);
        CustomField MaintPlanning_NbWUField = customFieldManager.getCustomFieldObject(ConstantManager.IF_CF_MAINTPLANNING_NBWU);
        CustomField MaintPlanningValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_VALIDATED);
        CustomField MaintPlanning_DN_Field = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_DN);
        CustomField MaintPlanningStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNINGSTATUS);

        CustomField Maint_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_EFFECTIVECOST);
        CustomField Maint_NbWUField = customFieldManager.getCustomFieldObject(ConstantManager.IF_CF_MAINT_NBWU);
        CustomField MaintValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_VALIDATED);
        CustomField Maint_DN_Field = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_DN);
        CustomField MaintStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTSTATUS);

        CustomField Maint_WUField = customFieldManager.getCustomFieldObject(ConstantManager.IF_CF_MAINT_WU);
        CustomField DomainField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_DOMAIN);
        CustomField MaintPlanning_WUField = customFieldManager.getCustomFieldObject(ConstantManager.IF_CF_MAINTPLANNING_WU);
        CustomField PartsData_WUField = customFieldManager.getCustomFieldObject(ConstantManager.IF_CF_PARTSDATA_WU);
        CustomField RestrictedDataField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_RESTRICTEDDATA);
        CustomField WUValueField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_WUVALUE);
        CustomField FirstSNAppliField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FIRSTSNAPPLI);
        CustomField TechnicalClosureDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_TECHNICALCLOSURE);
        CustomField TechnicallyClosedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_TECHNICALLYCLOSED);
        CustomField BlockedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_BLOCKED);
        CustomField PreIA_PartsData_AdditionalCostField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PREIA_PARTSDATA_ADDTIONALCOST);
        CustomField PreIA_MaintPlanning_AdditionalCostField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PREIA_MAINTPLANNING_ADDTIONALCOST);
        CustomField PreIA_Maint_AdditionalCostField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PREIA_MAINT_ADDTIONALCOST);
        CustomField EscalatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_ESCALATED);

        //Variables;
        String TechnicallyClosed = "Yes";
        Date TechnicalClosure;
        Date TodayDate = new LocalDate(new Date()).toDate();
        Timestamp TodaysDateCompare = new Timestamp(TodayDate.getTime());

        Issue issuePartsDataWU = Toolbox.getIssueFromNFeedFieldNewFieldsSingle(PartsData_WUField, issue, log);
        Issue issueMaintPlanningWU = Toolbox.getIssueFromNFeedFieldNewFieldsSingle(MaintPlanning_WUField, issue, log);
        Issue issueMaintWU = Toolbox.getIssueFromNFeedFieldNewFieldsSingle(Maint_WUField, issue, log);
        double PartsDataEffectiveCost;
        double MaintPlanningEffectiveCost;
        double MaintEffectiveCost;
        double PartsDataWUValue;
        double MaintPlanningWUValue;
        double MaintWUValue;
        double PartsDataNbWU;
        double MaintPlanningNbWU;
        double MaintNbWU;

        //Business;
        List<String> itemsChangedAtCreation = new ArrayList<String>();
        itemsChangedAtCreation.add(FirstSNAppliField.getFieldName());
        log.debug("issue = " + issue.getKey());


        log.debug("edit");
        //Edit;
        if (issue.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {

            //If Restricted data is changed, propagate to Cluster and Trigger;
            List<String> itemsChangedRestrictedData = new ArrayList<String>();
            itemsChangedRestrictedData.add(RestrictedDataField.getFieldName());
            boolean isRestrictedDataChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedRestrictedData);
            if (isRestrictedDataChanged) {
                fireRestrictedDataChanged(issue, user, log);
            }


            //Calculate Technically Closed;
            //@TODO
            List<Option> DomainValues = (List<Option>)issue.getCustomFieldValue(DomainField);
            for (Option domainValue : DomainValues) {
                if (domainValue.toString() == "Parts Data") {
                    if (issue.getCustomFieldValue(PartsDataValidatedField).toString() == "Validated" && TechnicallyClosed == "Yes") {
                        TechnicallyClosed = "Yes";
                    } else {
                        TechnicallyClosed = "No";
                    }
                }
                if (domainValue.toString() == "Maint Planning") {
                    if (issue.getCustomFieldValue(MaintPlanningValidatedField).toString() == "Validated" && TechnicallyClosed == "Yes") {
                        TechnicallyClosed = "Yes";
                    } else {
                        TechnicallyClosed = "No";
                    }
                }
                if (domainValue.toString() == "Maintenance") {
                    if (issue.getCustomFieldValue(MaintValidatedField).toString() == "Validated" && TechnicallyClosed == "Yes") {
                        TechnicallyClosed = "Yes";
                    } else {
                        TechnicallyClosed = "No";
                    }
                }
            }

            if (TechnicallyClosed == "Yes" && issue.getCustomFieldValue(TechnicalClosureDateField) == null) {
                TechnicalClosure = TodaysDateCompare;
                issue.setCustomFieldValue(TechnicalClosureDateField, TechnicalClosure);

            } else if (TechnicallyClosed == "No") {
                TechnicalClosure = null;
                issue.setCustomFieldValue(TechnicalClosureDateField, TechnicalClosure);
            }


            List<String> itemsChangedPartsDataValidated = new ArrayList<String>();
            itemsChangedPartsDataValidated.add(PartsDataValidatedField.getFieldName());
            boolean isPartsDataValidatedChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedPartsDataValidated);
            if (isPartsDataValidatedChanged) {
                firePartsDataValidatedChanged(issue, user, changeLog, log);
            }


            //Calculate status if DN is filled
            List<String> itemsChangedPartsDataDN = new ArrayList<String>();
            itemsChangedPartsDataDN.add(PartsData_DN_Field.getFieldName());
            boolean isPartsDataDNChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedPartsDataDN);
            if (isPartsDataDNChanged || isPartsDataValidatedChanged) {
                firePartsDataDNChanged(issue, user, log);
            }

            List<String> itemsChangedMaintPlanningValidated = new ArrayList<String>();
            itemsChangedMaintPlanningValidated.add(MaintPlanningValidatedField.getFieldName());
            boolean isMaintPlanningValidatedChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedMaintPlanningValidated);
            if (isMaintPlanningValidatedChanged) {
                fireMaintPlanningValidatedChanged(issue, user, log);
            }

            List<String> itemsChangedMaintPlanningDN = new ArrayList<String>();
            itemsChangedMaintPlanningDN.add(MaintPlanning_DN_Field.getFieldName());
            boolean isMaintPlanningDNChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedMaintPlanningDN);
            if (isMaintPlanningDNChanged || isMaintPlanningValidatedChanged) {
                fireMaintPlanningChanged(issue, user, log);
            }

            List<String> itemsChangedMaintValidated = new ArrayList<String>();
            itemsChangedMaintValidated.add(MaintValidatedField.getFieldName());
            boolean isMaintValidatedChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedMaintValidated);
            if (isMaintValidatedChanged) {
                fireMaintValidatedChanged(issue, user, log);
            }

            List<String> itemsChangedMaintDN = new ArrayList<String>();
            itemsChangedMaintDN.add(Maint_DN_Field.getFieldName());
            boolean isMaintDNChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedMaintDN);
            if (isMaintDNChanged || isMaintValidatedChanged) {
                fireMaintDNChanged(issue, user, log);
            }

            //Calculate date if status is changed;
            List<String> itemsChangedPartsDataStatus = new ArrayList<String>();
            itemsChangedPartsDataStatus.add(PartsDataStatusField.getFieldName());
            boolean isPartsDataStatusChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedPartsDataStatus);
            if (isPartsDataStatusChanged) {
                firePartsDataStatusChanged(issue, user, log, changeLog);
            }


            List<String> itemsChangedMaintPlanningStatus = new ArrayList<String>();
            itemsChangedMaintPlanningStatus.add(MaintPlanningStatusField.getFieldName());
            boolean isMaintPlanningStatusChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedMaintPlanningStatus);
            if (isMaintPlanningStatusChanged) {
                fireMaintPlanningStatusChanged(issue, user, log,changeLog);
            }

            List<String> itemsChangedMaintStatus = new ArrayList<String>();
            itemsChangedMaintStatus.add(MaintStatusField.getFieldName());
            boolean isMaintStatusChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedMaintStatus);
            if (isMaintStatusChanged) {
                fireMaintStatusChanged(issue, user, log, changeLog);
            }


            //Calculate value when Blocked is edited;
            List<String> itemsChangedBlocked = new ArrayList<String>();
            itemsChangedBlocked.add(BlockedField.getFieldName());
            boolean isBlockedChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedBlocked);
            if (isBlockedChanged) {
                fireBlockedChanged(issue, user, log);
            }


            //Calculate value when Esclated is edited;
            List<String> itemsChangedEscalated = new ArrayList<String>();
            itemsChangedEscalated.add(EscalatedField.getFieldName());
            boolean isEscalatedChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedEscalated);
            if (isEscalatedChanged) {
                fireEscalatedChanged(issue, user, log);
            }


            //Calculate effective costs;
            if (issuePartsDataWU != null && issue.getCustomFieldValue(PartsData_NbWUField) != null) {
                PartsDataWUValue = (Double) issuePartsDataWU.getCustomFieldValue(WUValueField);
                PartsDataNbWU = (Double) issue.getCustomFieldValue(PartsData_NbWUField);
                PartsDataEffectiveCost = PartsDataWUValue * PartsDataNbWU;
            }
            if (issue.getCustomFieldValue(PreIA_PartsData_AdditionalCostField) != null) {
                PartsDataEffectiveCost = PartsDataEffectiveCost + (Double) issue.getCustomFieldValue(PreIA_PartsData_AdditionalCostField);
            }
            if (issueMaintPlanningWU != null && issue.getCustomFieldValue(MaintPlanning_NbWUField) != null) {
                MaintPlanningWUValue = (Double) issueMaintPlanningWU.getCustomFieldValue(WUValueField);
                MaintPlanningNbWU = (Double) issue.getCustomFieldValue(MaintPlanning_NbWUField);
                MaintPlanningEffectiveCost = MaintPlanningWUValue * MaintPlanningNbWU;
            }
            if (issue.getCustomFieldValue(PreIA_MaintPlanning_AdditionalCostField) != null) {
                MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + (Double) issue.getCustomFieldValue(PreIA_MaintPlanning_AdditionalCostField);
            }
            if (issueMaintWU != null && issue.getCustomFieldValue(Maint_NbWUField) != null) {
                MaintWUValue = (Double) issueMaintWU.getCustomFieldValue(WUValueField);
                MaintNbWU = (Double) issue.getCustomFieldValue(Maint_NbWUField);
                MaintEffectiveCost = MaintWUValue * MaintNbWU;
            }
            if (issue.getCustomFieldValue(PreIA_Maint_AdditionalCostField) != null) {
                MaintEffectiveCost = MaintEffectiveCost + (Double) issue.getCustomFieldValue(PreIA_Maint_AdditionalCostField);
            }

            //Set data
            FieldConfig fieldConfigTechnicallyClosed = TechnicallyClosedField.getRelevantConfig(issue);
            Option valueTechnicallyClosed = ComponentAccessor.getOptionsManager().getOptions(fieldConfigTechnicallyClosed)?.find {
                it.toString() == TechnicallyClosed;
            }
            issue.setCustomFieldValue(TechnicallyClosedField, valueTechnicallyClosed);

            issue.setCustomFieldValue(PartsData_EffectiveCostField, PartsDataEffectiveCost);
            issue.setCustomFieldValue(MaintPlanning_EffectiveCostField, MaintPlanningEffectiveCost);
            issue.setCustomFieldValue(Maint_EffectiveCostField, MaintEffectiveCost);

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);


            //Set costs data on Trigger;
            setCostsDataOnTriggerEditionMode(issue, user, log);
        }

        String FirstSNAppliValue="Update";
        issue.setCustomFieldValue(FirstSNAppliField, FirstSNAppliValue);
        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
        issueIndexingService.reIndex(issue);
    }

    private static void fireMaintStatusChanged(MutableIssue issue, ApplicationUser user, Logger log, GenericValue changeLog) throws IndexException {

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField Maint_PreIA_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_PREIA_VALIDATION_REQUESTEDDATE);
        CustomField Maint_PreIA_Validation_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_PREIA_VALIDATION_1STCOMMITEDDATE);
        CustomField Maint_PreIA_Validation_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_PREIA_VALIDATION_LASTCOMMITEDDATE);
        CustomField MaintValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_VALIDATED);
        CustomField Maint_1stDeliveryField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_1STDELIVERY);
        CustomField Maint_LastDeliveryField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_LASTDELIVERY);
        CustomField MaintStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTSTATUS);

        //Variables
        Date Maint_PreIA_Validation_RequestedDate;
        Date Maint_PreIA_Validation_1stCommitedDate;
        Date Maint_PreIA_Validation_LastCommitedDate;
        Date Maint_1stDelivery;
        Date Maint_LastDelivery;
        Date TodayDate = new LocalDate(new Date()).toDate();
        Timestamp TodaysDateCompare = new Timestamp(TodayDate.getTime());
        String MaintValidated;

        if (issue.getCustomFieldValue(MaintStatusField).toString() == "Delivered") {
            if (issue.getCustomFieldValue(Maint_1stDeliveryField) == null) {
                Maint_1stDelivery = TodaysDateCompare;
                issue.setCustomFieldValue(Maint_1stDeliveryField, Maint_1stDelivery);

                Maint_PreIA_Validation_1stCommitedDate = Toolbox.incrementTimestamp(TodaysDateCompare , 7);
                issue.setCustomFieldValue(Maint_PreIA_Validation_1stCommitedDateField, Maint_PreIA_Validation_1stCommitedDate);
            }

            List<String> itemsChangedMaintValidation = new ArrayList<String>();
            itemsChangedMaintValidation.add(MaintValidatedField.getFieldName());
            boolean isMaintValidationChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedMaintValidation);
            if(!isMaintValidationChanged){
                MaintValidated = "Validation pending";

                FieldConfig fieldConfigMaintValidated = MaintValidatedField.getRelevantConfig(issue);
                Option valueMaintValidated = ComponentAccessor.getOptionsManager().getOptions(fieldConfigMaintValidated)?.find {
                    it.toString() == MaintValidated;
                }
                issue.setCustomFieldValue(MaintValidatedField, valueMaintValidated);
            }

            Maint_LastDelivery = TodaysDateCompare;
            Maint_PreIA_Validation_RequestedDate = Toolbox.incrementTimestamp(TodaysDateCompare, 7);
            Maint_PreIA_Validation_LastCommitedDate = Maint_PreIA_Validation_RequestedDate;

            issue.setCustomFieldValue(Maint_LastDeliveryField, Maint_LastDelivery);
            issue.setCustomFieldValue(Maint_PreIA_Validation_LastCommitedDateField, Maint_PreIA_Validation_LastCommitedDate);
            issue.setCustomFieldValue(Maint_PreIA_Validation_RequestedDateField, Maint_PreIA_Validation_RequestedDate);

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
    }

    private static void fireMaintPlanningStatusChanged(MutableIssue issue, ApplicationUser user, Logger log, GenericValue changeLog) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField MaintPlanning_Validation_PreIA_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_VALIDATION_PREIA_REQUESTEDDATE);
        CustomField MaintPlanning_Validation_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_VALIDATION_PREIA_1STCOMMITEDDATE);
        CustomField MaintPlanning_Validation_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_VALIDATION_PREIA_LASTCOMMITEDDATE);
        CustomField MaintPlanningValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_VALIDATED);
        CustomField MaintPlanning_1stDeliveryField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_1STDELIVERY);
        CustomField MaintPlanning_LastDeliveryField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_LASTDELIVERY);
        CustomField MaintPlanningStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNINGSTATUS);

        //Variables
        String MaintPlanningValidated;
        Date MaintPlanning_Validation_PreIA_RequestedDate;
        Date MaintPlanning_PreIA_Validation_1stCommitedDate;
        Date MaintPlanning_PreIA_Validation_LastCommitedDate;
        Date MaintPlanning_1stDelivery;
        Date MaintPlanning_LastDelivery;
        Date TodayDate = new LocalDate(new Date()).toDate();
        Timestamp TodaysDateCompare = new Timestamp(TodayDate.getTime());

        if (issue.getCustomFieldValue(MaintPlanningStatusField).toString() == "Delivered") {
            if (issue.getCustomFieldValue(MaintPlanning_1stDeliveryField) == null) {
                MaintPlanning_1stDelivery = TodaysDateCompare;
                issue.setCustomFieldValue(MaintPlanning_1stDeliveryField, MaintPlanning_1stDelivery);

                MaintPlanning_PreIA_Validation_1stCommitedDate = Toolbox.incrementTimestamp(TodaysDateCompare, 7);
                issue.setCustomFieldValue(MaintPlanning_Validation_PreIA_1stCommitedDateField, MaintPlanning_PreIA_Validation_1stCommitedDate);
            }

            List<String> itemsChangedMaintPlanningValidation = new ArrayList<String>();
            itemsChangedMaintPlanningValidation.add(MaintPlanningValidatedField.getFieldName());
            boolean isMaintPlanningValidationChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedMaintPlanningValidation);

            if(!isMaintPlanningValidationChanged){
                MaintPlanningValidated = "Validation pending";

                FieldConfig fieldConfigMaintPlanningValidated = MaintPlanningValidatedField.getRelevantConfig(issue);
                Option valueMaintPlanningValidated = ComponentAccessor.getOptionsManager().getOptions(fieldConfigMaintPlanningValidated)?.find {
                    it.toString() == MaintPlanningValidated;
                }
                issue.setCustomFieldValue(MaintPlanningValidatedField, valueMaintPlanningValidated);

            }

            MaintPlanning_LastDelivery = TodaysDateCompare;
            MaintPlanning_Validation_PreIA_RequestedDate = Toolbox.incrementTimestamp(TodaysDateCompare, 7);
            MaintPlanning_PreIA_Validation_LastCommitedDate = MaintPlanning_Validation_PreIA_RequestedDate;

            issue.setCustomFieldValue(MaintPlanning_LastDeliveryField, MaintPlanning_LastDelivery);
            issue.setCustomFieldValue(MaintPlanning_Validation_PreIA_LastCommitedDateField, MaintPlanning_PreIA_Validation_LastCommitedDate);
            issue.setCustomFieldValue(MaintPlanning_Validation_PreIA_RequestedDateField, MaintPlanning_Validation_PreIA_RequestedDate);

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
    }

    private static void firePartsDataStatusChanged(MutableIssue issue, ApplicationUser user, Logger log, GenericValue changeLog) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField PartsData_Validation_PreIA_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_VALIDATION_PREIA_REQUESTEDDATE);
        CustomField PartsData_PreIA_Validation_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_PREIA_VALIDATION_1STCOMMITEDDATE);
        CustomField PartsData_PreIA_Validation_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_PREIA_VALIDATION_LASTCOMMITEDDATE);
        CustomField PartsDataValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_VALIDATED);
        CustomField PartsData_1stDeliveryField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_1STDELIVERY);
        CustomField PartsData_LastDeliveryField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_LASTDELIVERY);
        CustomField PartsDataStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATASTATUS);

        //Variables
        Date PartsData_Validation_PreIA_RequestedDate;
        Date PartsData_PreIA_Validation_1stCommitedDate;
        Date PartsData_PreIA_Validation_LastCommitedDate;
        Date PartsData_1stDelivery;
        Date PartsData_LastDelivery;
        Date TodayDate = new LocalDate(new Date()).toDate();
        Timestamp TodaysDateCompare = new Timestamp(TodayDate.getTime());
        String PartsDataValidated;

        if (issue.getCustomFieldValue(PartsDataStatusField).toString() == "Delivered") {
            if (issue.getCustomFieldValue(PartsData_1stDeliveryField) == null) {
                PartsData_1stDelivery = TodaysDateCompare;
                issue.setCustomFieldValue(PartsData_1stDeliveryField, PartsData_1stDelivery);

                PartsData_PreIA_Validation_1stCommitedDate = Toolbox.incrementTimestamp(TodaysDateCompare, 7);
                issue.setCustomFieldValue(PartsData_PreIA_Validation_1stCommitedDateField, PartsData_PreIA_Validation_1stCommitedDate);
            }

            List<String> itemsChangedPartsValidation = new ArrayList<String>();
            itemsChangedPartsValidation.add(PartsDataValidatedField.getFieldName());
            boolean isPartsDataValidationChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedPartsValidation);

            if(!isPartsDataValidationChanged){
                PartsDataValidated = "Validation pending";

                FieldConfig fieldConfigPartsDataValidated = PartsDataValidatedField.getRelevantConfig(issue);
                Option valuePartsDataValidated = ComponentAccessor.getOptionsManager().getOptions(fieldConfigPartsDataValidated)?.find {
                    it.toString() == PartsDataValidated;
                }
                issue.setCustomFieldValue(PartsDataValidatedField, valuePartsDataValidated);
            }

            PartsData_LastDelivery = TodaysDateCompare;
            PartsData_Validation_PreIA_RequestedDate = Toolbox.incrementTimestamp(TodaysDateCompare , 7);
            PartsData_PreIA_Validation_LastCommitedDate = PartsData_Validation_PreIA_RequestedDate;

            issue.setCustomFieldValue(PartsData_LastDeliveryField, PartsData_LastDelivery);
            issue.setCustomFieldValue(PartsData_PreIA_Validation_LastCommitedDateField, PartsData_PreIA_Validation_LastCommitedDate);
            issue.setCustomFieldValue(PartsData_Validation_PreIA_RequestedDateField, PartsData_Validation_PreIA_RequestedDate);

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
    }

    private static void fireMaintDNChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField MaintValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_VALIDATED);
        CustomField Maint_DN_Field = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_DN);
        CustomField MaintStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTSTATUS);

        //Variables
        String MaintStatus;

        if (issue.getCustomFieldValue(Maint_DN_Field) != null && issue.getCustomFieldValue(MaintValidatedField).toString() == "Validated") {
            MaintStatus = "Completed";

            FieldConfig fieldConfigMaintStatus = MaintStatusField.getRelevantConfig(issue);
            Option valueMaintStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigMaintStatus)?.find {
                it.toString() == MaintStatus;
            }
            issue.setCustomFieldValue(MaintStatusField, valueMaintStatus);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
    }

    private static void fireMaintPlanningChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField MaintPlanningValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_VALIDATED);
        CustomField MaintPlanning_DN_Field = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_DN);
        CustomField MaintPlanningStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNINGSTATUS);

        //Variables
        String MaintPlanningStatus;

        if (issue.getCustomFieldValue(MaintPlanning_DN_Field) != null && issue.getCustomFieldValue(MaintPlanningValidatedField).toString() == "Validated") {
            MaintPlanningStatus = "Completed";
            FieldConfig fieldConfigMaintPlanningStatus = MaintPlanningStatusField.getRelevantConfig(issue);
            Option valueMaintPlanningStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigMaintPlanningStatus)?.find {
                it.toString() == MaintPlanningStatus;
            }
            issue.setCustomFieldValue(MaintPlanningStatusField, valueMaintPlanningStatus);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
    }

    private static void firePartsDataDNChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField PartsDataValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_VALIDATED);
        CustomField PartsData_DN_Field = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_DN);
        CustomField PartsDataStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATASTATUS);

        //Variables
        String PartsDataStatus;

        if (issue.getCustomFieldValue(PartsData_DN_Field)  != null && issue.getCustomFieldValue(PartsDataValidatedField).toString() == "Validated") {
            PartsDataStatus = "Completed";

            FieldConfig fieldConfigPartsDataStatus = PartsDataStatusField.getRelevantConfig(issue);
            Option valuePartsDataStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigPartsDataStatus)?.find {
                it.toString() == PartsDataStatus;
            }
            issue.setCustomFieldValue(PartsDataStatusField, valuePartsDataStatus);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
    }

    private static void fireMaintPlanningValidatedChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField MaintPlanning_Validation_1stDeliveryField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_VALIDATION_1STDELIVERY);
        CustomField MaintPlanningValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_VALIDATED);
        CustomField MaintPlanning_ReworkDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_REWORKDATE);
        customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_VALIDATION_1STDELIVERY);
        CustomField MaintPlanning_Validation_LastDeliveryField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_VALIDATION_LASTDELIVERY);
        CustomField MaintPlanningStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNINGSTATUS);
        CustomField MaintPlanning_Valdidation_1stDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_VALDIDATION_1STDELIVERYDATE);
        CustomField MaintPlanning_Valdidation_LastDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_VALDIDATION_LASTDELIVERYDATE);

        //Variables;
        String MaintPlanningStatus;
        Date MaintPlanning_Valdidation_1stDeliveryDate;
        Date MaintPlanning_Valdidation_LastDeliveryDate;
        Date MaintPlanning_ReworkDate;

        Date TodayDate = new LocalDate(new Date()).toDate();
        Timestamp TodaysDateCompare = new Timestamp(TodayDate.getTime());

        log.debug("maint planning validated changed");
        if (issue.getCustomFieldValue(MaintPlanning_Validation_1stDeliveryField) == null) {
            log.debug("setting maint planning validation 1st delivery");
            MaintPlanning_Valdidation_1stDeliveryDate = TodaysDateCompare;
            issue.setCustomFieldValue(MaintPlanning_Validation_1stDeliveryField, MaintPlanning_Valdidation_1stDeliveryDate);
        }
        MaintPlanning_Valdidation_LastDeliveryDate = TodaysDateCompare;
        issue.setCustomFieldValue(MaintPlanning_Validation_LastDeliveryField, MaintPlanning_Valdidation_LastDeliveryDate);
        if (issue.getCustomFieldValue(MaintPlanningValidatedField).toString() == "Rejected") {
            MaintPlanningStatus = "In progress";

            FieldConfig fieldConfigMaintPlanningStatus = MaintPlanningStatusField.getRelevantConfig(issue);
            Option valueMaintPlanningStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigMaintPlanningStatus)?.find {
                it.toString() == MaintPlanningStatus;
            }

            MaintPlanning_ReworkDate = Toolbox.incrementTimestamp(TodaysDateCompare, 7);

            issue.setCustomFieldValue(MaintPlanningStatusField, valueMaintPlanningStatus);
            issue.setCustomFieldValue(MaintPlanning_ReworkDateField, MaintPlanning_ReworkDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
        if (issue.getCustomFieldValue(MaintPlanningValidatedField).toString() == "Validated" || issue.getCustomFieldValue(MaintPlanningValidatedField).toString() == "Rejected") {
            log.debug("validated = validated");
            log.debug("setting validation 1st and last delivery date");
            MaintPlanning_Valdidation_1stDeliveryDate = TodaysDateCompare;
            MaintPlanning_Valdidation_LastDeliveryDate = TodaysDateCompare;

            if (issue.getCustomFieldValue(MaintPlanning_Valdidation_1stDeliveryDateField) == null) {
                issue.setCustomFieldValue(MaintPlanning_Valdidation_1stDeliveryDateField, MaintPlanning_Valdidation_1stDeliveryDate);
            }
            issue.setCustomFieldValue(MaintPlanning_Valdidation_LastDeliveryDateField, MaintPlanning_Valdidation_LastDeliveryDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
        log.debug("end of maint planning validated change");
    }

    private static void fireBlockedChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField TotalBlockingDurationField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_TOTALBLOCKINGDURATION);
        CustomField IA_BlockedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_BLOCKED);
        CustomField BlockedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_BLOCKED);
        CustomField OldBlockStartDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_OLDBLOCKSTARTDATE);
        CustomField BlockStartDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_BLOCKSTARTDATE);
        CustomField OldBlockEndDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_OLDBLOCKENDDATE);
        CustomField BlockEndDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_BLOCKENDDATE);

        //Variables
        String TotalBlockingDurationString;
        Date TodayDate = new LocalDate(new Date()).toDate();
        Timestamp TodaysDateCompare = new Timestamp(TodayDate.getTime());

        Date BlockStartDate;
        Date BlockEndDate;
        double TotalBlockingDuration;

        String TotalBlockingDurationTriggerString;
        double TotalBlockingDurationTrigger;
        double TotalBlockingDurationOld;

        String BlockedTrigger;
        log.debug("PWO-0 set to Blocked");
        log.debug("value of blocked : " + issue.getCustomFieldValue(BlockedField));
        if (issue.getCustomFieldValue(BlockedField) != null) {
            BlockEndDate = null;
            issue.setCustomFieldValue(BlockEndDateField, BlockEndDate);

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
            if (issue.getCustomFieldValue(OldBlockStartDateField) == null || issue.getCustomFieldValue(OldBlockStartDateField) != TodaysDateCompare) {
                log.debug("Start date to update");
                BlockStartDate = TodaysDateCompare;
                issue.setCustomFieldValue(BlockStartDateField, BlockStartDate);
                issue.setCustomFieldValue(OldBlockStartDateField, BlockStartDate);

                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);
            }
            BlockedTrigger = "No";
            Collection<Issue> LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssueOfCurrentPWO0 : LinkedIssuesOfCurrentPWO) {
                if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                    Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                    for (Issue LinkedIssueOfCluster : LinkedIssuesOfCluster) {
                        if (LinkedIssueOfCluster.getIssueTypeId() == ConstantManager.ID_IT_TRIGGER) {
                            //Trigger;
                            Collection<Issue> LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues();
                            for (Issue linkedIssueOfTrigger : LinkedIssuesOfTrigger) {
                                if (linkedIssueOfTrigger.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                    Collection<Issue> LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                    for (Issue linkedIssueOfClusterTrigger : LinkedIssuesOfClusterTrigger) {
                                        if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                                            log.debug("PWO-0 : " + linkedIssueOfClusterTrigger.getKey());
                                            //Get total blocking duration of PWO0;
                                            if (linkedIssueOfClusterTrigger.getCustomFieldValue(BlockedField) != null) {
                                                log.debug("PWO-0 " + linkedIssueOfClusterTrigger.getKey() + " is blocked");
                                                BlockedTrigger = "Yes";
                                            }
                                            Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                            for (Issue linkedIssueOfPWO0 : LinkedIssuesOfPWO0) {
                                                if (linkedIssueOfPWO0.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                                                    //Get total blocking duration of PWO1;
                                                    if (linkedIssueOfPWO0.getCustomFieldValue(IA_BlockedField).toString() == "Yes") {
                                                        BlockedTrigger = "Yes";
                                                    }
                                                    Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                    for (Issue linkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
                                                        if (linkedIssueOfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO2) {
                                                            if (linkedIssueOfPWO1.getCustomFieldValue(IA_BlockedField).toString() == "Yes") {
                                                                BlockedTrigger = "Yes";
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            log.debug("value to set : " + BlockedTrigger);
                            //Set data on trigger;
                            FieldConfig fieldConfigBlockedTrigger = IA_BlockedField.getRelevantConfig(LinkedIssueOfCluster);
                            Option valueBlockedTrigger = ComponentAccessor.getOptionsManager().getOptions(fieldConfigBlockedTrigger)?.find {
                                it.toString() == BlockedTrigger;
                            }

                            MutableIssue mutableIssue = issueManager.getIssueObject(LinkedIssueOfCluster.getKey());
                            mutableIssue.setCustomFieldValue(IA_BlockedField, valueBlockedTrigger);
                            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                            issueIndexingService.reIndex(mutableIssue);
                        }
                    }
                }
            }
        } else {
            log.debug("unblocked");
            BlockEndDate = TodaysDateCompare;
            issue.setCustomFieldValue(BlockEndDateField, BlockEndDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
            BlockStartDate = (Date) issue.getCustomFieldValue(BlockStartDateField);

            log.debug("old end date : " + issue.getCustomFieldValue(OldBlockEndDateField));
            Date OldBlockDate = (Date) issue.getCustomFieldValue(OldBlockEndDateField);


            if (issue.getCustomFieldValue(OldBlockEndDateField) == null || issue.getCustomFieldValue(OldBlockEndDateField) != TodaysDateCompare) {
                log.debug("set old date");
                issue.setCustomFieldValue(OldBlockEndDateField, BlockEndDate);
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);

                if (issue.getCustomFieldValue(TotalBlockingDurationField) != null) {
                    log.debug("not empty");
                    TotalBlockingDurationOld = Double.valueOf(issue.getCustomFieldValue(TotalBlockingDurationField).toString().replace(" d", ""));
                    if (issue.getCustomFieldValue(OldBlockStartDateField) == TodaysDateCompare || ((Date)issue.getCustomFieldValue(OldBlockStartDateField)).after(OldBlockDate)) {
                        log.debug("+1");
                        TotalBlockingDuration = Toolbox.daysBetween(BlockEndDate, BlockStartDate) + 1;
                    } else {
                        log.debug("without +1");
                        TotalBlockingDuration = Toolbox.daysBetween(BlockEndDate, BlockStartDate);
                    }
                    log.debug("new value to add : " + TotalBlockingDuration);
                    TotalBlockingDuration = TotalBlockingDurationOld + TotalBlockingDuration;
                } else {
                    TotalBlockingDuration = Toolbox.daysBetween(BlockEndDate, BlockStartDate) + 1;
                }
                TotalBlockingDuration = TotalBlockingDuration.trunc();
                TotalBlockingDurationString = TotalBlockingDuration.trunc() + " d";
                log.debug("final value : " + TotalBlockingDurationString);

                issue.setCustomFieldValue(TotalBlockingDurationField, TotalBlockingDurationString);

                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);

            }
            log.debug("end of if");

            //BlockStartDate=null;
            //issue.setCustomFieldValue(BlockStartDateField,BlockStartDate);

            //Calculate Blocking duration for trigger;
            TotalBlockingDurationTrigger = 0;
            BlockedTrigger = "No";
            Collection<Issue> LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssueOfCurrentPWO0 : LinkedIssuesOfCurrentPWO) {
                if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                    Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                    for (Issue LinkedIssueOfCluster : LinkedIssuesOfCluster) {
                        if (LinkedIssueOfCluster.getIssueTypeId() == ConstantManager.ID_IT_TRIGGER) {
                            //Trigger;
                            Collection<Issue> LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues();
                            for (Issue linkedIssueOfTrigger : LinkedIssuesOfTrigger) {
                                if (linkedIssueOfTrigger.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                    Collection<Issue> LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                    for (Issue linkedIssueOfClusterTrigger : LinkedIssuesOfClusterTrigger) {
                                        if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                                            if (linkedIssueOfClusterTrigger.getCustomFieldValue(BlockedField) != null) {
                                                BlockedTrigger = "Yes";
                                            }
                                            //Get total blocking duration of PWO0;
                                            if (linkedIssueOfClusterTrigger.getCustomFieldValue(TotalBlockingDurationField) != null) {
                                                TotalBlockingDurationTriggerString = linkedIssueOfClusterTrigger.getCustomFieldValue(TotalBlockingDurationField).toString();
                                                TotalBlockingDurationTriggerString = TotalBlockingDurationTriggerString.replaceAll(" d", "");
                                                TotalBlockingDurationTrigger = TotalBlockingDurationTrigger + Double.valueOf(TotalBlockingDurationTriggerString);
                                            }
                                            Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                            for (Issue linkedIssueOfPWO0 : LinkedIssuesOfPWO0) {
                                                if (linkedIssueOfPWO0.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                                                    //Get total blocking duration of PWO1;
                                                    if (linkedIssueOfPWO0.getCustomFieldValue(IA_BlockedField).toString() == "Yes") {
                                                        BlockedTrigger = "Yes";
                                                    }
                                                    if (linkedIssueOfPWO0.getCustomFieldValue(TotalBlockingDurationField) != null) {
                                                        TotalBlockingDurationTriggerString = linkedIssueOfPWO0.getCustomFieldValue(TotalBlockingDurationField).toString();
                                                        TotalBlockingDurationTriggerString = TotalBlockingDurationTriggerString.replaceAll(" d", "");
                                                        TotalBlockingDurationTrigger = Double.valueOf(TotalBlockingDurationTrigger + TotalBlockingDurationTriggerString);
                                                    }
                                                    Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                    for (Issue linkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
                                                        if (linkedIssueOfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO2) {
                                                            if (linkedIssueOfPWO1.getCustomFieldValue(IA_BlockedField).toString() == "Yes") {
                                                                BlockedTrigger = "Yes";
                                                            }
                                                            if (linkedIssueOfPWO1.getCustomFieldValue(TotalBlockingDurationField) != null) {
                                                                TotalBlockingDurationTriggerString = linkedIssueOfPWO1.getCustomFieldValue(TotalBlockingDurationField).toString();
                                                                TotalBlockingDurationTriggerString = TotalBlockingDurationTriggerString.replaceAll(" d", "");
                                                                TotalBlockingDurationTrigger = TotalBlockingDurationTrigger + Double.valueOf(TotalBlockingDurationTriggerString);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            //Set data on trigger;
                            FieldConfig fieldConfigBlockedTrigger = IA_BlockedField.getRelevantConfig(LinkedIssueOfCluster);
                            Option valueBlockedTrigger = ComponentAccessor.getOptionsManager().getOptions(fieldConfigBlockedTrigger)?.find {
                                it.toString() == BlockedTrigger;
                            }

                            MutableIssue mutableIssue = issueManager.getIssueObject(LinkedIssueOfCluster.getKey());
                            mutableIssue.setCustomFieldValue(IA_BlockedField, valueBlockedTrigger);
                            TotalBlockingDurationTrigger = TotalBlockingDurationTrigger.trunc();
                            TotalBlockingDurationTriggerString = TotalBlockingDurationTrigger.trunc() + " d";
                            mutableIssue.setCustomFieldValue(TotalBlockingDurationField, TotalBlockingDurationTriggerString);
                            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                            issueIndexingService.reIndex(mutableIssue);
                        }
                    }
                }
            }
        }
    }

    /**
     *
     * Set costs data on Trigger
     *
     * @param issue
     * @param user
     * @param log
     */
    private static void setCostsDataOnTriggerEditionMode(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
//Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField PartsData_EstimatedCostsField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_ESTIMATEDCOSTS);
        CustomField PartsData_TimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_TIMESPENT);
        CustomField PartsData_EffectiveCostsField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_EFFECTIVECOSTS);
        CustomField MaintPlanning_EstimatedCostsField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_ESTIMATEDCOSTS);
        CustomField MaintPlanning_TimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_TIMESPENT);
        CustomField MaintPlanning_EffectiveCostsField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_EFFECTIVECOSTS);
        CustomField Maint_EstimatedCostsField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_ESTIMATEDCOSTS);
        CustomField Maint_TimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_TIMESPENT);
        CustomField Maint_EffectiveCostsField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_EFFECTIVECOSTS);
        CustomField DomainPWO1Field = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_DOMAINPWO1);
        CustomField IATimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_TIMESPENT);
        CustomField IAEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_EFFECTIVECOST);
        CustomField AuthTimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_TIMESPENT);
        CustomField AuthEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_EFFECTIVECOST);
        CustomField FormTimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_TIMESPENT);
        CustomField FormEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_EFFECTIVECOST);
        CustomField IntTimeSpentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_TIMESPENT);
        CustomField IntEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_EFFECTIVECOST);
        CustomField TriggerTotalEffectiveCostsField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_TRIGGERTOTALEFFECTIVECOSTS);

        //Variables
        String DomainOfPWO2;
        double IAEffectiveCost;

        double PartsDataEstimatedCosts = 0;
        double MaintPlanningEstimatedCosts = 0;
        double MaintEstimatedCosts = 0;
        long PartsDataTimeSpent = 0;
        long MaintPlanningTimeSpent = 0;
        long MaintTimeSpent = 0;
        double PartsDataEffectiveCostsOfTrigger = 0;
        double MaintPlanningEffectiveCostsOfTrigger = 0;
        double MaintEffectiveCostsOfTrigger = 0;
        String PartsDataTimeSpentString;
        String MaintPlanningTimeSpentString;
        String MaintTimeSpenString;
        long IATimeSpentCurrent;
        long PartsDataTimeSpentCurrent;
        long MaintPlanningTimeSpentCurrent;
        long MaintTimeSpentCurrent;
        long AuthTimeSpentCurrent;
        long FormTimeSpentCurrent;
        long IntTimeSpentCurrent;
        double AuthEffectiveCost = 0; //Fix
        double FormEffectiveCost = 0; //Fix
        double IntEffectiveCost = 0; //Fix
        double TriggerEffectiveCost = 0; //Fix


        Collection<Issue> LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfCurrentPWO0 : LinkedIssuesOfCurrentPWO) {
            if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                for (Issue LinkedIssueOfCluster : LinkedIssuesOfCluster) {
                    if (LinkedIssueOfCluster.getIssueTypeId() == ConstantManager.ID_IT_TRIGGER) {
                        log.debug("Trigger");
                        //Reset variables time spent;
                        PartsDataTimeSpent=0;
                        MaintPlanningTimeSpent=0;
                        MaintTimeSpent=0;
                        PartsDataEstimatedCosts=0;
                        MaintPlanningEstimatedCosts=0;
                        MaintEstimatedCosts=0;
                        //Trigger;
                        Collection<Issue> LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues();
                        for (Issue linkedIssueOfTrigger : LinkedIssuesOfTrigger) {
                            if (linkedIssueOfTrigger.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                Collection<Issue> LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                for (Issue linkedIssueOfClusterTrigger : LinkedIssuesOfClusterTrigger) {
                                    if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                                        log.debug("PWO-0 : "+linkedIssueOfClusterTrigger.getKey());

                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EstimatedCostsField) != null) {
                                            PartsDataEstimatedCosts = PartsDataEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EstimatedCostsField);
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EstimatedCostsField) != null) {
                                            MaintPlanningEstimatedCosts = MaintPlanningEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EstimatedCostsField);
                                        }
                                        log.debug("maint estimated : "+linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EstimatedCostsField));
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EstimatedCostsField) != null) {
                                            MaintEstimatedCosts = MaintEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EstimatedCostsField);
                                        }
                                        log.debug("value : "+MaintEstimatedCosts);

                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_TimeSpentField) != null) {
                                            PartsDataTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_TimeSpentField).toString());
                                            PartsDataTimeSpent = PartsDataTimeSpent + PartsDataTimeSpentCurrent;
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_TimeSpentField) != null) {
                                            MaintPlanningTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_TimeSpentField).toString());
                                            MaintPlanningTimeSpent = MaintPlanningTimeSpent + MaintPlanningTimeSpentCurrent;
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_TimeSpentField) != null) {
                                            MaintTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_TimeSpentField).toString());
                                            MaintTimeSpent = MaintTimeSpent + MaintTimeSpentCurrent;
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EffectiveCostsField) != null) {
                                            PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EffectiveCostsField);
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EffectiveCostsField) != null) {
                                            MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EffectiveCostsField);
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EffectiveCostsField) != null) {
                                            MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EffectiveCostsField);
                                        }

                                        Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                        for (Issue linkedIssueOfPWO0 : LinkedIssuesOfPWO0) {
                                            if (linkedIssueOfPWO0.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                                                if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField) != null) {
                                                    IATimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField).toString());
                                                } else {
                                                    IATimeSpentCurrent = 0;
                                                }
                                                if (linkedIssueOfPWO0.getCustomFieldValue(IAEffectiveCostField) != null) {
                                                    IAEffectiveCost = (Double) linkedIssueOfPWO0.getCustomFieldValue(IAEffectiveCostField);
                                                } else {
                                                    IAEffectiveCost = 0;
                                                }

                                                if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Parts Data") {
                                                    if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField) != null) {
                                                        PartsDataTimeSpent = PartsDataTimeSpent + IATimeSpentCurrent;
                                                    }
                                                    PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + IAEffectiveCost;
                                                } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance Planning") {
                                                    if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField) != null) {
                                                        MaintPlanningTimeSpent = MaintPlanningTimeSpent + IATimeSpentCurrent;
                                                    }
                                                    MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + IAEffectiveCost;
                                                } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance") {
                                                    if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField) != null) {
                                                        MaintTimeSpent = MaintTimeSpent + IATimeSpentCurrent;
                                                    }
                                                    MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + IAEffectiveCost;
                                                }

                                                Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                for (Issue linkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
                                                    if (linkedIssueOfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO2) {
                                                        DomainOfPWO2 = linkedIssueOfPWO1.getCustomFieldValue(DomainPWO1Field).toString(); //fix
                                                        //Get PWO-2's subtask;
                                                        Collection<Issue> LinkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects();
                                                        for (Issue linkedIssueOfPWO2 : LinkedIssuesOfPWO2) {
                                                            if (linkedIssueOfPWO2.getIssueTypeId() == ConstantManager.ID_IT_PWO21) {
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(AuthTimeSpentField) != null) {
                                                                    AuthTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(AuthTimeSpentField).toString());
                                                                } else {
                                                                    AuthTimeSpentCurrent = 0;
                                                                }
                                                                log.debug("Auth time spent current : "+AuthTimeSpentCurrent);
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(AuthEffectiveCostField) != null) {
                                                                    AuthEffectiveCost = (Double) linkedIssueOfPWO2.getCustomFieldValue(AuthEffectiveCostField);
                                                                }
                                                                if (DomainOfPWO2 == "Parts Data"){
                                                                    if(AuthEffectiveCost != 0) {
                                                                        PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + AuthEffectiveCost;
                                                                    }
                                                                    PartsDataTimeSpent = PartsDataTimeSpent + AuthTimeSpentCurrent;
                                                                } else if (DomainOfPWO2 == "Maintenance Planning"){
                                                                    if (AuthEffectiveCost != 0){
                                                                        MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + AuthEffectiveCost;
                                                                    }
                                                                    MaintPlanningTimeSpent = MaintPlanningTimeSpent + AuthTimeSpentCurrent;
                                                                } else if (DomainOfPWO2 == "Maintenance"){
                                                                    if(AuthEffectiveCost != 0) {
                                                                        MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + AuthEffectiveCost;
                                                                    }
                                                                    MaintTimeSpent = MaintTimeSpent + AuthTimeSpentCurrent;
                                                                }
                                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ConstantManager.ID_IT_PWO22) {
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(FormTimeSpentField) != null) {
                                                                    FormTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(FormTimeSpentField).toString());
                                                                } else {
                                                                    FormTimeSpentCurrent = 0;
                                                                }
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(FormEffectiveCostField) != null) {
                                                                    FormEffectiveCost = (Double) linkedIssueOfPWO2.getCustomFieldValue(FormEffectiveCostField);
                                                                }
                                                                if (DomainOfPWO2 == "Parts Data"){
                                                                    if(FormEffectiveCost != 0) {
                                                                        PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + FormEffectiveCost;
                                                                    }
                                                                    PartsDataTimeSpent = PartsDataTimeSpent + FormTimeSpentCurrent;
                                                                } else if (DomainOfPWO2 == "Maintenance Planning"){
                                                                    if(FormEffectiveCost != 0) {
                                                                        MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + FormEffectiveCost;
                                                                    }
                                                                    MaintPlanningTimeSpent = MaintPlanningTimeSpent + FormTimeSpentCurrent;
                                                                } else if (DomainOfPWO2 == "Maintenance"){
                                                                    if(FormEffectiveCost != 0) {
                                                                        MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + FormEffectiveCost;
                                                                    }
                                                                    MaintTimeSpent = MaintTimeSpent + FormTimeSpentCurrent;
                                                                }
                                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ConstantManager.ID_IT_PWO23) {
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(IntTimeSpentField) != null) {
                                                                    IntTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(IntTimeSpentField).toString());
                                                                } else {
                                                                    IntTimeSpentCurrent = 0;
                                                                }
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(IntEffectiveCostField) != null) {
                                                                    IntEffectiveCost = (Double) linkedIssueOfPWO2.getCustomFieldValue(IntEffectiveCostField);
                                                                }
                                                                if (DomainOfPWO2 == "Parts Data"){
                                                                    if(IntEffectiveCost != 0) {
                                                                        PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + IntEffectiveCost;
                                                                    }
                                                                    PartsDataTimeSpent = PartsDataTimeSpent + IntTimeSpentCurrent;
                                                                } else if (DomainOfPWO2 == "Maintenance Planning"){
                                                                    if(IntEffectiveCost != 0) {
                                                                        MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + IntEffectiveCost;
                                                                    }
                                                                    MaintPlanningTimeSpent = MaintPlanningTimeSpent + IntTimeSpentCurrent;
                                                                } else if (DomainOfPWO2 == "Maintenance"){
                                                                    if(IntEffectiveCost != 0) {
                                                                        MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + IntEffectiveCost;
                                                                    }
                                                                    MaintTimeSpent = MaintTimeSpent + IntTimeSpentCurrent;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //Set data on trigger;
                        log.debug("setting estimated cost on trigger : "+LinkedIssueOfCluster.getKey());
                        MutableIssue mutableIssue = issueManager.getIssueObject(LinkedIssueOfCluster.getKey());
                        mutableIssue.setCustomFieldValue(PartsData_EstimatedCostsField, PartsDataEstimatedCosts);
                        mutableIssue.setCustomFieldValue(MaintPlanning_EstimatedCostsField, MaintPlanningEstimatedCosts);
                        mutableIssue.setCustomFieldValue(Maint_EstimatedCostsField, MaintEstimatedCosts);
                        log.debug("updating with user IMS : "+ user);
                        //issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                        //issueIndexingService.reIndex(mutableIssue);

                        TriggerEffectiveCost = PartsDataEffectiveCostsOfTrigger + MaintPlanningEffectiveCostsOfTrigger + MaintEffectiveCostsOfTrigger;
                        mutableIssue.setCustomFieldValue(TriggerTotalEffectiveCostsField, TriggerEffectiveCost);
                        mutableIssue.setCustomFieldValue(PartsData_EffectiveCostsField, PartsDataEffectiveCostsOfTrigger);
                        mutableIssue.setCustomFieldValue(MaintPlanning_EffectiveCostsField, MaintPlanningEffectiveCostsOfTrigger);
                        mutableIssue.setCustomFieldValue(Maint_EffectiveCostsField, MaintEffectiveCostsOfTrigger);
                        //LinkedIssueOfCluster.setCustomFieldValue(PartsData_EstimatedCostsField, PartsDataEstimatedCosts);
                        //LinkedIssueOfCluster.setCustomFieldValue(MaintPlanning_EstimatedCostsField, MaintPlanningEstimatedCosts);
                        //LinkedIssueOfCluster.setCustomFieldValue(Maint_EstimatedCostsField, MaintEstimatedCosts);
                        PartsDataTimeSpentString = Toolbox.formatSecondesValueInHoursMinutes(PartsDataTimeSpent);
                        mutableIssue.setCustomFieldValue(PartsData_TimeSpentField, PartsDataTimeSpentString);
                        MaintPlanningTimeSpentString = Toolbox.formatSecondesValueInHoursMinutes(MaintPlanningTimeSpent);
                        mutableIssue.setCustomFieldValue(MaintPlanning_TimeSpentField, MaintPlanningTimeSpentString);
                        MaintTimeSpenString = Toolbox.formatSecondesValueInHoursMinutes(MaintTimeSpent);
                        mutableIssue.setCustomFieldValue(Maint_TimeSpentField, MaintTimeSpenString);
                        issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                        issueIndexingService.reIndex(LinkedIssueOfCluster);
                    }
                }
            }
        }
    }

    private static void fireEscalatedChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField EscalatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_ESCALATED);
        CustomField EscalateStartDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_ESCALATESTARTDATE);
        CustomField OldEscalateStartDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_OLDESCALATESTARTDATE);
        CustomField EscalateEndDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_ESCALATEENDDATE);
        CustomField OldEscalateEndDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_OLDESCALATEENDDATE);
        CustomField TotalEscalatedDurationField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_TOTALESCALATEDDURATION);

        //Variables
        String TotalEscalatedDurationString;
        Date TodayDate = new LocalDate(new Date()).toDate();
        Timestamp TodaysDateCompare = new Timestamp(TodayDate.getTime());
        Date EscalateStartDate;
        Date EscalateEndDate;
        double TotalEscalatedDuration;
        String TotalEscalatedDurationTriggerString;
        double TotalEscalatedDurationTrigger;
        double TotalEscalatedDurationOld;
        String EscalatedTrigger;

        if (issue.getCustomFieldValue(EscalatedField).toString() == "Yes") {
            EscalateEndDate = null;
            issue.setCustomFieldValue(EscalateEndDateField, EscalateEndDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);

            if (issue.getCustomFieldValue(OldEscalateStartDateField) == null || issue.getCustomFieldValue(OldEscalateStartDateField) != TodaysDateCompare) {
                log.debug("Start date to update");
                EscalateStartDate = TodaysDateCompare;
                issue.setCustomFieldValue(EscalateStartDateField, EscalateStartDate);
                issue.setCustomFieldValue(OldEscalateStartDateField, EscalateStartDate);

                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);
            }


            EscalatedTrigger = "No";
            Collection<Issue> LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssueOfCurrentPWO0 : LinkedIssuesOfCurrentPWO) {
                if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                    Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                    for (Issue LinkedIssueOfCluster : LinkedIssuesOfCluster) {
                        if (LinkedIssueOfCluster.getIssueTypeId() == ConstantManager.ID_IT_TRIGGER) {
                            //Trigger;
                            Collection<Issue> LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues();
                            for (Issue linkedIssueOfTrigger : LinkedIssuesOfTrigger) {
                                if (linkedIssueOfTrigger.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                    Collection<Issue> LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                    for (Issue linkedIssueOfClusterTrigger : LinkedIssuesOfClusterTrigger) {
                                        if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                                            log.debug("PWO-0 : " + linkedIssueOfClusterTrigger.getKey());
                                            //Get total blocking duration of PWO0;
                                            if (linkedIssueOfClusterTrigger.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                EscalatedTrigger = "Yes";
                                            }
                                            Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                            for (Issue linkedIssueOfPWO0 : LinkedIssuesOfPWO0) {
                                                if (linkedIssueOfPWO0.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                                                    //Get total blocking duration of PWO1;
                                                    if (linkedIssueOfPWO0.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                        EscalatedTrigger = "Yes";
                                                    }
                                                    Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                    for (Issue linkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
                                                        if (linkedIssueOfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO2) {
                                                            if (linkedIssueOfPWO1.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                                EscalatedTrigger = "Yes";
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            //Set data on trigger;
                            FieldConfig fieldConfigEscalatedTrigger = EscalatedField.getRelevantConfig(LinkedIssueOfCluster);
                            Option valueEscalatedTrigger = ComponentAccessor.getOptionsManager().getOptions(fieldConfigEscalatedTrigger)?.find {
                                it.toString() == EscalatedTrigger;
                            }
                            MutableIssue mutableIssue = issueManager.getIssueObject(LinkedIssueOfCluster.getKey());
                            mutableIssue.setCustomFieldValue(EscalatedField, valueEscalatedTrigger);
                            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                            issueIndexingService.reIndex(mutableIssue);
                        }
                    }
                }
            }
        } else {
            EscalateEndDate = TodaysDateCompare;
            issue.setCustomFieldValue(EscalateEndDateField, EscalateEndDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);

            EscalateStartDate = (Date) issue.getCustomFieldValue(EscalateStartDateField);
            Date OldEscalatedDate = (Date) issue.getCustomFieldValue(OldEscalateEndDateField);
            log.debug("old escalated date : " + OldEscalatedDate);
            log.debug("todaysdate : " + TodaysDateCompare);

            if (issue.getCustomFieldValue(OldEscalateEndDateField) == null || issue.getCustomFieldValue(OldEscalateEndDateField) != TodaysDateCompare) {
                log.debug("set old date");
                issue.setCustomFieldValue(OldEscalateEndDateField, EscalateEndDate);
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);

                if (issue.getCustomFieldValue(TotalEscalatedDurationField) != null) {
                    TotalEscalatedDurationOld = Double.valueOf(issue.getCustomFieldValue(TotalEscalatedDurationField).toString().replace(" d", ""));
                    if (issue.getCustomFieldValue(OldEscalateStartDateField) == TodaysDateCompare || ((Date)issue.getCustomFieldValue(OldEscalateStartDateField)).after(OldEscalatedDate)) {
                        TotalEscalatedDuration = Toolbox.daysBetween(EscalateEndDate, EscalateStartDate) + 1;
                    } else {
                        TotalEscalatedDuration = Toolbox.daysBetween(EscalateEndDate, EscalateStartDate);
                    }
                    TotalEscalatedDuration = TotalEscalatedDurationOld + TotalEscalatedDuration;
                } else {
                    TotalEscalatedDuration = Toolbox.daysBetween(EscalateEndDate, EscalateStartDate) + 1;
                }
                TotalEscalatedDuration = TotalEscalatedDuration.trunc();
                TotalEscalatedDurationString = TotalEscalatedDuration.trunc() + " d";

                issue.setCustomFieldValue(TotalEscalatedDurationField, TotalEscalatedDurationString);

                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);

            }


            //Calculate Blocking duration for trigger
            TotalEscalatedDurationTrigger = 0;
            EscalatedTrigger = "No";
            Collection<Issue> LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssueOfCurrentPWO0 : LinkedIssuesOfCurrentPWO) {
                if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                    Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                    for (Issue LinkedIssueOfCluster : LinkedIssuesOfCluster) {
                        if (LinkedIssueOfCluster.getIssueTypeId() == ConstantManager.ID_IT_TRIGGER) {
                            //Trigger;
                            Collection<Issue> LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues();
                            for (Issue linkedIssueOfTrigger : LinkedIssuesOfTrigger) {
                                if (linkedIssueOfTrigger.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                                    Collection<Issue> LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                    for (Issue linkedIssueOfClusterTrigger : LinkedIssuesOfClusterTrigger) {
                                        if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                                            //Get total blocking duration of PWO0;
                                            if (linkedIssueOfClusterTrigger.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                EscalatedTrigger = "Yes";
                                            }
                                            if (linkedIssueOfClusterTrigger.getCustomFieldValue(TotalEscalatedDurationField) != null) {
                                                TotalEscalatedDurationTriggerString = linkedIssueOfClusterTrigger.getCustomFieldValue(TotalEscalatedDurationField).toString();
                                                TotalEscalatedDurationTriggerString = TotalEscalatedDurationTriggerString.replaceAll(" d", "");
                                                TotalEscalatedDurationTrigger = TotalEscalatedDurationTrigger + Double.valueOf(TotalEscalatedDurationTriggerString);
                                            }
                                            Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                            for (Issue linkedIssueOfPWO0 : LinkedIssuesOfPWO0) {
                                                if (linkedIssueOfPWO0.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                                                    //Get total blocking duration of PWO1;
                                                    if (linkedIssueOfPWO0.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                        EscalatedTrigger = "Yes";
                                                    }
                                                    if (linkedIssueOfPWO0.getCustomFieldValue(TotalEscalatedDurationField) != null) {
                                                        TotalEscalatedDurationTriggerString = linkedIssueOfPWO0.getCustomFieldValue(TotalEscalatedDurationField).toString();
                                                        TotalEscalatedDurationTriggerString = TotalEscalatedDurationTriggerString.replaceAll(" d", "");
                                                        TotalEscalatedDurationTrigger = TotalEscalatedDurationTrigger + Double.valueOf(TotalEscalatedDurationTriggerString);
                                                    }
                                                    Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                    for (Issue linkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
                                                        if (linkedIssueOfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO2) {
                                                            if (linkedIssueOfPWO1.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                                EscalatedTrigger = "Yes";
                                                            }
                                                            if (linkedIssueOfPWO1.getCustomFieldValue(TotalEscalatedDurationField) != null) {
                                                                TotalEscalatedDurationTriggerString = linkedIssueOfPWO1.getCustomFieldValue(TotalEscalatedDurationField).toString();
                                                                TotalEscalatedDurationTriggerString = TotalEscalatedDurationTriggerString.replaceAll(" d", "");
                                                                TotalEscalatedDurationTrigger = TotalEscalatedDurationTrigger + Double.valueOf(TotalEscalatedDurationTriggerString);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            //Set data on trigger;
                            FieldConfig fieldConfigEscalatedTrigger = EscalatedField.getRelevantConfig(LinkedIssueOfCluster);
                            Option valueEscalatedTrigger = ComponentAccessor.getOptionsManager().getOptions(fieldConfigEscalatedTrigger)?.find {
                                it.toString() == EscalatedTrigger;
                            }
                            MutableIssue mutableIssue = issueManager.getIssueObject(LinkedIssueOfCluster.getKey());
                            mutableIssue.setCustomFieldValue(EscalatedField, valueEscalatedTrigger);
                            TotalEscalatedDurationTrigger = TotalEscalatedDurationTrigger.trunc();
                            TotalEscalatedDurationTriggerString = TotalEscalatedDurationTrigger.trunc() + " d";
                            mutableIssue.setCustomFieldValue(TotalEscalatedDurationField, TotalEscalatedDurationTriggerString);
                            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                            issueIndexingService.reIndex(mutableIssue);
                        }
                    }
                }
            }
        }
    }

    private static void fireMaintValidatedChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField MaintValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_VALIDATED);
        CustomField Maint_Validation_1stDeliveryField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_VALIDATION_1STDELIVERY);
        CustomField Maint_Validation_LastDeliveryField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_VALIDATION_LASTDELIVERY);
        CustomField MaintStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTSTATUS);
        CustomField Maint_Valdidation_1stDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_VALDIDATION_1STDELIVERYDATE);
        CustomField Maint_Valdidation_LastDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_VALDIDATION_LASTDELIVERYDATE);
        CustomField Maint_ReworkDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_REWORKDATE);


        //Variables
        String MaintStatus;
        Date TodayDate = new LocalDate(new Date()).toDate();
        Timestamp TodaysDateCompare = new Timestamp(TodayDate.getTime());
        Date Maint_Valdidation_1stDeliveryDate;
        Date Maint_Valdidation_LastDeliveryDate;
        Date Maint_ReworkDate;


        if (issue.getCustomFieldValue(Maint_Validation_1stDeliveryField) == null) {
            Maint_Valdidation_1stDeliveryDate = TodaysDateCompare;
            issue.setCustomFieldValue(Maint_Validation_1stDeliveryField, Maint_Valdidation_1stDeliveryDate);
        }
        Maint_Valdidation_LastDeliveryDate = TodaysDateCompare;
        issue.setCustomFieldValue(Maint_Validation_LastDeliveryField, Maint_Valdidation_LastDeliveryDate);
        if (issue.getCustomFieldValue(MaintValidatedField).toString() == "Rejected") {
            MaintStatus = "In progress";

            FieldConfig fieldConfigMaintStatus = MaintStatusField.getRelevantConfig(issue);
            Option valueMaintStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigMaintStatus)?.find {
                it.toString() == MaintStatus;
            }

            Maint_ReworkDate = Toolbox.incrementTimestamp(TodaysDateCompare, 7);

            issue.setCustomFieldValue(MaintStatusField, valueMaintStatus);
            issue.setCustomFieldValue(Maint_ReworkDateField, Maint_ReworkDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
        if (issue.getCustomFieldValue(MaintValidatedField).toString() == "Validated" || issue.getCustomFieldValue(MaintValidatedField).toString() == "Rejected") {
            Maint_Valdidation_1stDeliveryDate = TodaysDateCompare;
            Maint_Valdidation_LastDeliveryDate = TodaysDateCompare;

            if (issue.getCustomFieldValue(Maint_Valdidation_1stDeliveryDateField) == null) {
                issue.setCustomFieldValue(Maint_Valdidation_1stDeliveryDateField, Maint_Valdidation_1stDeliveryDate);
            }
            issue.setCustomFieldValue(Maint_Valdidation_LastDeliveryDateField, Maint_Valdidation_LastDeliveryDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
    }

    private static void firePartsDataValidatedChanged(MutableIssue issue, ApplicationUser user, GenericValue changeLog, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField PartsDataValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_VALIDATED);
        CustomField PartsData_Validation_1stDeliveryField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_VALIDATION_1STDELIVERY);
        CustomField PartsData_Validation_LastDeliveryField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_VALIDATION_LASTDELIVERY);
        CustomField PartsDataStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATASTATUS);
        CustomField PartsData_Valdidation_1stDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_VALIDATION_1STDELIVERYDATE);
        CustomField PartsData_Valdidation_LastDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_VALIDATION_LASTDELIVERYDATE);
        CustomField PartsData_ReworkDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_REWORKDATE);

        //Variables;
        String PartsDataStatus;
        Date PartsData_ReworkDate;
        Date TodayDate = new LocalDate(new Date()).toDate();
        Timestamp TodaysDateCompare = new Timestamp(TodayDate.getTime());

        Date PartsData_Valdidation_1stDeliveryDate;
        Date PartsData_Valdidation_LastDeliveryDate;


        //Business;
        if (issue.getCustomFieldValue(PartsData_Validation_1stDeliveryField) == null) {
            PartsData_Valdidation_1stDeliveryDate = TodaysDateCompare;
            issue.setCustomFieldValue(PartsData_Validation_1stDeliveryField, PartsData_Valdidation_1stDeliveryDate);
        }
        PartsData_Valdidation_LastDeliveryDate = TodaysDateCompare;
        issue.setCustomFieldValue(PartsData_Validation_LastDeliveryField, PartsData_Valdidation_LastDeliveryDate);
        if (issue.getCustomFieldValue(PartsDataValidatedField).toString() == "Rejected") {
            PartsDataStatus = "In progress";

            FieldConfig fieldConfigPartsDataStatus = PartsDataStatusField.getRelevantConfig(issue);
            Option valuePartsDataStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigPartsDataStatus)?.find {
                it.toString() == PartsDataStatus;
            }

            PartsData_ReworkDate = Toolbox.incrementTimestamp(TodaysDateCompare, 7);

            issue.setCustomFieldValue(PartsDataStatusField, valuePartsDataStatus);
            issue.setCustomFieldValue(PartsData_ReworkDateField, PartsData_ReworkDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
        if (issue.getCustomFieldValue(PartsDataValidatedField).toString() == "Validated" || issue.getCustomFieldValue(PartsDataValidatedField).toString() == "Rejected") {
            PartsData_Valdidation_1stDeliveryDate = TodaysDateCompare;
            PartsData_Valdidation_LastDeliveryDate = TodaysDateCompare;

            if (issue.getCustomFieldValue(PartsData_Valdidation_1stDeliveryDateField) == null) {
                issue.setCustomFieldValue(PartsData_Valdidation_1stDeliveryDateField, PartsData_Valdidation_1stDeliveryDate);
            }
            issue.setCustomFieldValue(PartsData_Valdidation_LastDeliveryDateField, PartsData_Valdidation_LastDeliveryDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
    }

    private static void fireRestrictedDataChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField RestrictedDataField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_RESTRICTEDDATA);
        CustomField FirstSNAppliField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FIRSTSNAPPLI);

        //Variables

        String RestrictedDataTrigger = "No";

        //Business;
        List<String> itemsChangedAtCreation = new ArrayList<String>();
        itemsChangedAtCreation.add(FirstSNAppliField.getFieldName());
        log.debug("issue = " + issue.getKey());

        Collection<Issue> LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfCurrentPWO0 : LinkedIssuesOfCurrentPWO) {
            if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantManager.ID_IT_CLUSTER) {
                Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                for (Issue linkedIssueOfCluster : LinkedIssuesOfCluster) {
                    if (linkedIssueOfCluster.getIssueTypeId() == ConstantManager.ID_IT_PWO0) {
                        if (linkedIssueOfCluster.getCustomFieldValue(RestrictedDataField).toString() == "Yes") {
                            RestrictedDataTrigger = "Yes";
                        } else if (RestrictedDataTrigger != "Yes" && linkedIssueOfCluster.getCustomFieldValue(RestrictedDataField).toString() == "Unknown") {
                            RestrictedDataTrigger = "Unknown";
                        }
                        Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                        for (Issue linkedIssueOfPWO : LinkedIssuesOfPWO0) {
                            if (linkedIssueOfPWO.getIssueTypeId() == ConstantManager.ID_IT_PWO1) {
                                if (linkedIssueOfPWO.getCustomFieldValue(RestrictedDataField).toString() == "Yes") {
                                    RestrictedDataTrigger = "Yes";
                                } else if (RestrictedDataTrigger != "Yes" && linkedIssueOfPWO.getCustomFieldValue(RestrictedDataField).toString() == "Unknown") {
                                    RestrictedDataTrigger = "Unknown";
                                }
                            }
                            Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO, user).getAllIssues();
                            for (Issue linkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
                                if (linkedIssueOfPWO1.getIssueTypeId() == ConstantManager.ID_IT_PWO2) {
                                    if (linkedIssueOfPWO1.getCustomFieldValue(RestrictedDataField).toString() == "Yes") {
                                        RestrictedDataTrigger = "Yes";
                                    } else if (RestrictedDataTrigger != "Yes" && linkedIssueOfPWO1.getCustomFieldValue(RestrictedDataField).toString() == "Unknown") {
                                        RestrictedDataTrigger = "Unknown";
                                    }
                                }
                            }
                        }
                    }
                }
                FieldConfig fieldConfigRestrictedData = RestrictedDataField.getRelevantConfig(linkedIssueOfCurrentPWO0);
                Option valueRestrictedData = ComponentAccessor.getOptionsManager().getOptions(fieldConfigRestrictedData)?.find {
                    it.toString() == RestrictedDataTrigger;
                }

                //Set Restricted data on Cluster;
                MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfCurrentPWO0.getKey());
                mutableIssue.setCustomFieldValue(RestrictedDataField, valueRestrictedData);
                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(mutableIssue);

                //Set Restricted data on Triggers;
                Collection<Issue> LinkedTriggersOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                for (Issue linkedTriggerOfCluster : LinkedTriggersOfCluster) {
                    if (linkedTriggerOfCluster.getIssueTypeId() == ConstantManager.ID_IT_TRIGGER) {
                        FieldConfig fieldConfigRestrictedDataTrigger = RestrictedDataField.getRelevantConfig(linkedTriggerOfCluster);
                        Option valueRestrictedDataTrigger = ComponentAccessor.getOptionsManager().getOptions(fieldConfigRestrictedDataTrigger)?.find {
                            it.toString() == RestrictedDataTrigger;
                        }
                        mutableIssue = issueManager.getIssueObject(linkedTriggerOfCluster.getKey());
                        mutableIssue.setCustomFieldValue(RestrictedDataField, valueRestrictedDataTrigger);
                        issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                        issueIndexingService.reIndex(mutableIssue);
                    }
                }
            }
        }
    }
}
