package com.airbus.ims.business;

import com.airbus.ims.util.Toolbox;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.ChangeHistoryItem;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.option.Option;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl;
import groovy.util.GroovyScriptEngine;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.ofbiz.core.entity.GenericValue;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

/**
 * IMS Businness Rules Manager
 */
public class WorkflowOperations {


    /**
     * Initialize the trigger
     * @param issue
     * @return
     */
    public static boolean initializeTrigger(Issue issue) {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        boolean passesCondition = true;
        if (issueLinkManager.getInwardLinks(issue.getId()) != null) {
            issueLinkManager.getInwardLinks(issue.getId()).each{ issueLink ->
                Issue sourceIssue = (Issue)issueLink.getSourceObject();
                String issueType = sourceIssue.getIssueTypeId();
                if (issueType.equals(ConstantManager.ID_IT_CLUSTER)) {
                    passesCondition = false;
                }
            };
        }
        return passesCondition;
    }

    /**
     * Creation/Edit of a PWO, update of the reference
     * @param mutableIssue
     * @param user
     * @param log
     * @param pwoType
     */
    public static void createOrEditPWO(MutableIssue mutableIssue, ApplicationUser user, Logger log, String pwoType) {
        log.warn("PWO created UpdateTitleCopyToSummary");
        log.debug("Creation/Edit of a PWO, update of the reference");

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        JqlQueryParser jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser.class);
        SearchProvider searchProvider = ComponentAccessor.getComponent(SearchProvider.class);

        //CustomField
        CustomField programField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PROGRAM);
        CustomField referenceField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_REFERENCE);
        CustomField incrementalIDField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INCREMENTALID);

        //Business

        log.debug("issue : " + mutableIssue.getKey());

        String valueToReturn;
        String programValueBrut;
        //String[] ProgramValueList
        String programValue;
        String yearValue;
        String JQL_QUERY = "project = \"%s\" and issuetype in  (\"%s\") AND created > startOfYear() and \"Program\"=\"%s [Program_%s]\" and \"Incremental ID\" is not empty ORDER BY \"Incremental ID\" DESC";
        String completeCompteur;
        String query;
        double compteur = 0;

        List<Issue> issuesFound = new ArrayList<Issue>();
        Issue firstIssue = null;


        programValueBrut = ((String) (mutableIssue.getCustomFieldValue(programField)));
        programValue = programValueBrut.replaceAll("Program_", "").replaceAll("<content>", "").replaceAll("<value>", "").replaceAll("</value>", ",").replaceAll("</content>", "").replaceAll(",", "").replaceAll("\n", "").replaceAll("  ", "");

        String dateCreated = mutableIssue.getCreated().toString();
        yearValue = dateCreated.replaceAll("/ -.*/","");

        log.debug("issue type : " + mutableIssue.getIssueTypeId());

        try {
            query = String.format(JQL_QUERY, mutableIssue.getProjectObject().getName(), mutableIssue.getIssueType().getName(), programValue, programValue, mutableIssue.getId());
            log.debug("Query : " + query);
            Query jqlQuery = jqlQueryParser.parseQuery(query);
            SearchResults results = searchProvider.search(jqlQuery, user, PagerFilter.getUnlimitedFilter());
            issuesFound = results.getIssues();
            log.debug("issues found : " + issuesFound);
            log.debug("issues found size : " + issuesFound.size());
            if (issuesFound.size() > 0) {
                if (!issuesFound.get(0).getKey().equals(mutableIssue.getKey())) {
                    firstIssue = issuesFound.get(0);
                } else if (issuesFound.get(1) != null) {
                    firstIssue = issuesFound.get(1);
                } else {
                    compteur = 1;
                }

                log.debug("issue : " + firstIssue);
                //if (compteur != null && firstIssue.getCustomFieldValue(incrementalIDField) != null) {
                if (firstIssue.getCustomFieldValue(incrementalIDField) != null) {
                    compteur = Double.parseDouble(firstIssue.getCustomFieldValue(incrementalIDField).toString());
                    compteur = Math.round(compteur + 1);
                }

                log.debug("compteur : " + compteur);
            } else {
                compteur = 1;
            }


            if (compteur < 10) {
                completeCompteur = "0000";
            } else if (compteur < 100) {
                completeCompteur = "000";
            } else if (compteur < 1000) {
                completeCompteur = "00";
            } else if (compteur < 10000) {
                completeCompteur = "0";
            } else {
                completeCompteur = "";
            }


            valueToReturn = programValue + "_" + yearValue + "_" + pwoType + "_" + completeCompteur + Math.round(compteur);
            log.debug("Value : " + valueToReturn);

            mutableIssue.setCustomFieldValue(referenceField, valueToReturn);
            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);

            mutableIssue.setCustomFieldValue(incrementalIDField, compteur);
            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);

            mutableIssue.setSummary(valueToReturn);
            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (SearchException e1) {
            e1.printStackTrace();
        } catch (JqlParseException e2) {
            e2.printStackTrace();
        } catch (IndexException e3) {
            e3.printStackTrace();
        }
    }

    /**
     * Set the security Level for Sub-PWO Sub PWO (PWO21, PWO22, PWO23)
     * @param mutableIssue
     * @param log
     */
    public static void setSecurityLevelForSubPWO(MutableIssue mutableIssue, Logger log) {
        IssueSecuritySchemeManager issueSecuritySchemeManager = ComponentAccessor.getComponent(IssueSecuritySchemeManager.class);
        IssueSecurityLevelManager issueSecurityLevelManager = ComponentAccessor.getComponent(IssueSecurityLevelManager.class);
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        Scheme schemeFromName = issueSecuritySchemeManager.getSchemeObjects()?.find{
                it.getName().equals("Program");
        };

        CustomField programField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PROGRAM);

        String programnFeed = mutableIssue.getCustomFieldValue(programField).toString();
        String[] moanFeedFormatted = programnFeed.substring(19).replaceAll("</value>/\n/</content>", "").replaceAll("</value>", ",").replaceAll("<value>", "").replaceAll( "/\n /", "").replace("  ", " ").split(",");
        log.debug("MOA nFeed formatted : " + moanFeedFormatted[0]);

        IssueSecurityLevel securityLevelfound = issueSecurityLevelManager.getIssueSecurityLevels(schemeFromName.getId())?.find{
                it.getName().equals(moanFeedFormatted[0]);
        };
        Long securityLvl = (securityLevelfound == null ? null : securityLevelfound.getId());
        log.debug("securityLvl : " + securityLvl);

        mutableIssue.setSecurityLevelId(securityLvl);
    }

    /**
     * Check if PWO22 is ready (TO BE COMPLETED)
     * @param issue
     * @param user
     * @return
     */
    public static boolean isPWO22Ready(Issue issue, ApplicationUser user, Logger log) {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        CustomField awoTransWUfield = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AWO_TRANSWU);
        CustomField woTransNbWUfield = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AWO_TRANSNBWU);
        CustomField awoTransCommentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AWO_TRANSCOMMENT);
        CustomField awoFormWU = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AWO_FORMWU);
        CustomField awoFormNbWUField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AWO_FORMNBWU);
        CustomField awoFormCommentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AWO_FORMCOMMENT);
        CustomField awoFormVerifWUField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AWO_FORMVERIFWU);
        CustomField awoFormVerifNbWUField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AWO_FORMVERIFNBWU);
        CustomField awoFormVerifCommentField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AWO_FORMVERIFCOMMENT);
        CustomField awoTransObjLangField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AWO_TRANSOBJLAN);

        String result = "OK";

        Collection<Issue> linkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfCurrentPWO : linkedIssuesOfCurrentPWO) {
            if (linkedIssueOfCurrentPWO.getProjectId() == ConstantManager.ID_PROJECT_AWO) {
                if (linkedIssueOfCurrentPWO.getCustomFieldValue(awoTransObjLangField) != null) {
                    if (!result.equals("KO") && linkedIssueOfCurrentPWO.getCustomFieldValue(awoTransWUfield) != null && linkedIssueOfCurrentPWO.getCustomFieldValue(woTransNbWUfield) != null) {
                        result = "OK";
                    } else {
                        result = "KO";
                    }

                } else {
                    if (!result.equals("KO") && linkedIssueOfCurrentPWO.getCustomFieldValue(awoFormWU) != null && linkedIssueOfCurrentPWO.getCustomFieldValue(awoFormNbWUField) != null && linkedIssueOfCurrentPWO.getCustomFieldValue(awoFormVerifWUField) != null && linkedIssueOfCurrentPWO.getCustomFieldValue(awoFormVerifNbWUField) != null) {
                        result = "OK";
                    } else {
                        result = "KO";
                    }

                }
            }
        }

        boolean passesCondition = false;
        if (result.equals("OK")) {
            passesCondition = true;
        }
        return passesCondition;
    }

    /**
     * TO BE COMPLETED
     * @param issue
     * @param user
     * @return
     */
    public static boolean isPWO23Ready(Issue issue, ApplicationUser user, Logger log) {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        CustomField intWUField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_WU);
        CustomField intNbWUField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_NBWU);

        String result = "OK";

        Collection<Issue> LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfCurrentPWO : LinkedIssuesOfCurrentPWO) {
            if (linkedIssueOfCurrentPWO.getProjectId() == ConstantManager.ID_PROJECT_AWO) {
                if (!result.equals("KO") && linkedIssueOfCurrentPWO.getCustomFieldValue(intWUField) != null && linkedIssueOfCurrentPWO.getCustomFieldValue(intNbWUField) != null) {
                    result = "OK";
                } else {
                    result = "KO";
                }

            }

        }

        boolean passesCondition = false;
        if (result.equals("OK")) {
            passesCondition = true;
        }
        return passesCondition;
    }

    /**
     * Verify Links
     * @param issue
     * @param user
     * @param log
     * @param issueTypeId
     * @return
     */
    public static boolean isLinkedToIssueType(Issue issue, ApplicationUser user, Logger log, String issueTypeId) {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        boolean passesCondition = false;
        Issue sourceIssue;
        if (issueLinkManager.getInwardLinks(issue.getId()) != null) {
            log.debug("inward link");
            issueLinkManager.getInwardLinks(issue.getId()).each{IssueLink issueLink ->
                sourceIssue = issueLink.getSourceObject();
                log.debug("source issue : " + sourceIssue);
                String sourceIssueTypeId = sourceIssue.getIssueTypeId();
                log.debug("sourceIssueTypeId: " + sourceIssueTypeId);
                if (sourceIssueTypeId.equals(String.valueOf(issueTypeId))) {
                    passesCondition = true;
                }
            };
        }

        if (issueLinkManager.getOutwardLinks(issue.getId()) != null) {
            log.debug("outward link");
            issueLinkManager.getOutwardLinks(issue.getId()).each{IssueLink issueLink ->
                sourceIssue = issueLink.getDestinationObject();
                log.debug("source issue : " + sourceIssue);
                String sourceIssueTypeId = sourceIssue.getIssueTypeId();
                log.debug("sourceIssueTypeId: " + sourceIssueTypeId);
                if (sourceIssueTypeId.equals(String.valueOf(issueTypeId))) {
                    passesCondition = true;
                }

            }
        }
        return passesCondition;
    }

    /**
     *
     * @param issue
     * @param user
     * @return TO BE COMPLETED
     */
    public static boolean isPWO21Ready(Issue issue, ApplicationUser user, Logger log) {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        CustomField authWUField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_WU);
        CustomField authNbWUField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_NBWU);
        CustomField authVerifWUField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIF_WU);
        CustomField authVerifNbWUField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIF_NBWU);

        String result = "OK";

        Collection<Issue> linkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfCurrentPWO : linkedIssuesOfCurrentPWO) {
            if (linkedIssueOfCurrentPWO.getProjectId() == ConstantManager.ID_PROJECT_AWO) {
                if (!result.equals("KO") && linkedIssueOfCurrentPWO.getCustomFieldValue(authWUField) != null && linkedIssueOfCurrentPWO.getCustomFieldValue(authNbWUField) != null && linkedIssueOfCurrentPWO.getCustomFieldValue(authVerifWUField) != null && linkedIssueOfCurrentPWO.getCustomFieldValue(authVerifNbWUField) != null) {
                    result = "OK";
                } else {
                    result = "KO";
                }
            }
        }

        boolean passesCondition = false;
        if (result.equals("OK")) {
            passesCondition = true;
        }
        return passesCondition;
    }

    /**
     * Process PWO0 Business Rules on final dates
     * @param mutableIssue
     * @param user
     * @param log
     */
    public static void processPWO0FinalDates(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        final String PATH = "/atlassian/jira/data/scripts/util/Functions.groovy";

        try {
            Object script = new GroovyScriptEngine(".", ScriptRunnerImpl.getScriptRunner().getGroovyScriptEngine().getGroovyClassLoader()).with{
                return loadScriptByName(PATH);
            };
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField preIAPartsDataValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PREIA_PARTSDATA_VALIDATION_REQUESTEDDATE);
        CustomField preIAMaintPlanningValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PREIA_MAINTPLANNING_VALIDATION_REQUESTEDDATE);
        CustomField preIAMaintValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PREIA_MAINT_VALIDATION_REQUESTEDDATE);
        CustomField partsDataValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_VALIDATION_1STCOMMITTEDDATE);
        CustomField maintPlanningValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNINGVALIDATION_1STCOMMITTEDDATE);
        CustomField maintValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.IF_CF_MAINTVALIDATION_1STCOMMITTEDDATE);
        CustomField partsDataValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATAVALIDATION_LASTCOMMITTEDDATE);
        CustomField maintPlanningValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNINGVALIDATION_LASTCOMMITTEDDATE);
        CustomField maintValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.IF_CF_MAINTVALIDATION_LASTCOMMITTEDDATE);
        CustomField requestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_REQUESTEDDATE);

        //Variables
        Timestamp now = new Timestamp(new Date().getTime());
        log.debug("Todays Date : " + now);
        Date preIAPartsDataValidationRequestedDate;
        Date preIAMaintPlanningValidationRequestedDate;
        Date preIAMaintValidationRequestedDate;
        Date partsDataValidation1stCommittedDate;
        Date maintPlanningValidation1stCommittedDate;
        Date maintValidation1stCommittedDate;
        Date partsDataValidationLastCommittedDate;
        Date maintPlanningValidationLastCommittedDate;
        Date maintValidationLastCommittedDate;

        //Business

        preIAPartsDataValidationRequestedDate = Toolbox.incrementTimestamp(now, 7);
        preIAMaintPlanningValidationRequestedDate = Toolbox.incrementTimestamp(now, 7);
        preIAMaintValidationRequestedDate = Toolbox.incrementTimestamp(now, 7);

        partsDataValidation1stCommittedDate = preIAPartsDataValidationRequestedDate;
        maintPlanningValidation1stCommittedDate = preIAMaintPlanningValidationRequestedDate;
        maintValidation1stCommittedDate = preIAMaintValidationRequestedDate;

        partsDataValidationLastCommittedDate = partsDataValidation1stCommittedDate;
        maintPlanningValidationLastCommittedDate = maintPlanningValidation1stCommittedDate;
        maintValidationLastCommittedDate = maintValidation1stCommittedDate;

        try {
            mutableIssue.setCustomFieldValue(preIAPartsDataValidationRequestedDateField, preIAPartsDataValidationRequestedDate);
            mutableIssue.setCustomFieldValue(preIAMaintPlanningValidationRequestedDateField, preIAMaintPlanningValidationRequestedDate);
            mutableIssue.setCustomFieldValue(preIAMaintValidationRequestedDateField, preIAMaintValidationRequestedDate);

            mutableIssue.setCustomFieldValue(partsDataValidation1stCommittedDateField, partsDataValidation1stCommittedDate);
            mutableIssue.setCustomFieldValue(maintPlanningValidation1stCommittedDateField, maintPlanningValidation1stCommittedDate);
            mutableIssue.setCustomFieldValue(maintValidation1stCommittedDateField, maintValidation1stCommittedDate);

            mutableIssue.setCustomFieldValue(partsDataValidationLastCommittedDateField, partsDataValidationLastCommittedDate);
            mutableIssue.setCustomFieldValue(maintPlanningValidationLastCommittedDateField, maintPlanningValidationLastCommittedDate);
            mutableIssue.setCustomFieldValue(maintValidationLastCommittedDateField, maintValidationLastCommittedDate);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO0 Business Rules on dates
     * @param mutableIssue
     * @param user
     * @param log
     */
    public static void processPWO0PreDates(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        /*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
        loadScriptByName(path);
        } */

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField preIAPartsDataRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PREIA_PARTSDATA_REQUESTEDDATE);
        CustomField preIAPartsDataValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PREIA_PARTSDATA_VALIDATION_REQUESTEDDATE);
        CustomField preIAMaintPlanningRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PREIA_MAINTPLANNING_REQUESTEDDATE);
        CustomField preIAMaintPlanningValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PREIA_MAINTPLANNING_VALIDATION_REQUESTEDDATE);
        CustomField preIAMaintRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PREIA_MAINT_REQUESTEDDATE);
        CustomField preIAMaintValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PREIA_MAINT_VALIDATION_REQUESTEDDATE);
        CustomField partsDataStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_STATUS);
        CustomField maintPlanningStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_STATUS);
        CustomField maintStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_STATUS);
        CustomField PartsData_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_1STCOMMITTEDDATE);
        CustomField maintPlanning1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_1STCOMMITTEDDATE);
        CustomField maint1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.IF_CF_MAINT_1STCOMMITTEDDATE);
        CustomField partsDataLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_LASTCOMMITTEDDATE);
        CustomField maintPlanningLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_LASTCOMMITTEDDATE);
        CustomField maintLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.IF_CF_MAINT_LASTCOMMITTEDDATE);
        CustomField partsDataValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_VALIDATION_1STCOMMITTEDDATE);
        CustomField maintPlanningValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNINGVALIDATION_1STCOMMITTEDDATE);
        CustomField maintValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.IF_CF_MAINTVALIDATION_1STCOMMITTEDDATE);
        CustomField partsDataValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATAVALIDATION_LASTCOMMITTEDDATE);
        CustomField maintPlanningValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNINGVALIDATION_LASTCOMMITTEDDATE);
        CustomField maintValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.IF_CF_MAINTVALIDATION_LASTCOMMITTEDDATE);
        CustomField requestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_REQUESTEDDATE);
        CustomField domainField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_DOMAIN);

        //Variables
        //def now = new Timestamp(new Date().getTime())
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        log.debug("Todays Date : " + now);
        Date preIAPartsDataRequestedDate;
        Date preIAMaintPlanningRequestedDate;
        Date preIAMaintRequestedDate;
        Date preIAPartsDataValidationRequestedDate;
        Date preIAMaintPlanningValidationRequestedDate;
        Date preIAMaintValidationRequestedDate;
        Date partsData1stCommittedDate;
        Date maintplanning1stCommittedDate;
        Date maint1stCommittedDate;
        Date partsDataLastCommittedDate;
        Date maintPlanningLastCommittedDate;
        Date maintLastCommittedDate;
        Date partsDataValidation1stCommittedDate;
        Date maintPlanningValidation1stCommittedDate;
        Date maintValidation1stCommittedDate;
        Date partsDataValidationLastCommittedDate;
        Date maintPlanningValidationLastCommittedDate;
        Date maintValidationLastCommittedDate;

        try {
            //Business

            preIAPartsDataRequestedDate = Toolbox.incrementTimestamp(now, 7);
            preIAMaintPlanningRequestedDate = Toolbox.incrementTimestamp(now, 7);
            preIAMaintRequestedDate = Toolbox.incrementTimestamp(now, 7);
            log.debug("preIAMaintRequestedDate : " + preIAMaintRequestedDate);

            preIAPartsDataValidationRequestedDate = Toolbox.incrementTimestamp(now, 14);
            preIAMaintPlanningValidationRequestedDate = Toolbox.incrementTimestamp(now, 14);
            preIAMaintValidationRequestedDate = Toolbox.incrementTimestamp(now, 14)

            FieldConfig fieldConfigPartsDataStatus = partsDataStatusField.getRelevantConfig(mutableIssue);
            FieldConfig fieldConfigMaintPlanningStatus = maintPlanningStatusField.getRelevantConfig(mutableIssue);
            FieldConfig fieldConfigMaintStatus = maintStatusField.getRelevantConfig(mutableIssue);

            String resultsPartsData;
            String resultsMaintPlanning;
            String resultsMaint;

            Object[] domainValues = (Object[])mutableIssue.getCustomFieldValue(domainField);
            log.debug("Domains values = " + Arrays.toString(domainValues));
            for (Object domainValue : domainValues) {
                if ("Parts Data".equals(domainValue.toString())) {
                    resultsPartsData = "In progress";
                } else if (!"In progress".equals(resultsPartsData)) {
                    resultsPartsData = "Cancelled";
                }

                if ("Maint Planning".equals(domainValue.toString())) {
                    resultsMaintPlanning = "In progress";
                } else if (!"In progress".equals(resultsMaintPlanning)) {
                    resultsMaintPlanning = "Cancelled";
                }

                if ("Maintenance".equals(domainValue.toString())) {
                    resultsMaint = "In progress";
                } else if (!"In progress".equals(resultsMaint)) {
                    resultsMaint = "Cancelled";
                }
            }

            Object valuePartsDataStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigPartsDataStatus)?.find{
                    it.toString().equals(resultsPartsData);
            };
            Object valueMaintPlanningStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigMaintPlanningStatus)?.find{
                    it.toString().equals(resultsMaintPlanning);
            };
            Object valueMaintStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigMaintStatus)?.find{ it->
                    it.toString().equals(resultsMaint);
            };

            mutableIssue.setCustomFieldValue(partsDataStatusField, valuePartsDataStatus);
            mutableIssue.setCustomFieldValue(maintPlanningStatusField, valueMaintPlanningStatus);
            mutableIssue.setCustomFieldValue(maintStatusField, valueMaintStatus);

            partsData1stCommittedDate = preIAPartsDataRequestedDate;
            maintplanning1stCommittedDate = preIAMaintPlanningRequestedDate;
            log.debug("maint planning 1st committed : " + maintplanning1stCommittedDate);
            maint1stCommittedDate = preIAMaintRequestedDate;

            partsDataLastCommittedDate = partsData1stCommittedDate;
            maintPlanningLastCommittedDate = maintplanning1stCommittedDate;
            log.debug("maint planning last committed : " + maintPlanningLastCommittedDate);
            maintLastCommittedDate = maint1stCommittedDate;

            partsDataValidation1stCommittedDate = preIAPartsDataValidationRequestedDate;
            maintPlanningValidation1stCommittedDate = preIAMaintPlanningValidationRequestedDate;
            maintValidation1stCommittedDate = preIAMaintValidationRequestedDate;

            partsDataValidationLastCommittedDate = partsDataValidation1stCommittedDate;
            maintPlanningValidationLastCommittedDate = maintPlanningValidation1stCommittedDate;
            maintValidationLastCommittedDate = maintValidation1stCommittedDate;

            mutableIssue.setCustomFieldValue(preIAPartsDataRequestedDateField, preIAPartsDataRequestedDate);
            mutableIssue.setCustomFieldValue(preIAMaintPlanningRequestedDateField, preIAMaintPlanningRequestedDate);
            mutableIssue.setCustomFieldValue(preIAMaintRequestedDateField, preIAMaintRequestedDate);

            mutableIssue.setCustomFieldValue(preIAPartsDataValidationRequestedDateField, preIAPartsDataValidationRequestedDate);
            mutableIssue.setCustomFieldValue(preIAMaintPlanningValidationRequestedDateField, preIAMaintPlanningValidationRequestedDate);
            mutableIssue.setCustomFieldValue(preIAMaintValidationRequestedDateField, preIAMaintValidationRequestedDate);

            mutableIssue.setCustomFieldValue(PartsData_1stCommittedDateField, partsData1stCommittedDate);
            mutableIssue.setCustomFieldValue(maintPlanning1stCommittedDateField, maintplanning1stCommittedDate);
            mutableIssue.setCustomFieldValue(maint1stCommittedDateField, maint1stCommittedDate);

            mutableIssue.setCustomFieldValue(partsDataLastCommittedDateField, partsDataLastCommittedDate);
            log.debug("maint planning last committed avant setting: " + maintPlanningValidationLastCommittedDate);
            mutableIssue.setCustomFieldValue(maintPlanningLastCommittedDateField, maintPlanningValidationLastCommittedDate);
            mutableIssue.setCustomFieldValue(maintLastCommittedDateField, maintLastCommittedDate);

            mutableIssue.setCustomFieldValue(partsDataValidation1stCommittedDateField, partsDataValidation1stCommittedDate);
            mutableIssue.setCustomFieldValue(maintPlanningValidation1stCommittedDateField, maintPlanningValidationLastCommittedDate);
            mutableIssue.setCustomFieldValue(maintValidation1stCommittedDateField, maintValidation1stCommittedDate);

            mutableIssue.setCustomFieldValue(partsDataValidationLastCommittedDateField, partsDataValidationLastCommittedDate);
            mutableIssue.setCustomFieldValue(maintPlanningValidationLastCommittedDateField, maintPlanningValidation1stCommittedDate);
            mutableIssue.setCustomFieldValue(maintValidationLastCommittedDateField, maintValidationLastCommittedDate);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO1 rules on dates
     * @param mutableIssue
     * @param user
     * @param log
     */
    public static void processPWO1Dates(MutableIssue mutableIssue, ApplicationUser user, Logger log){
        /*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
            loadScriptByName(path);
        } */

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField iaRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_REQUESTEDDATE);
        CustomField iaValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_VALIDATION_REQUESTEDDATE);
        CustomField ia1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_1STCOMMITTEDDATE);
        CustomField iaLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_LASTCOMMITTEDDATE);
        CustomField iaValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_VALIDATION_1STCOMMITTEDDATE);
        CustomField iaValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_VALIDATION_LASTCOMMITTEDDATE);


        //Variables
        //def now = new Timestamp(new Date().getTime())
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        log.debug("Todays Date : " + now);
        Date iaRequestedDate;
        Date iaValidationRequestedDate;
        Date ia1stCommittedDate;
        Date iaLastCommittedDate;
        Date iaValidation1stCommittedDate;
        Date iaValidationLastCommittedDate;

        //Business

        try {
            iaRequestedDate = Toolbox.incrementTimestamp(now, 14);
            ia1stCommittedDate = iaRequestedDate;
            iaLastCommittedDate = ia1stCommittedDate;
            iaValidationRequestedDate =  Toolbox.incrementTimestamp(now, 21);
            iaValidation1stCommittedDate = iaValidationRequestedDate;
            iaValidationLastCommittedDate = iaValidation1stCommittedDate;

            mutableIssue.setCustomFieldValue(iaRequestedDateField, iaRequestedDate);
            mutableIssue.setCustomFieldValue(iaValidationRequestedDateField, iaValidationRequestedDate);
            mutableIssue.setCustomFieldValue(ia1stCommittedDateField, ia1stCommittedDate);
            mutableIssue.setCustomFieldValue(iaLastCommittedDateField, iaLastCommittedDate);
            mutableIssue.setCustomFieldValue(iaValidation1stCommittedDateField, iaValidation1stCommittedDate);
            mutableIssue.setCustomFieldValue(iaValidationLastCommittedDateField, iaValidationLastCommittedDate);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO21 Business Rules on dates TO BE COMPLETED
     * @param mutableIssue
     * @param user
     * @param log
     */
    public static void processPWO21Dates(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        /*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
            loadScriptByName(path);
        } */


        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantManager.ID_IT_PWO0);
        PWOIT.add(ConstantManager.ID_IT_PWO1);
        PWOIT.add(ConstantManager.ID_IT_PWO2);
        PWOIT.add(ConstantManager.ID_IT_PWO21);
        PWOIT.add(ConstantManager.ID_IT_PWO22);
        PWOIT.add(ConstantManager.ID_IT_PWO23);

        //CustomField
        CustomField authRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_REQUESTEDDATE);
        CustomField authVerificationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFICATION_REQUESTEDDATE);
        CustomField authValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VALIDATION_REQUESTEDDATE);
        CustomField authFirstCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_FIRSTCOMMITTEDDATE);
        CustomField authVerificationFirstCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFICATION_FIRSTCOMMITTEDDATE);
        CustomField authValidationFirstCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VALIDATION_FIRSTCOMMITTEDDATE);
        CustomField authLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_LASTCOMMITTEDDATE);
        CustomField authVerificationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFICATION_LASTCOMMITTEDDATE);
        CustomField authValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VALIDATION_LASTCOMMITTEDDATE);


        //Variables
        //def now = new Timestamp(new Date().getTime())
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        Date authRequestedDate;
        Date authVerificationRequestedDate;
        Date authValidationRequestedDate;
        Date authFirstCommittedDate;
        Date authVerificationFirstCommittedDate;
        Date authValidationFirstCommittedDate;
        Date authLastCommittedDate;
        Date authVerificationLastCommittedDate;
        Date authValidationLastCommittedDate;

        try {
            //Business
            //Calculate date on PWO-2.1
            authRequestedDate = Toolbox.incrementTimestamp(now, 14);
            authVerificationRequestedDate = Toolbox.incrementTimestamp(now, 28);
            authValidationRequestedDate = Toolbox.incrementTimestamp(now, 42);

            authFirstCommittedDate = authRequestedDate;
            authVerificationFirstCommittedDate = authVerificationRequestedDate;
            authValidationFirstCommittedDate = authValidationRequestedDate;

            authLastCommittedDate = authFirstCommittedDate;
            authVerificationLastCommittedDate = authVerificationFirstCommittedDate;
            authValidationLastCommittedDate = authValidationFirstCommittedDate;


            mutableIssue.setCustomFieldValue(authRequestedDateField, authRequestedDate);
            mutableIssue.setCustomFieldValue(authVerificationRequestedDateField, authVerificationRequestedDate);
            mutableIssue.setCustomFieldValue(authValidationRequestedDateField, authValidationRequestedDate);

            mutableIssue.setCustomFieldValue(authFirstCommittedDateField, authFirstCommittedDate);
            mutableIssue.setCustomFieldValue(authVerificationFirstCommittedDateField, authVerificationFirstCommittedDate);
            mutableIssue.setCustomFieldValue(authValidationFirstCommittedDateField, authValidationFirstCommittedDate);

            mutableIssue.setCustomFieldValue(authLastCommittedDateField, authLastCommittedDate);
            mutableIssue.setCustomFieldValue(authVerificationLastCommittedDateField, authVerificationLastCommittedDate);
            mutableIssue.setCustomFieldValue(authValidationLastCommittedDateField, authValidationLastCommittedDate);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO21 Business Rules on Authoring Validation
     * @param mutableIssue
     * @param user
     * @param log
     */
    public static void processPWO21AuthoringValidation(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        try {
            Object script = new GroovyScriptEngine(".", ScriptRunnerImpl.getScriptRunner().getGroovyScriptEngine().getGroovyClassLoader()).with{
                return loadScriptByName(ConstantManager.GROOVY_PATH_FUNCTION);
            };
        } catch(IOException e) {
            e.printStackTrace();
        }

        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantManager.ID_IT_PWO0);
        PWOIT.add(ConstantManager.ID_IT_PWO1);
        PWOIT.add(ConstantManager.ID_IT_PWO2);
        PWOIT.add(ConstantManager.ID_IT_PWO21);
        PWOIT.add(ConstantManager.ID_IT_PWO22);
        PWOIT.add(ConstantManager.ID_IT_PWO23);

        //CustomField
        CustomField authLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_LASTCOMMITTEDDATE);
        CustomField authVerificationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFICATION_LASTCOMMITTEDDATE);
        CustomField authValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VALIDATION_LASTCOMMITTEDDATE);

        //Variables
        Date authLastCommittedDate;
        Date authVerificationLastCommittedDate;
        Date authValidationLastCommittedDate;

        Timestamp now = new Timestamp(new Date().getTime());

        try {
            //Business
            authLastCommittedDate = Toolbox.incrementTimestamp(now,14);
            mutableIssue.setCustomFieldValue(authLastCommittedDateField, authLastCommittedDate);

            authVerificationLastCommittedDate =  Toolbox.incrementTimestamp(now,28);
            mutableIssue.setCustomFieldValue(authVerificationLastCommittedDateField, authVerificationLastCommittedDate);

            authValidationLastCommittedDate =  Toolbox.incrementTimestamp(now,42);
            mutableIssue.setCustomFieldValue(authValidationLastCommittedDateField, authValidationLastCommittedDate);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO21 Business Rules on the field ReworkDateField
     * @param mutableIssue
     * @param user
     * @param log
     */
    public static void processPWO21AuthoringRework(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        CustomField authReworkDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_REWORKDATE);

        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        Date authReworkDate;
        Date futureDate = Toolbox.incrementTimestamp(now, 7);

        if (!mutableIssue.getCustomFieldValue(authReworkDateField).equals(futureDate)) {
            authReworkDate = futureDate;

            try {
                mutableIssue.setCustomFieldValue(authReworkDateField, authReworkDate);

                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(mutableIssue);
            } catch (IndexException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Process PWO21 Business Rules on the Field Rework
     * @param mutableIssue
     * @param user
     * @param log
     */
    public static void processPWO21ReworkField(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        log.debug("Start");

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        List<ChangeHistoryItem> changeItems = ComponentAccessor.getChangeHistoryManager().getAllChangeItems(mutableIssue);
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        log.debug("Issue : " + mutableIssue.getKey());

        //CustomField
        CustomField manuallyEditedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MANUALLYEDITED);
        CustomField nbReworkField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_NBREWORK);

        //Valeur
        String authManuallyEdited = (String) mutableIssue.getCustomFieldValue(manuallyEditedField);
        double nbRework = 0;
        if (mutableIssue.getCustomFieldValue(nbReworkField) != null) {
            nbRework = Double.parseDouble(mutableIssue.getCustomFieldValue(nbReworkField).toString());
        }


        //Business
        double valeurToReturn;
        double compteur = 0;
        if (authManuallyEdited.equals("Yes")) {
            valeurToReturn = nbRework + 1;
        } else {
            log.debug("Pas d'Ã©dit manuel");
            for (ChangeHistoryItem item : changeItems) {
                if (item.getField().equals("status") && item.getFroms().values().contains("In Progress") && item.getTos().values().contains("Delivered")) {
                    compteur = compteur + 1;
                }

            }

            valeurToReturn = compteur - 1;
        }

        try {
            mutableIssue.setCustomFieldValue(nbReworkField, valeurToReturn);
            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process dates calculation for PWO21 on Authoring Request
     * @param mutableIssue
     * @param log
     */
    public static void processPWO21AuthoringRequest(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        /*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
            loadScriptByName(path);
        }*/

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantManager.ID_IT_PWO0);
        PWOIT.add(ConstantManager.ID_IT_PWO1);
        PWOIT.add(ConstantManager.ID_IT_PWO2);
        PWOIT.add(ConstantManager.ID_IT_PWO21);
        PWOIT.add(ConstantManager.ID_IT_PWO22);
        PWOIT.add(ConstantManager.ID_IT_PWO23);

        //CustomField
        CustomField authVerifiedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFIED);
        CustomField authValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VALIDATED);
        CustomField auth1stDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_1STDELIVERYDATE);
        CustomField authLastDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_LASTDELIVERYDATE);
        CustomField authVerificationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFICATION_REQUESTEDDATE);
        CustomField authVerificationFirstCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFICATION_FIRSTCOMMITTEDDATE);
        CustomField authVerificationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFICATION_LASTCOMMITTEDDATE);
        CustomField authValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VALIDATION_REQUESTEDDATE);
        CustomField authValidationFirstCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VALIDATION_FIRSTCOMMITTEDDATE);
        CustomField authValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VALIDATION_LASTCOMMITTEDDATE);
        CustomField authVerificationReworkDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_AUTH_VERIFICATION_REWORKDATE);


        //Variables
        //def now = new Timestamp(new Date().getTime())
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        Date auth1stDeliveryDate;
        Date authLastDeliveryDate;
        Date authVerificationRequestedDate;
        Date authVerification1stCommittedDate;
        Date authVerificationLastCommittedDate;
        Date authValidationRequestedDate;
        Date authValidation1stCommittedDate;
        Date authValidationLastCommittedDate;
        Date authVerificationReworkDate;


        //Business
        //Calculate date on PWO-2.1
        auth1stDeliveryDate = now;
        authLastDeliveryDate = now;

        authVerificationRequestedDate = Toolbox.incrementTimestamp(now, 14);
        authValidationRequestedDate = Toolbox.incrementTimestamp(now, 28);

        Timestamp authVerificationFirstCommittedDate = ((Timestamp) (authVerificationRequestedDate));
        Timestamp authValidationFirstCommittedDate = ((Timestamp) (authValidationRequestedDate));

        authVerificationLastCommittedDate = authVerificationFirstCommittedDate;
        authValidationLastCommittedDate = authValidationFirstCommittedDate;

        authVerificationReworkDate = Toolbox.incrementTimestamp(now, 7);

        if (mutableIssue.getCustomFieldValue(auth1stDeliveryDateField) != null) {
            log.debug("1st delivery, 1stdelivery field is empty");
            mutableIssue.setCustomFieldValue(auth1stDeliveryDateField, auth1stDeliveryDate);
            mutableIssue.setCustomFieldValue(authVerificationRequestedDateField, authVerificationRequestedDate);
            mutableIssue.setCustomFieldValue(authVerificationFirstCommittedDateField, authVerificationFirstCommittedDate);
            mutableIssue.setCustomFieldValue(authValidationRequestedDateField, authValidationRequestedDate);
            mutableIssue.setCustomFieldValue(authValidationFirstCommittedDateField, authValidationFirstCommittedDate);
            mutableIssue.setCustomFieldValue(authVerificationLastCommittedDateField, authVerificationLastCommittedDate);
            mutableIssue.setCustomFieldValue(authValidationLastCommittedDateField, authValidationLastCommittedDate);
        } else {
            log.debug("not 1st delivery, 1stdelivery field is filled");
            mutableIssue.setCustomFieldValue(authVerificationReworkDateField, authVerificationReworkDate);
        }

        try {
            log.debug("LastDelivery date : " + authLastDeliveryDate);
            mutableIssue.setCustomFieldValue(authLastDeliveryDateField, authLastDeliveryDate);


            FieldConfig fieldConfigVerified = authVerifiedField.getRelevantConfig(mutableIssue);
            Option valueVerified = ComponentAccessor.getOptionsManager().getOptions(fieldConfigVerified)?.find{
                    it.toString().equals(ConstantManager.AUTH_VERIFIED);
            };
            mutableIssue.setCustomFieldValue(authVerifiedField, valueVerified);

            FieldConfig fieldConfigValidated = authValidatedField.getRelevantConfig(mutableIssue);
            Option valueValidated = ComponentAccessor.getOptionsManager().getOptions(fieldConfigValidated)?.find{
                    it.toString().equals(ConstantManager.AUTH_VALIDATED);
            };
            mutableIssue.setCustomFieldValue(authValidatedField, valueValidated);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO22 Business Rules on Form Delivery Dates
     * @param mutableIssue
     * @param user
     * @param log
     */
    public static void processPWO22FormDeliveryDates(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        /*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
            loadScriptByName(path);
        } */

        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        OptionsManager optionsManager = ComponentAccessor.getOptionsManager();
        DefaultIssueChangeHolder changeHolder = new DefaultIssueChangeHolder();

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantManager.ID_IT_PWO0);
        PWOIT.add(ConstantManager.ID_IT_PWO1);
        PWOIT.add(ConstantManager.ID_IT_PWO2);
        PWOIT.add(ConstantManager.ID_IT_PWO21);
        PWOIT.add(ConstantManager.ID_IT_PWO22);
        PWOIT.add(ConstantManager.ID_IT_PWO23);

        //CustomField
        CustomField formRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_REQUESTEDDATE);
        CustomField formVerificationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFICATION_REQUESTEDDATE);
        CustomField formValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VALIDATION_REQUESTEDDATE);
        CustomField form1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_1STCOMMITTEDDATE);
        CustomField formVerification1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFICATION_1STCOMMITTEDDATE);
        CustomField formValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VALIDATION_1STCOMMITTEDDATE);
        CustomField formLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_LASTCOMMITTEDDATE);
        CustomField formVerificationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFICATION_LASTCOMMITTEDDATE);
        CustomField formValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VALIDATION_LASTCOMMITTEDDATE);
        CustomField formVerifiedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFIED);
        CustomField formValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VALIDATED);
        CustomField form1stDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_1STDELIVERYDATE);
        CustomField formLastDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_LASTDELIVERYDATE);
        CustomField formVerificationReworkDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFICATION_REWORKDATE);

        //Variables
        //def now = new Timestamp(new Date().getTime())
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        Date formRequestedDate;
        Date formVerificationRequestedDate;
        Date formValidationRequestedDate;
        Date form1stCommittedDate;
        Date formVerification1stCommittedDate;
        Date formValidation1stCommittedDate;
        Date formLastCommittedDate;
        Date formVerificationLastCommittedDate;
        Date formValidationLastCommittedDate;
        Date form1stDeliveryDate;
        Date formLastDeliveryDate;
        Date formVerificationReworkDate;

        try{
            //Business
            //Calculate date on PWO-2.1
            if (mutableIssue.getCustomFieldValue(form1stDeliveryDateField) != null) {
                form1stDeliveryDate = now;
                formVerificationRequestedDate =  Toolbox.incrementTimestamp(now,7);
                formValidationRequestedDate =  Toolbox.incrementTimestamp(now,14);
                formVerification1stCommittedDate = formVerificationRequestedDate;
                formVerificationLastCommittedDate =  Toolbox.incrementTimestamp(now,7);
                formValidation1stCommittedDate = formValidationRequestedDate;
                formValidationLastCommittedDate =  Toolbox.incrementTimestamp(now,14);

                mutableIssue.setCustomFieldValue(form1stDeliveryDateField, form1stDeliveryDate);
                mutableIssue.setCustomFieldValue(formVerificationRequestedDateField, formVerificationRequestedDate);
                mutableIssue.setCustomFieldValue(formValidationRequestedDateField, formValidationRequestedDate);
                mutableIssue.setCustomFieldValue(formVerification1stCommittedDateField, formVerification1stCommittedDate);
                mutableIssue.setCustomFieldValue(formVerificationLastCommittedDateField, formVerificationLastCommittedDate);
                mutableIssue.setCustomFieldValue(formValidation1stCommittedDateField, formValidation1stCommittedDate);
                mutableIssue.setCustomFieldValue(formValidationLastCommittedDateField, formValidationLastCommittedDate);
            } else {
                formVerificationReworkDate =  Toolbox.incrementTimestamp(now,7);
                mutableIssue.setCustomFieldValue(formVerificationReworkDateField, formVerificationReworkDate);
            }


            formLastDeliveryDate = now;
            //formVerificationLastCommittedDate=(Timestamp) (now+7)
            //formValidationLastCommittedDate=(Timestamp) (now+14)

            mutableIssue.setCustomFieldValue(formLastDeliveryDateField, formLastDeliveryDate);
            //issue.setCustomFieldValue(formVerificationLastCommittedDateField,formVerificationLastCommittedDate)
            //issue.setCustomFieldValue(formValidationLastCommittedDateField,formValidationLastCommittedDate)

            FieldConfig fieldConfigVerified = formVerifiedField.getRelevantConfig(mutableIssue);
            Option valueVerified = ComponentAccessor.getOptionsManager().getOptions(fieldConfigVerified)?.find{
                    it.toString().equals(ConstantManager.FORM_VERIFIED);
            };
            mutableIssue.setCustomFieldValue(formVerifiedField, valueVerified);

            FieldConfig fieldConfigValidated = formValidatedField.getRelevantConfig(mutableIssue);
            Option valueValidated = ComponentAccessor.getOptionsManager().getOptions(fieldConfigValidated)?.find{it ->
                    it.toString().equals(ConstantManager.FORM_VALIDATED);
            };
            mutableIssue.setCustomFieldValue(formValidatedField, valueValidated);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO22 Business Rules on Form Committed Dates
     * @param mutableIssue
     * @param user
     * @param log
     */
    public static void processPWO22CommittedDates(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        //String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        //def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
        //    loadScriptByName(path);
        //}


        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        OptionsManager optionsManager = ComponentAccessor.getOptionsManager();
        DefaultIssueChangeHolder changeHolder = new DefaultIssueChangeHolder();

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantManager.ID_IT_PWO0);
        PWOIT.add(ConstantManager.ID_IT_PWO1);
        PWOIT.add(ConstantManager.ID_IT_PWO2);
        PWOIT.add(ConstantManager.ID_IT_PWO21);
        PWOIT.add(ConstantManager.ID_IT_PWO22);
        PWOIT.add(ConstantManager.ID_IT_PWO23);

        //CustomField
        CustomField formLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_LAST_COMMITTED_DATE);
        CustomField formVerificationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFICATION_LAST_COMMITTED_DATE);
        CustomField formValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VALIDATION_LAST_COMMITTED_DATE);

        //Variables
        Date formLastCommittedDate;
        Date formVerificationLastCommittedDate;
        Date formValidationLastCommittedDate;

        Timestamp now = new Timestamp(new Date().getTime());


        //Business
        try {
            formLastCommittedDate = Toolbox.incrementTimestamp(now,7);
            mutableIssue.setCustomFieldValue(formLastCommittedDateField, formLastCommittedDate);

            formVerificationLastCommittedDate = Toolbox.incrementTimestamp(now,14);
            mutableIssue.setCustomFieldValue(formVerificationLastCommittedDateField, formVerificationLastCommittedDate);

            formValidationLastCommittedDate = Toolbox.incrementTimestamp(now,21);
            mutableIssue.setCustomFieldValue(formValidationLastCommittedDateField, formValidationLastCommittedDate);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO22 Business Rules on Form Rework Date Field
     * @param mutableIssue
     * @param user
     * @param log
     */
    public static void processPWO22FormReworkDate(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        CustomField formReworkDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_REWORKDATE);

        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        Date formReworkDate;
        Date futureDate =  Toolbox.incrementTimestamp(now,7);

        try{
            if (!mutableIssue.getCustomFieldValue(formReworkDateField).equals(futureDate)) {
                formReworkDate = futureDate;
                mutableIssue.setCustomFieldValue(formReworkDateField, formReworkDate);

                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(mutableIssue);
            }
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO22 Business Rules on NbReworkField
     * @param mutableIssue
     * @param user
     * @param log
     */
    public static void processPWO22NbReworkField(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        log.debug("Start");

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        ChangeHistoryManager changeHistoryManager = ComponentAccessor.getChangeHistoryManager();
        ChangeHistoryItem changeHistoryItem = ComponentAccessor.getComponentOfType(ChangeHistoryItem.class);
        List<ChangeHistoryItem> changeItems = ComponentAccessor.getChangeHistoryManager().getAllChangeItems(mutableIssue);
        DefaultIssueChangeHolder changeHolder = new DefaultIssueChangeHolder();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        log.debug("Issue : " + mutableIssue.getKey());

        //CustomField
        CustomField manuallyEditedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MANUALLYEDITED);
        CustomField nbReworkField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_NBREWORK);

        //Valeur
        String formManuallyEdited = (String) mutableIssue.getCustomFieldValue(manuallyEditedField);
        double nbRework = 0;
        if (mutableIssue.getCustomFieldValue(nbReworkField) != null) {
            nbRework = Double.parseDouble(mutableIssue.getCustomFieldValue(nbReworkField).toString());
        }

        try {
            //Business
            double valeurToReturn;
            double compteur = 0;
            if (formManuallyEdited.equals("Yes")) {
                valeurToReturn = nbRework + 1;
            } else {
                log.debug("Pas d'Ã©dit manuel");
                for (ChangeHistoryItem item : changeItems) {
                    if (item.getField().equals("status") && item.getFroms().values().contains("In Progress") && item.getTos().values().contains("Delivered")) {
                        compteur = compteur + 1;
                    }

                }
                valeurToReturn = compteur - 1;
            }

            mutableIssue.setCustomFieldValue(nbReworkField, valeurToReturn);
            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO22 Business Rules on Verification Dates
     * @param mutableIssue
     * @param user
     * @param log
     */
    public static void processPWO22VerificationDates(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        //String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        /*def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
            loadScriptByName(path);
        } */

        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        OptionsManager optionsManager = ComponentAccessor.getOptionsManager();
        DefaultIssueChangeHolder changeHolder = new DefaultIssueChangeHolder();

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantManager.ID_IT_PWO0);
        PWOIT.add(ConstantManager.ID_IT_PWO1);
        PWOIT.add(ConstantManager.ID_IT_PWO2);
        PWOIT.add(ConstantManager.ID_IT_PWO21);
        PWOIT.add(ConstantManager.ID_IT_PWO22);
        PWOIT.add(ConstantManager.ID_IT_PWO23);

        //CustomField
        CustomField formRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_REQUESTEDDATE);
        CustomField formVerificationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFICATION_REQUESTEDDATE);
        CustomField formValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VALIDATION_REQUESTEDDATE);
        CustomField form1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_1STCOMMITTEDDATE);
        CustomField formVerification1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFICATION_1STCOMMITTEDDATE);
        CustomField formValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VALIDATION_1STCOMMITTEDDATE);
        CustomField formLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_LASTCOMMITTEDDATE);
        CustomField formVerificationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VERIFICATION_LASTCOMMITTEDDATE);
        CustomField formValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_FORM_VALIDATION_LASTCOMMITTEDDATE);


        //Variables
        //def now = new Timestamp(new Date().getTime())
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        Date formRequestedDate;
        Date formVerificationRequestedDate;
        Date formValidationRequestedDate;
        Date form1stCommittedDate;
        Date formVerification1stCommittedDate;
        Date formValidation1stCommittedDate;
        Date formLastCommittedDate;
        Date formVerificationLastCommittedDate;
        Date formValidationLastCommittedDate;


        //Business
        //Calculate date on PWO-2.1
        formRequestedDate = Toolbox.incrementTimestamp(now,7);
        formVerificationRequestedDate =  Toolbox.incrementTimestamp(now,14);
        formValidationRequestedDate =  Toolbox.incrementTimestamp(now,21);

        form1stCommittedDate = formRequestedDate;
        formVerification1stCommittedDate = formVerificationRequestedDate;
        formValidation1stCommittedDate = formValidationRequestedDate;

        formLastCommittedDate = form1stCommittedDate;
        formVerificationLastCommittedDate = formVerification1stCommittedDate;
        formValidationLastCommittedDate = formValidation1stCommittedDate;

        mutableIssue.setCustomFieldValue(formRequestedDateField, formRequestedDate);
        mutableIssue.setCustomFieldValue(formVerificationRequestedDateField, formVerificationRequestedDate);
        mutableIssue.setCustomFieldValue(formValidationRequestedDateField, formValidationRequestedDate);

        mutableIssue.setCustomFieldValue(form1stCommittedDateField, form1stCommittedDate);
        mutableIssue.setCustomFieldValue(formVerification1stCommittedDateField, formVerification1stCommittedDate);
        mutableIssue.setCustomFieldValue(formValidation1stCommittedDateField, formValidation1stCommittedDate);

        mutableIssue.setCustomFieldValue(formLastCommittedDateField, formLastCommittedDate);
        mutableIssue.setCustomFieldValue(formVerificationLastCommittedDateField, formVerificationLastCommittedDate);
        mutableIssue.setCustomFieldValue(formValidationLastCommittedDateField, formValidationLastCommittedDate);

        //issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
        //issueIndexingService.reIndex(issue);
    }

    /**
     * Process PWO23 Business Rules on INT Validation???
     * @param mutableIssue
     * @param user
     * @param log
     */
    public static void processPWO23IntValidation(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        /*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
            loadScriptByName(path);
        } */

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantManager.ID_IT_PWO0);
        PWOIT.add(ConstantManager.ID_IT_PWO1);
        PWOIT.add(ConstantManager.ID_IT_PWO2);
        PWOIT.add(ConstantManager.ID_IT_PWO21);
        PWOIT.add(ConstantManager.ID_IT_PWO22);
        PWOIT.add(ConstantManager.ID_IT_PWO23);

        //CustomField
        CustomField intRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_REQUESTEDDATE);
        CustomField int1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_1STCOMMITTEDDATE);
        CustomField intLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_LASTCOMMITTEDDATE);


        //Variables
        //def now = new Timestamp(new Date().getTime())
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        Date intRequestedDate;
        Date int1stCommittedDate;
        Date intLastCommittedDate;

        try {
            //Business
            //Calculate date on PWO-2.1
            intRequestedDate = Toolbox.incrementTimestamp(now, 7);
            int1stCommittedDate = intRequestedDate;
            intLastCommittedDate = int1stCommittedDate;

            mutableIssue.setCustomFieldValue(intRequestedDateField, intRequestedDate);
            mutableIssue.setCustomFieldValue(int1stCommittedDateField, int1stCommittedDate);
            mutableIssue.setCustomFieldValue(intLastCommittedDateField, intLastCommittedDate);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO23 Business Rules on Technical Closure
     * @param mutableIssue
     * @param user
     * @param log
     */
    public static void processPWO23TechnicalClosure(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        /*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
            loadScriptByName(path);
        } */

        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantManager.ID_IT_PWO0);
        PWOIT.add(ConstantManager.ID_IT_PWO1);
        PWOIT.add(ConstantManager.ID_IT_PWO2);
        PWOIT.add(ConstantManager.ID_IT_PWO21);
        PWOIT.add(ConstantManager.ID_IT_PWO22);
        PWOIT.add(ConstantManager.ID_IT_PWO23);

        //CustomField
        CustomField int1stDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_1STDELIVERYDATE);
        CustomField intLastDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_INT_LASTDELIVERYDATE);
        CustomField technicallyClosedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_TECHNICALLYCLOSED);
        CustomField technicalClosureDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_TECHNICALCLOSUREDATE);

        //Variables
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        Date int1stDeliveryDate;
        Date intLastDeliveryDate;
        Date technicalClosureDate;
        String technicallyClosed = "Yes";

        try {
            //Business
            //Calculate date on PWO-2.1
            if (mutableIssue.getCustomFieldValue(int1stDeliveryDateField) != null) {
                int1stDeliveryDate = now;

                mutableIssue.setCustomFieldValue(int1stDeliveryDateField, int1stDeliveryDate);
            }


            intLastDeliveryDate = now;
            technicalClosureDate = now;

            mutableIssue.setCustomFieldValue(intLastDeliveryDateField, intLastDeliveryDate);

            FieldConfig fieldConfigTechnicallyClosed = technicallyClosedField.getRelevantConfig(mutableIssue);
            Option valueTechnicallyClosed = ComponentAccessor.getOptionsManager().getOptions(fieldConfigTechnicallyClosed).find{
                it.toString().equals(technicallyClosed);
            };
            mutableIssue.setCustomFieldValue(technicallyClosedField, valueTechnicallyClosed);
            mutableIssue.setCustomFieldValue(technicalClosureDateField, technicalClosureDate);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * re-open PWO0 as Administrator
     * @param mutableIssue
     * @param user
     * @param log
     */
    public static void reOpenPWO0AsAdministrator(MutableIssue mutableIssue, ApplicationUser user, Logger log){
        /*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
            loadScriptByName(path);
        } */

        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager.class);
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField partsDataStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_STATUS);
        CustomField maintPlanningStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_STATUS);
        CustomField maintStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_STATUS);

        //Variables

        //Business

        FieldConfig fieldConfigPartsDataStatus = partsDataStatusField.getRelevantConfig(mutableIssue);
        Object valuePartsDataStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigPartsDataStatus)?.find{
            it.toString().equals(ConstantManager.NOT_STARTED);
        };
        FieldConfig fieldConfigMaintPlanningStatus = maintPlanningStatusField.getRelevantConfig(mutableIssue);
        Object valueMaintPlanningStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigMaintPlanningStatus)?.find{
            it.toString().equals(ConstantManager.NOT_STARTED);
        };
        FieldConfig fieldConfigMaintStatus = maintStatusField.getRelevantConfig(mutableIssue);
        Object valueMaintStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigMaintStatus)?.find{  it->
            it.toString().equals(ConstantManager.NOT_STARTED);
        };

        mutableIssue.setCustomFieldValue(partsDataStatusField, valuePartsDataStatus);
        mutableIssue.setCustomFieldValue(maintPlanningStatusField, valueMaintPlanningStatus);
        mutableIssue.setCustomFieldValue(maintStatusField, valueMaintStatus);

        issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
        try {
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO0 Rules on dependencies
     * @param issue
     * @param user
     * @param log
     */
    public static void updatePWO0Dependencies(Issue issue, ApplicationUser user, Logger log){
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField startDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_STARTDATE);

        //def DateDuJour = new TimeStamp(new Date().getTime())
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        //log.debug "Date du jour : " + DateDuJour

        log.debug("issue en cours : " + issue.getKey());

        if (issueLinkManager.getOutwardLinks(issue.getId())!= null) {
            issueLinkManager.getOutwardLinks(issue.getId()).each{ issueLink ->
                Issue sourceIssue = (Issue)issueLink.getDestinationObject();
                log.debug("Linked issue of PWO-0 : " + sourceIssue.getKey());
                String issueType = sourceIssue.getIssueTypeId();
                if (issueType.equals(ConstantManager.ID_IT_CLUSTER)) {
                    Issue clusterIssue = (Issue)sourceIssue;
                    if (issueLinkManager.getOutwardLinks(clusterIssue.getId()) != null) {
                        return issueLinkManager.getOutwardLinks(clusterIssue.getId()).each{ issueLink2 ->
                            Issue sourceIssue2 = (Issue)issueLink2.getDestinationObject();
                            log.debug("Linked issue of Cluster : " + sourceIssue2.getKey());
                            String issueType2 = sourceIssue2.getIssueTypeId();
                            if (issueType2.equals(ConstantManager.ID_IT_TRIGGER)) {
                                log.debug("Trigger found : " + sourceIssue2.getKey());
                                Issue triggerissue = issueManager.getIssueObject(sourceIssue2.getId());
                                if (!triggerissue.getStatusId().equals(ConstantManager.REQUIRED_TRIGGER_STATUS)) {
                                    ((MutableIssue) triggerissue).setCustomFieldValue(startDateField, now);

                                    issueManager.updateIssue(user, (MutableIssue) triggerissue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                    try {
                                        issueIndexingService.reIndex(triggerissue);
                                    } catch (IndexException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else if (issueLinkManager.getInwardLinks(issue.getId()) != null) {
            issueLinkManager.getInwardLinks(issue.getId()).each{ issueLink ->
                Issue sourceIssue = (Issue)issueLink.getSourceObject();
                log.debug("Linked issue of PWO-0 : " + sourceIssue.getKey());
                String issueType = sourceIssue.getIssueTypeId().toString();
                if (issueType.equals(ConstantManager.ID_IT_CLUSTER)) {
                    Issue clusterIssue = sourceIssue;
                    if (issueLinkManager.getOutwardLinks(clusterIssue.getId()) != null) {
                        return issueLinkManager.getOutwardLinks(clusterIssue.getId()).each{ issueLink2 ->
                            Issue sourceIssue2 = (Issue)issueLink2.getDestinationObject();
                            log.debug("Linked issue of Cluster : " + sourceIssue2.getKey());
                            String issueType2 = sourceIssue2.getIssueTypeId();
                            if (issueType2.equals(ConstantManager.ID_IT_TRIGGER)) {
                                log.debug("Trigger found : " + sourceIssue2.getKey());
                                Issue triggerissue = (Issue)issueManager.getIssueObject(sourceIssue2.getId());
                                if (!triggerissue.getStatusId().equals(ConstantManager.REQUIRED_TRIGGER_STATUS)) {
                                    ((MutableIssue) triggerissue).setCustomFieldValue(startDateField, now);
                                    issueManager.updateIssue(user, (MutableIssue) triggerissue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                    try {
                                        issueIndexingService.reIndex(triggerissue);
                                    } catch (IndexException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        };
                    }
                }
            };
        }
    }

    /**
     * Call a remote script from Start transition for the issue
     * and Field Link to data???
     * @param mutableIssue
     * @param user
     * @param log
     */
    public static void updateFieldLinkToData(MutableIssue mutableIssue, ApplicationUser user, Logger log){
        log.debug("Call a remote script from Start transition for " + mutableIssue.getKey());

        // Managers
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //Customfield
        CustomFieldManager cfManager = ComponentAccessor.getCustomFieldManager();
        CustomField linkToDataField = cfManager.getCustomFieldObject(ConstantManager.ID_CF_LINKTODATA);

        // Windows
        //String script = "\\\\"+SERVER+"\\"+NETWORK+"\\"+FOLDER+"\\"+SUBFOLDER+"\\IMS_Script.bat "+issue.getSummary()
        String path = "\\\\" + ConstantManager.SERVER + "\\" + ConstantManager.NETWORK + "\\" + ConstantManager.FOLDER + "\\" + mutableIssue.getSummary();
        String script = "\\\\D:\\JIRA-HOME\\NAS\\" + "IMS_Script.bat " + path;
        // Unix
        //String script = "sh /"+SERVER+"/"+NETWORK+"/script-IMS-Test-TLFWP1-81.sh"
        log.debug("script : " + script);
        Object proc = script.execute();

        log.debug("Executed");
        proc.waitForOrKill(5000);
        if (proc.exitValue() != null) {
            log.debug("Error");
            log.debug(proc.err.text);
        } else {
            log.debug("Applied");
            log.debug(proc.text);
        }

        try {
            mutableIssue.setCustomFieldValue(linkToDataField, path);
            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
            log.debug("Field Link to data updated");
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Validate Trigger
     * @param mutableIssue
     * @param user
     * @param log
     * @return
     */
    public static boolean validateTrigger(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        //Manager
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        JqlQueryParser jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser.class);
        SearchProvider searchProvider = ComponentAccessor.getComponent(SearchProvider.class);

        String JQL_QUERY = "issuetype = \"Trigger\" and summary ~ \"%s\" and Program = \"%s [Program_%s]\" and \"Type of H/C\" = \"%s\" ";
        String query = "";

        //CustomField
        CustomField programField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PROGRAM);
        CustomField typeOfHCField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_TYPEOFHC);

        String currentSummary = mutableIssue.getSummary();
        String currentProgramBrut = String.valueOf(mutableIssue.getCustomFieldValue(programField));
        String currentProgram = currentProgramBrut.replaceAll("Program_", "").replaceAll("<content>", "").replaceAll("<value>", "").replaceAll("</value>", ",").replaceAll("</content>", "").replaceAll(",", "").replaceAll("\n", "").replaceAll("  ", "");
        String currentTypeOfHC = String.valueOf(mutableIssue.getCustomFieldValue(typeOfHCField));

        List<Issue> issuesFound = new ArrayList<Issue>();

        query = String.format(JQL_QUERY, currentSummary, currentProgram, currentProgram, currentTypeOfHC, mutableIssue.getId());
        log.debug("Query : " + query);
        Query JQLQuery = null;
        try {
            JQLQuery = jqlQueryParser.parseQuery(query);
            SearchResults results = searchProvider.search(JQLQuery, user, PagerFilter.getUnlimitedFilter());
            issuesFound = results.getIssues();
        } catch (JqlParseException e) {
            e.printStackTrace();
        } catch (SearchException e) {
            e.printStackTrace();
        }

        log.debug("issues found : " + issuesFound);
        if(issuesFound != null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Cancel PWO0
     * @param mutableIssue
     * @param user
     * @param log
     */
    public static void cancelPWO0(MutableIssue mutableIssue, ApplicationUser user, Logger log){
        //String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        //def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
        //    loadScriptByName(path);
        //}

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField partsDataStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_PARTSDATA_STATUS);
        CustomField maintPlanningStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINTPLANNING_STATUS);
        CustomField maintStatusField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MAINT_STATUS);

        //Variables

        //Business
        FieldConfig fieldConfigPartsDataStatus = partsDataStatusField.getRelevantConfig(mutableIssue);
        Object valuePartsDataStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigPartsDataStatus)?.find{
                it.toString().equals("Cancelled");
        };
        FieldConfig fieldConfigMaintPlanningStatus = maintPlanningStatusField.getRelevantConfig(mutableIssue);
        Object valueMaintPlanningStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigMaintPlanningStatus)?.find{
                it.toString().equals("Cancelled");
        };
        FieldConfig fieldConfigMaintStatus = maintStatusField.getRelevantConfig(mutableIssue);
        Object valueMaintStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigMaintStatus)?.find{
                it.toString().equals("Cancelled");
        };

        try {
            mutableIssue.setCustomFieldValue(partsDataStatusField, valuePartsDataStatus);
            mutableIssue.setCustomFieldValue(maintPlanningStatusField, valueMaintPlanningStatus);
            mutableIssue.setCustomFieldValue(maintStatusField, valueMaintStatus);
            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Check if the status of the issue is different than 'Completed' or 'Cancelled'
     * @param issue
     * @return
     */
    public static boolean checkIfStatusIsNotCompletedOrCancelled(Issue issue) {
        return !("Completed".equals(issue.getStatus().getName()) || "Cancelled".equals(issue.getStatus().getName()));
    }

    /**
     * Auto cancel the cluster
     * @param mutableIssue
     * @param user
     * @param log
     * @return
     */
    public static boolean autoCancelCluster(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();

        String cancelled = "cancelled";
        String result = "OK";

        /*def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
            //If cluster;
            if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {*/
        //Get linked issues of Cluster;
        Collection<Issue> linkedIssuesOfCluster = issueLinkManager.getLinkCollection(mutableIssue, user).getAllIssues();
        for (Issue linkedIssueOfCluster : linkedIssuesOfCluster) {
            //If PWO-0;
            if (linkedIssueOfCluster.getIssueTypeId().equals(String.valueOf(ConstantManager.ID_IT_PWO0))) {
                log.debug("PWO-0 : " + linkedIssueOfCluster.getKey());
                log.debug("PWO-0 status : " + linkedIssueOfCluster.getStatus().getSimpleStatus().getName());
                //Get status;
                if (linkedIssueOfCluster.getStatus().getSimpleStatus().getName() == cancelled && result == "OK") {
                    result = "OK";
                } else {
                    result = "KO";
                }
                //Get linked issues of PWO-0;
                Collection<Issue> linkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                for (Issue linkedIssueOfPWO0 : linkedIssuesOfPWO0) {
                    //if PWO-1;
                    if (linkedIssueOfPWO0.getIssueTypeId().equals(String.valueOf(ConstantManager.ID_IT_PWO1))) {
                        log.debug("PWO-1 : " + linkedIssueOfPWO0.getKey());
                        log.debug("PWO-1 status : " + linkedIssueOfPWO0.getStatus().getSimpleStatus().getName());
                        //Get status;
                        if (linkedIssueOfPWO0.getStatus().getSimpleStatus().getName() == cancelled && result == "OK") {
                            result = "OK";
                        } else {
                            result = "KO";
                        }
                        //Get linked issues of PWO-1;
                        Collection<Issue> linkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                        for (Issue linkedIssueOfPWO1 : linkedIssuesOfPWO1) {
                            //If PWO-2;
                            if (linkedIssueOfPWO1.getIssueTypeId().equals(String.valueOf(ConstantManager.ID_IT_PWO2))) {
                                //Get sub-task;
                                Collection<Issue> LinkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects();
                                for (Issue linkedIssueOfPWO2 : LinkedIssuesOfPWO2) {
                                    if (linkedIssueOfPWO2.getIssueTypeId().equals(String.valueOf(ConstantManager.ID_IT_PWO21))) {
                                        log.debug("PWO-2.1 : " + linkedIssueOfPWO2.getKey());
                                        log.debug("PWO-2.1 status : " + linkedIssueOfPWO2.getStatus().getSimpleStatus().getName());
                                        //Get status;
                                        if (linkedIssueOfPWO2.getStatus().getSimpleStatus().getName() == cancelled && result == "OK") {
                                            result = "OK";
                                        } else {
                                            result = "KO";
                                        }
                                    } else if (linkedIssueOfPWO2.getIssueTypeId().equals(String.valueOf(ConstantManager.ID_IT_PWO22))) {
                                        log.debug("PWO-2.2 : " + linkedIssueOfPWO2.getKey());
                                        log.debug("PWO-2.2 status : " + linkedIssueOfPWO2.getStatus().getSimpleStatus().getName());
                                        //Get status;
                                        if (linkedIssueOfPWO2.getStatus().getSimpleStatus().getName() == cancelled && result == "OK") {
                                            result = "OK";
                                        } else {
                                            result = "KO";
                                        }
                                    } else if (linkedIssueOfPWO2.getIssueTypeId().equals(String.valueOf(ConstantManager.ID_IT_PWO23))) {
                                        log.debug("PWO-2.3 : " + linkedIssueOfPWO2.getKey());
                                        log.debug("PWO-2.3 status : " + linkedIssueOfPWO2.getStatus().getSimpleStatus().getName());
                                        //Get status;
                                        if (linkedIssueOfPWO2.getStatus().getSimpleStatus().getName() == cancelled && result == "OK") {
                                            result = "OK";
                                        } else {
                                            result = "KO";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //}
        //}
        boolean passesCondition = false;
        if (result == "OK") {
            passesCondition = true;
        }
        return passesCondition;
    }

    /**
     * Condition for PWO1 to start
     * @param mutableIssue
     * @param user
     * @param log
     * @return
     */
    public static boolean isPWO1StartConditionSatisfied(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        //Manager
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        CustomField iaWUField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_WU);
        CustomField iaNbWUField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_NBWU);

        String result = "OK";

        if (!result.equals("KO") && mutableIssue.getCustomFieldValue(iaWUField) != null && mutableIssue.getCustomFieldValue(iaNbWUField) != null) {
            result = "OK";
        } else {
            result = "KO";
        }

        boolean passesCondition = false;
        if (result.equals("OK")) {
            passesCondition = true;
        }
        return passesCondition;
    }

    /**
     * Condition to copy DU
     * @param mutableIssue
     * @param user
     * @param log
     * @return
     */
    public static boolean isCopyDUConditionSatisfied(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();

        List<Issue> linkedIssuesOfDU = issueLinkManager.getLinkCollection(mutableIssue, user).getInwardIssues("Copies");
        boolean passesCondition = false;
        if (linkedIssuesOfDU != null) {
            for (Issue linkedIssueOfDU : linkedIssuesOfDU) {
                log.debug("Outward link of DU = " + linkedIssueOfDU.getKey());
                if (linkedIssueOfDU.getProjectId() == ConstantManager.ID_PROJECT_DU) {
                    passesCondition = false;
                } else {
                    passesCondition = true;
                }
            }

        } else {
            passesCondition = true;
        }

        return passesCondition;
    }

    /**
     *Process PWO1 Business Rules on the field iaValidatedField (First step on delivery)
     *
     *(TO BE COMPLETED)
     * @param mutableIssue
     * @param user
     * @param log
     */
    public static void processPWO1IAValidatedField(MutableIssue mutableIssue, ApplicationUser user, Logger log){
        /*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
            loadScriptByName(path);
        } */

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField iaValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_VALIDATION_REQUESTEDDATE);
        CustomField iaValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_VALIDATION_1STCOMMITTEDDATE);
        CustomField iaValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_VALIDATION_LASTCOMMITTEDDATE);
        CustomField ia1stDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_1STDELIVERYDATE);
        CustomField iaLastDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_LASTDELIVERYDATE);
        CustomField iaValidatedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_IA_VALIDATED);

        //Variables
        //def now = new Timestamp(new Date().getTime())
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        Date iaValidationRequestedDate;
        Date iaValidation1stCommittedDate;
        Date iaValidationLastCommittedDate;
        Date ia1stDeliveryDate;
        Date iaLastDeliveryDate;

        try {
            //Business
            iaValidationRequestedDate = Toolbox.incrementTimestamp(now, 7);
            iaValidation1stCommittedDate = iaValidationRequestedDate;
            iaValidationLastCommittedDate = iaValidation1stCommittedDate;
            ia1stDeliveryDate = now;
            iaLastDeliveryDate = now;
            log.debug("IA LAst delivery date to set : " + iaLastDeliveryDate);

            mutableIssue.setCustomFieldValue(iaValidationRequestedDateField, iaValidationRequestedDate);
            mutableIssue.setCustomFieldValue(iaValidation1stCommittedDateField, iaValidation1stCommittedDate);
            mutableIssue.setCustomFieldValue(iaValidationLastCommittedDateField, iaValidationLastCommittedDate);
            log.debug("setting last delivery date");
            mutableIssue.setCustomFieldValue(iaLastDeliveryDateField, iaLastDeliveryDate);
            log.debug("last delivery date have been set");
            if (mutableIssue.getCustomFieldValue(ia1stDeliveryDateField) != null) {
                mutableIssue.setCustomFieldValue(ia1stDeliveryDateField, ia1stDeliveryDate);
            }


            if (mutableIssue.getCustomFieldValue(iaValidatedField).toString().equals("Rejected")) {
                FieldConfig fieldConfigValidated = iaValidatedField.getRelevantConfig(mutableIssue);
                Option valueValidated = ComponentAccessor.getOptionsManager().getOptions(fieldConfigValidated)?.find{
                    it.toString().equals(ConstantManager.VALIDATION_PENDING);
                };
                mutableIssue.setCustomFieldValue(iaValidatedField, valueValidated);
            }

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO1 Business rules on nbReworkField (Second step on delivery)
     *
     * (TO BE COMPLETED)
     * @param mutableIssue
     * @param user
     * @param log
     */
    public static void processPWO1NbReworkField(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        log.debug("Start");

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        List<ChangeHistoryItem> changeItems = ComponentAccessor.getChangeHistoryManager().getAllChangeItems(mutableIssue);
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        log.debug("Issue : " + mutableIssue.getKey());

        //CustomField
        CustomField manuallyEditedField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_MANUALLYEDITED_PWO1);
        CustomField nbReworkField = customFieldManager.getCustomFieldObject(ConstantManager.ID_CF_NBREWORK_PWO1);

        //Valeur
        String iaManuallyEdited = (String) mutableIssue.getCustomFieldValue(manuallyEditedField);
        double nbRework = 0;
        if (mutableIssue.getCustomFieldValue(nbReworkField) != null) {
            nbRework = Double.valueOf(mutableIssue.getCustomFieldValue(nbReworkField).toString());
        }

        //Business
        double valeurToReturn;
        double compteur = 0;
        if (iaManuallyEdited.equals("Yes")) {
            valeurToReturn = nbRework + 1;
        } else {
            log.debug("Pas d'edit manuel");
            for (ChangeHistoryItem item : changeItems) {
                log.debug("Change history item : " + item.getField() + ", change history value : " + item.getTos().values());
                if (item.getField().equals("status") && item.getTos().values().contains("Delivered") && item.getFroms().values().contains("In Progress")) {
                    log.debug("je compte");
                    compteur = compteur + 1;
                    log.debug("compteur : " + compteur);
                }

            }

            valeurToReturn = compteur - 1;
            log.debug("Valeur : " + valeurToReturn);
        }

        try {
            mutableIssue.setCustomFieldValue(nbReworkField, valeurToReturn);
            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Check if the workload is updated for Sub PWO
     * @param changeLog
     * @param itemsChanged
     * @return
     */
    public static boolean workloadIsUpdatedForSubPWO(GenericValue changeLog, List<String> itemsChanged) {
        boolean isUpdated = false;
        if (changeLog != null) {
            ChangeHistory change = ComponentAccessor.getChangeHistoryManager().getChangeHistoryById(changeLog.getLong("id"));
            List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans();
            for (ChangeItemBean changeIteamBean : changeItemBeans) {
                if (itemsChanged.contains(changeIteamBean.getField())) {
                    isUpdated = true;
                }
            }
        }
        return isUpdated;
    }
}